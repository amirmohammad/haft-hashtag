const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const smsLoginSchema = new Schema({
    phoneNumber : { type : String , required : true},
    code : { type : String , required : true},
    used : { type : String , default : false},
    expireAt: {type: Date, default: new Date().getTime() + (60*60*1000)}, // 1 hour valid
} , { timestamps : true , toJSON : { virtuals : true } });




module.exports = mongoose.model('SMSLogin' , smsLoginSchema);