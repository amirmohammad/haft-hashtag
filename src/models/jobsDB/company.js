const mongoose = require('mongoose'); 
const Schema = mongoose.Schema; 

const CompanySchema = new Schema({
    name: {type: String, required: true },
    description: {type: String},
    gallery: [{type: String, ref:'CompanyImage'}] ,
    certificate: [{type: String, ref:'CompanyCertificate'}],
    owner: {type: Schema.Types.ObjectId, ref: "User"},
    category: {type: Schema.Types.ObjectId, ref: "CompanyCategory", require: true},
    logo: {type: String, default: 'static/companyLogo.png'},
    state: {type: String, default: null},
    city: {type: String, default: null}


}, {timestamps: true, toJSON: {virtuals: true}} );

CompanySchema.virtual('Imagegallery' , {
    ref : 'CompanyImage',
    localField : '_id',
    foreignField : 'url'
});



module.exports = mongoose.model('Company', CompanySchema);