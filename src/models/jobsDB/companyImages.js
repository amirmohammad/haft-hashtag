const mongoose = require('mongoose'); 
const Schema = mongoose.Schema; 

const CompanyImageSchema = new Schema({
    url: {type: String, required: true, unique: true},
    description: {type: String},
    company : { type : Schema.Types.ObjectId , ref : 'Company'},
    
}, {timestamps: true, toJSON: {virtuals: true}});

// CompanyImageSchema.virtual('company' , {
//     ref : 'Company',
//     localField : 'company',
//     foreignField : '_id'
// });

module.exports = mongoose.model('CompanyImage', CompanyImageSchema);