const mongoose = require('mongoose'); 
const Schema = mongoose.Schema; 

const CompanyCertificateSchema = new Schema({
    url: {type: String, required: true, unique: true},
    description: {type: String},
    certificateLevel: {type: String, default: 1},
    company : { type : Schema.Types.ObjectId , ref : 'Company'},
}, {timestamps: true});

module.exports = mongoose.model('CompanyCertificate', CompanyCertificateSchema);