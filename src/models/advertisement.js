const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')
const Schema = mongoose.Schema;

const advertisementSchema = new Schema ({
    bannerImage: {type: String, required: true},
    link: {type: String, default: null},
    description: {type: String, default: null},
    wallpaper: {type: Boolean, default: false}
}, {timestamps: true})

module.exports = mongoose.model('Advertisement', advertisementSchema); 