const bcrypt = require('bcrypt')
const mongoose = require('mongoose')
// const Schema = mongoose.Schema; 
const uniqueString = require('unique-string')
// const mongoosePaginate = require('mongoose-paginate')


const adminUserSchema = new mongoose.Schema({
    username: {type: String, required: true},
    password: {type: String, required: true},
    rememberToken : { type : String , default : null },
    admin: {type: Boolean, default: false}
}, {timestamps: true})


adminUserSchema.pre('save' , function(next) {
    let salt = bcrypt.genSaltSync(15);
    let hash = bcrypt.hashSync(this.password , salt);

    this.password = hash;
    next();
});


adminUserSchema.methods.comparePassword = function(password) {
    return bcrypt.compareSync(password , this.password);
}

adminUserSchema.methods.setRememberToken = function(res) {
    const token = uniqueString();
    res.cookie('remember_token' , token , { maxAge : 1000 * 60 * 60 * 24 * 90 , httpOnly : true , signed :true});
    this.update({ rememberToken : token } , err => {
        if(err) console.log(err);
    });
}


module.exports = mongoose.model('AdminUser', adminUserSchema)