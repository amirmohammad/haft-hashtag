const bcrypt = require('bcrypt')
const mongoose = require('mongoose')
const Schema = mongoose.Schema; 
const uniqueString = require('unique-string')
const mongoosePaginate = require('mongoose-paginate-v2');

const userSchema = Schema({
    name : { type : String  },
    phonenumber : { type : String , required : true, unique: true },
    birthdate : {type : String },
    gender : {type: String, default: "male"},
    personalCodeID: { type: String  },
    state: { type: String, default: null }, 
    city: { type: String, default: null }, 
    active : { type : Boolean ,  default : false },
    admin : { type : Boolean ,  default : 0 },
    email : { type : String },
    password : { type : String, default: null },
    rememberToken : { type : String , default : null },
    role : { type: String, default: "normalUser"}, // values: normalUser, newbie, company
    profilePicture: {type: String, default: 'static/user_default_icon.jpg'},
} , { timestamps : true , toJSON : { virtuals : true } });

userSchema.plugin(mongoosePaginate)

// userSchema.pre('save' , function(next) {
//     let salt = bcrypt.genSaltSync(15);
//     let hash = bcrypt.hashSync(this.password , salt);
//
//     this.password = hash;
//     next();
// });

// userSchema.pre('findOneAndUpdate' , function(next) {
//     let salt = bcrypt.genSaltSync(15);
//     let hash = bcrypt.hashSync(this.getUpdate().$set.password , salt);
//
//     this.getUpdate().$set.password = hash;
//     next();
// });

userSchema.methods.comparePassword = function(password) {
    return bcrypt.compareSync(password , this.password);
}

userSchema.methods.setRememberToken = function(res) {
    const token = uniqueString();
    res.cookie('remember_token' , token , { maxAge : 1000 * 60 * 60 * 24 * 90 , httpOnly : true , signed :true});
    this.update({ rememberToken : token } , err => {
        if(err) console.log(err);
    });
}


module.exports = mongoose.model('User', userSchema)