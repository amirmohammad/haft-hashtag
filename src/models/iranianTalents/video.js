const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const videoSchema = Schema({
    uri: {type: String, required: true},
    comment: [{type: Schema.Types.ObjectId, ref: "MaheKhandanVideoComment"}],
    category: [{type: Schema.Types.ObjectId, ref: "IranianTalentsVideoCategory"}],
    point: [{type: Schema.Types.ObjectId, ref: "IranianTalentsVideoCategory"}],
} , { timestamps : true, toJSON : { virtuals : true } }); 





module.exports = mongoose.model('IranianTalentsVideo' , videoSchema);