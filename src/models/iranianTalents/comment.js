const mongoose = require('mongoose'); 
const Schema = mongoose.Schema; 

const commentSchema = new Schema({
    text: {type: String, required: true },
    user: {type: Schema.Types.ObjectId, ref: "User"},
    video: {type: Schema.Types.ObjectId, ref: "IranianTalentsVideo"},
}, {timestamps: true});

module.exports = mongoose.model('IranianTalentsComment', commentSchema);