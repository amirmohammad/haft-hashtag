
const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// fullname, type, birthdate, gender, personalID, description
const eachPeopleSchema = Schema({
    user : { type : Schema.Types.ObjectId , ref : 'User' , required : true},
    fullname: {type: String},
    bithdate: {type: String},
    gender: {type: String},
    personalID: {type: String},
    description: {type: String},
    type: {type: String},
    image: {type: String},
    
} , { timestamps : true });


module.exports = mongoose.model('People' , eachPeopleSchema);