
const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// user, name, city, state, plan, jobTitle, description, plan
const appliedJobSchema = Schema({
    user : { type : Schema.Types.ObjectId , ref : 'User' , required : true},
    name: {type: String},
    city: {type: String},
    state: {type: String},
    plan : { type : Schema.Types.ObjectId , ref : 'BoughtPlan' , required : true},
    description: {type: String},
    jobTitle: {type: String},
} , { timestamps : true });


module.exports = mongoose.model('AppliedJob' , appliedJobSchema);