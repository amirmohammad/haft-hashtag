
const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// fullname, type, birthdate, gender, personalID, description
const plansSchema = Schema({
    name: {type: String, required: true, unique: true},
    fee: {type: String, required: true},
    days:{type: String, required: true},
} , { timestamps : true });


module.exports = mongoose.model('Plans' , plansSchema);