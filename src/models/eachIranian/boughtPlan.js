
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment-jalaali');

// fullname, type, birthdate, gender, personalID, description
const boughtPlanSchema = Schema({
    user : { type : Schema.Types.ObjectId , ref : 'User' , required : true},
    start: {type: String, default : moment().format('jYYYY/jMM/jDD HH:mm')},
    end: {type: String, required: true},
    plan: { type : Schema.Types.ObjectId , ref : 'Plans' , required : true},
} , { timestamps : true });


module.exports = mongoose.model('BoughtPlan' , boughtPlanSchema);