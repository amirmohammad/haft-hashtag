
const mongoose = require('mongoose'); 
const Schema = mongoose.Schema; 

const pointSchema = new Schema({
    rate: {type: String, required: true, default: 0 },
    user: {type: Schema.Types.ObjectId, ref: "User"},
    video: {type: Schema.Types.ObjectId, ref: "MaheKhandanVideo"},
}, {timestamps: true});

module.exports = mongoose.model('MaheKhandanPoint', pointSchema);