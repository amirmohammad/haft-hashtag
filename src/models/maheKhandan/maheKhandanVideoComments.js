
const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const maheKhandanSchema = Schema({
    text: {type: String, required: true},
    video: {type: Schema.Types.ObjectId, ref: "MaheKhandanVideo"},
    user: {type: Schema.Types.ObjectId, ref: "User"},
} , { timestamps : true }); 

maheKhandanSchema.virtual('comments' , {
    ref : 'MaheKhandanVideoComment',
    localField : '_id',
    foreignField : 'comment'
});

module.exports = mongoose.model('MaheKhandanVideoComment' , maheKhandanSchema);