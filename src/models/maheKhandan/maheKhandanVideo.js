const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const maheKhandanSchema = Schema({
    uri: {type: String, required: true},
    comment: [{type: Schema.Types.ObjectId, ref: "MaheKhandanVideoComment"}],
    thumbnail: {type: String,}
} , { timestamps : true, toJSON : { virtuals : true } }); 





module.exports = mongoose.model('MaheKhandanVideo' , maheKhandanSchema);