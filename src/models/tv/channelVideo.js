
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const channelVideoSchema = Schema({
    uri : { type : String , required : true},
    channel : {type: Schema.Types.ObjectId, ref: 'TVChannel', required: true},
    banner: {type: Boolean, default: false},
} , { timestamps : true , toJSON : { virtuals : true } });



module.exports = mongoose.model('TVChannelVideo' , channelVideoSchema);