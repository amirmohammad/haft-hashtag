const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// user, name, city, state, plan, jobTitle, description, plan
const tvApplySchema = Schema({
    user : { type : Schema.Types.ObjectId , ref : 'User' , required : true},
    name: {type: String},
    video: {type: String, required: true},
    description: {type: String},
} , { timestamps : true });


module.exports = mongoose.model('OtherTVApply' , tvApplySchema);