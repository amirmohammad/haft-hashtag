const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// user, name, city, state, plan, jobTitle, description, plan
const haftHashtagApplySchema = Schema({
    user : { type : Schema.Types.ObjectId , ref : 'User' , required : true},
    name: {type: String},
    request: {type: String},
    description: {type: String},
} , { timestamps : true });


module.exports = mongoose.model('HaftHashtagApply' , haftHashtagApplySchema);