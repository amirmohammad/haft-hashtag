
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const channelSchema = Schema({
    name : { type : String , required : true},
    // uri : { type : String , required : true},
    free : {type: Boolean, default: true},
    channelType: {type: String, default: 'exclusive'},
    active: {type: Boolean, default: true},
    image: {type: String, default: null}
} , { timestamps : true , toJSON : { virtuals : true } });



module.exports = mongoose.model('TVChannel' , channelSchema);