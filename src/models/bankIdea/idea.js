
const mongoose = require('mongoose'); 
const { schema } = require('../user');
const Schema = mongoose.Schema; 

const ideaSchema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: "User"},
    type: {type: String, required: true},
    firstname: {type: String},
    lastname: {type: String},
    request: {type: String, required: true},
    description: {type: String, required: true},
    category: {type: Schema.Types.ObjectId, ref: "BankIdeaCategory"},
}, {timestamps: true});

module.exports = mongoose.model('Idea', ideaSchema);