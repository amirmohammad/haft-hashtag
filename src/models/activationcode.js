const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const activationCode = Schema({
    code : { type : String , required : true} ,
    used : { type : Boolean , default : false },
    expire : { type : String , required : true },
    phoneNumber: {type: String, required: true}
} , { timestamps : true }); 


module.exports = mongoose.model('ActivationCode' , activationCode);