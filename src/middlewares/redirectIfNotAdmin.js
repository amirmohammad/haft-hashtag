const middleware = require('./middleware');

class redirectIfNotAdmin extends middleware {
    
    // handle(req , res ,next) {
    //     if(req.isAuthenticated() && req.user.admin)
    //         return next();
    
    //     return res.redirect('/');
    // }

    handle(req, res, next) {
        // console.log(req.user.admin)
        if(! req.isAuthenticated() || !req.user.admin){
            return res.redirect('/auth/login');
        } 
        return next()
    }

}


module.exports = new redirectIfNotAdmin();