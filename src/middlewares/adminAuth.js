// const User = require('app/models/user');
const User = require('../models/user');
const middleware = require('./middleware');
const passport = require('passport');

class authenticateApi extends middleware {
    
    handle(req , res , next) {
        if(! req.user.admin) 
            return res.status(401).json({
                data :'شما دسترسی ندارید',
                status : 'error'
            })
        else
            next();
    }


}


module.exports = new authenticateApi();