const multer  = require('multer')
const mkdirp  = require('mkdirp');

const VideoStorage = multer.diskStorage({
    destination : (req , file , cb) => {
        let year = new Date().getFullYear();
        let month = new Date().getMonth() + 1;
        let day = new Date().getDay();
        let dir = `./uploads/videos/tv/channel/${year}/${month}/${day}`;

        mkdirp(dir , err => cb(err , dir))
    },
    filename: (req , file , cb) => {
        cb(null, new Date().toISOString() +  '-' + file.originalname ) 
    }
});

const VideoFilter = (req , file , cb) => {
    if(file.mimetype === "video/mp4") {
        cb(null , true)
    } else {
        cb(null , false)
    }
}

const uploadVideo = multer({ 
    storage : VideoStorage,
    limits : {
        fileSize : 1024 * 1024 * 50 //50mb
    },
    fileFilter : VideoFilter
})


module.exports = {
    uploadVideo
}