
const AppliedJobs = require('../../models/eachIranian/jobs')

const allAppliedJobs = async () => await AppliedJobs.find({})

module.exports = {
    allAppliedJobs,
}