const User = require('../../models/user')
const kaveNegar = require('kavenegar_promise')
const {kavenegar_token, messages} = require('../../config')

const smsLogin = require('../../models/smsLogin')

const api = kaveNegar.KavenegarApi({
    apikey: kavenegar_token
})

const userInfo = async (phonenumber) => {
    return await User.findOne({phonenumber});
}

const sendLoginSMS = async (phoneNumber, token) => {
    const message = messages.sendLoginMessage + token;
    return await api.Send({
        message: message,
        // sender: "10004346",
        receptor: phoneNumber
    });
}




function rand(min = 1000, max = 9999) {  
    let randomNum = Math.random() * (max - min) + min;  
    return Math.floor(randomNum);
}

const saveSMSLog = async (phoneNumber, code) => {
    const newLog = new smsLogin({
        phoneNumber, code
    })

    const saved = await newLog.save()
    return saved;
}

const isNumberExists = async (phoneNumber, code) => {
    const time = new Date(); 
    const isExists = await smsLogin.findOne({$and: [{phoneNumber}, {used: false}, {code}]});
    if(!isExists) return false
    else return true
}

module.exports = {
    userInfo,
    sendLoginSMS,
    rand,
    saveSMSLog,
    isNumberExists,
}