const channel = require('../../models/tv/channel');
const Channel = require('../../models/tv/channel')
const Video = require('../../models/tv/channelVideo')

const allChannels = async () => {
    return await Channel.find({}, 'id name');
}

const info = async (channelID) => {
    return await Channel.findById(channelID);
}

const allVideos = async (channel) => {
    return await Video.find({channel}, 'uri')
}

const channelName = async (id) => {
    return await Channel.findById(id, 'name');
}

const isChannelNameUnique = async (name, id) => {
    const channel = await Channel.findOne({name})
    if(!channel) return true;
    else if(channel._id == id) return true
    else return false;
}

const updateChannel = async (id, free, channelType, image, name ) => {
    return await Channel.updateOne({_id: id}, {free, channelType, image, name})
}

const isChannelUnique = async (name) => {
    const channel = await Channel.findOne({name});
    if(!channel) return true;
    else return false;
}

const createChannel = async (name, free, channelType, image ) => {
    const newChannel = new Channel({name, free, channelType, image})
    const saved = await newChannel.save()
    if(!saved) return false;
    else return saved;
}

const deleteChannelVideos = async (id) => {
    return await Video.deleteMany({channel: id});
}

const isChannelExists = async (id) => {
    const channel = await Channel.findById(id)
    if(!channel) return false;
    else return true;
}

const deleteChannel = async (id) => {
    return await Channel.deleteOne({_id: id})
}

const uploadVideo = async (channelID, video) => {
    const newVideo = new Video({
        uri: video,
        channel: channelID
    })
    const saved = newVideo.save()
    if(!saved) return false
    return saved;
}

const getVideo = async (id) => {
    return await Video.findById(id, 'uri')
}


const isVideoExists = async (id) => {
    const video = await Video.findById(id)
    if(video) return true
    else return false;
}

const deleteVideo = async (id) => {
    return await Video.deleteOne({_id: id})
}

const findChannelFromVideo = async (id) => await Video.findById(id, 'channel')

const setBanner = async (video) => await Video.updateOne({ _id: video}, { banner: true });

const removeBanner = async () => await Video.updateOne( {banner: true}, {banner: false})

module.exports = {
    allChannels,
    info,
    allVideos,
    channelName,
    isChannelNameUnique,
    updateChannel,
    isChannelUnique,
    createChannel,
    isChannelExists,
    deleteChannelVideos,
    deleteChannel,
    uploadVideo,
    getVideo,
    isVideoExists,
    deleteVideo,
    findChannelFromVideo,
    setBanner, 
    removeBanner, 
}