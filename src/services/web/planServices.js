const Plan = require('../../models/eachIranian/plans')


const createPlan = async (name, fee, days) => {

    const newPlan = new Plan({name, fee, days});
    const saved = await newPlan.save();

    return saved; 
}

const allPlans = async () => await Plan.find({});

const isPlanExists = async (name) => (await Plan.findOne({name})) ? true : false;

const plan = async (id) => await Plan.findById(id) 

const updatePlan = async (id, name, fee, days) => await Plan.updateOne({_id: id}, {name, fee, days}) 

const findPlanByID = async (id) => await Plan.findById(id);

const deletePlan = async (id) => await Plan.deleteOne({_id: id})

module.exports = {
    createPlan,
    allPlans,
    isPlanExists,
    plan,
    updatePlan,
    findPlanByID, 
    deletePlan, 
    
}