
const User = require('../../models/user')

const last100Users = async () => User.find({}).sort({'createdAt': -1}).limit(10);

const user = (id) => User.findById(id);

const isUserExists = async (id) => (await User.findById(id)) ? true : false;

const deleteUser = (id) => User.deleteOne({ _id: id});

module.exports = {
    last100Users,
    user,
    isUserExists,
    deleteUser,
}