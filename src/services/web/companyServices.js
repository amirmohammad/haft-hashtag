const Category = require('../../models/jobsDB/category')
const Company = require('../../models/jobsDB/company')
const Gallery = require('../../models/jobsDB/companyImages')
const Certificate = require('../../models/jobsDB/companyCertificate')

const allCategories = () => Category.find({})

const category = (id) => Category.findById(id)

const companies = (id) => Company.find({category: id})

const isCategoryExists = async (name) => (await Category.findOne({name})) ? true : false ;

const saveCategory = async (name) => {
    const newCategory = new Category({name})
    return await newCategory.save();
}

const updateCategoryName = async (category, name) => await Category.updateOne({_id: category}, {name});

const deleteCompanies = (category) => Company.deleteMany({category})

const deleteCategory = (category) => Category.deleteOne({ _id: category })

const company = (compnay) => Company.findById(compnay).populate('owner');

const isCompanyExists = async (company) => (await Company.findById(company)) ? true : false; 

const deleteCompany = (company) => Company.deleteOne({_id: company}); 

const findCategory = async (id) => await Company.findById(id, 'category')

module.exports = {
    allCategories,
    category,
    companies,
    isCategoryExists,
    saveCategory,
    updateCategoryName,
    deleteCompanies,
    deleteCategory,
    company,
    isCompanyExists,
    deleteCompany,
    findCategory,
}