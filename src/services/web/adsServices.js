
const Ads = require('../../models/advertisement')

const uploadAds = async (bannerImage, link, description, wallpaper=false) => {
    const newAds = new Ads({bannerImage, link, description, wallpaper})
    return await newAds.save();
}

const allAds = async () => {
    return await Ads.find({},'bannerImage')
}

const adsInfo = async (id) => {
    return await Ads.findById(id)
}

const removeAds = async (id) => {
    return await Ads.deleteOne({_id: id});
}

module.exports = {
    uploadAds,
    allAds,
    adsInfo,
    removeAds
}