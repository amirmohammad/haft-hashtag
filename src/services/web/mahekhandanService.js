const Category = require('../../models/maheKhandan/categories')
const Charity = require('../../models/maheKhandan/charity')
const Video = require('../../models/maheKhandan/maheKhandanVideo')
const Comment = require('../../models/maheKhandan/maheKhandanVideoComments')
const Point = require('../../models/maheKhandan/point');

const allCategories = () => {
    return Category.find({});
}

const allVideos = () => {
    return Video.find({});
}

const uploadVideo = async (path, thumbnail) => {
    const newVideo = new Video({ uri: path, thumbnail })
    const saved = await newVideo.save();
    if(!saved) return false;
    return saved;
}

const video = (uri) => {
    return Video.findById(uri)
}

const remove = (id) => {
    return Video.deleteOne({_id: id})
}

const comments = async (video) => {
    return await Comment.find({video}).populate('user', 'name phonenumber')
}

const removeComment = (id) => {
    return Comment.deleteOne({_id: id});
}

const allReactions = async (video) => {
    const reactions = await (await Point.find({video}, 'rate')).map( obj => obj.rate);
    
    const zeros = await reactions.filter(reaction => reaction == '0')
    const ones = await reactions.filter(reaction => reaction == '1')
    const twos = await reactions.filter(reaction => reaction == '2')
    const threes = await reactions.filter(reaction => reaction == '3')

    return {
        zeros: zeros.length,
        ones: ones.length,
        twos: twos.length,
        threes: threes.length
    }
    
}


module.exports = {
    allCategories, 
    allVideos,
    uploadVideo,
    video,
    remove,
    comments,
    removeComment,
    allReactions,
}