
const { update } = require('../../models/bankIdea/category');
const Category = require('../../models/bankIdea/category');

async function createCategoryService (category){

    console.log(category)
    const isCategoryExists = await Category.findOne({name: category});

    if(isCategoryExists != null) {
        return result = {
            success: false,
            description: "category exists"
        }
    }

    const newCategory = new Category({name: category})
    const saved = newCategory.save()

    if(!saved) {
        return result = {
            success: false,
            description: "category save error"
        }
    }

    return result = {
        success: true,
        description: saved
    }
}

async function updateCategoryService (oldname, newname){
    const isOldNameExists = await Category.findOne({name: oldname})

    if(!isOldNameExists) {
        return reuslt = {
            success: false,
            description: 'old name not exists'
        }
    }

    const updated = await Category.updateOne({name: oldname}, {name: newname})

    if(!updated) {
        return result = {
            success: false,
            description: 'update error'
        }
    }

    return result = {
        success: true,
        description: updated
    }
}

async function seeCategoryService (){
    const result = await Category.find({}, 'name');

    return result
}

async function deleteCategoryService (category){
    
    const isCategoryExists = await Category.findOne({name: category})

    if(!isCategoryExists){
        return result = {
            success: false,
            description: 'category not exists'
        }
    }

    const deleted = await Category.deleteOne({name: category})
    if(!deleted) {
        return result = {
            success: false,
            description: 'delete failed'
        }
    }

    return result = {
        success: true,
        description: deleted
    }
}

module.exports = {
    createCategoryService,
    updateCategoryService,
    seeCategoryService,
    deleteCategoryService
}