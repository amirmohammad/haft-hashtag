
const Category = require('../../models/bankIdea/category')
const Idea = require('../../models/bankIdea/idea')

async function saveIdeaService(id, firstname, lastname, type, category, request, description){

    const isCategoryExists = await Category.findById(category)

    if(!isCategoryExists) {
        return result= {
            success: false,
            description: 'category not exists'
        }
    }

    const newIdea = new Idea({
        user: id,
        firstname,
        lastname,
        type,
        request,
        description,
        category
    })

    const saved = await newIdea.save()

    if(!saved) {
        return result= {
            success: false,
            description: 'save failed'
        }
    }
    
    return result= {
        success: true,
        description: saved
    }

}

async function getIdeaService(category){
    const isCategoryExists = await Category.findById(category)

    if(! isCategoryExists){
        return result = {
            success: false,
            description: "category not exists"
        }
    }

    const ideas = await Idea.find({category}).populate('category', 'name')
    
    return result = {
        success: true,
        description: ideas,
    }
}

module.exports = {
    saveIdeaService,
    getIdeaService,
}