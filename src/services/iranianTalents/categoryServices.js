const Category = require('../../models/iranianTalents/category')
const Video = require('../../models/iranianTalents/video')

const createCategory = async (category) => {
    const isCategoryExists = await Category.findOne({name: category})
    if(isCategoryExists) {
        return result = {
            success: false,
            description: 'category exists',
            status: 400
        }
    }

    const newCategory = new Category({name: category})
    const saved = await newCategory.save()

    if(!saved) {
        return result = {
            success: false,
            description: 'error in saving category',
            status: 500
        }
    }
    return result = {
        success: true,
        description: saved,
        status: 200
    }
}

const getCategoies = async (category) => {
    const categories = await Category.find({}, 'name')

    return categories}

const updateCategoy = async (oldCategory, newCategory) => {

    const isOldCategoryExists = await Category.findOne({name: oldCategory})
    if(!isOldCategoryExists) {
        return result = {
            success: false,
            description: 'cateogry not found ',
            status: 404
        }
    }

    const updated = await Category.updateOne({name: oldCategory}, {name: newCategory})

    if(!updated) {
        return result = {
            success: false,
            description: 'cateogry not saved',
            status: 500
        }
    }

    return result = {
        success: true,
        description: updated,
        status: 200
    }
}

const deleteCategoy = async (category) => {

    const isCategoryExists = await Category.findOne({name: category})

    if(!isCategoryExists){
        return result = {
            success: false,
            status: 404,
            description: 'category not found'
        }
    }

    const deleted = await Category.deleteOne({name: category})
    if(!deleted){
        return result = {
            success: false,
            status: 500,
            description: 'error in deleting category'
        }
    }

    return result = {
        success: true,
        status: 200,
        description: deleted
    }
}

const getCategoyVideos = async (category) => {
    const isCategoryExists = await Category.findById(category)
    if(!isCategoryExists) {
        return result = {
            success: false, 
            description: 'category not found',
            status: 404
        }
    }

    const videos = await Video.find({category}, 'uri category')

    return videos
}

module.exports = {
    createCategory, 
    getCategoies,
    updateCategoy,
    deleteCategoy,
    getCategoyVideos,
}