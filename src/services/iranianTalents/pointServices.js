const Video = require('../../models/iranianTalents/video')
const Point = require('../../models/iranianTalents/point')


const sendPoint = async (user, video, point) => {
    const isVideoExists = await Video.findById(video)
    if(!isVideoExists) {
        return result = {
            success: false,
            status: 404,
            description: "video not found"
        }
    }

    // user has pointed before
    const isPointedBefore = await Point.findOne({user})
    if(isPointedBefore){
        const updatedPoint = await Point.updateOne({_id: isPointedBefore._id}, {rate: point})
        return result = {
            success: true,
            status: 200,
            description: updatedPoint
        }
    }

    const newPoint = new Point({
        video,
        user, 
        rate: point
    })

    const saved = await newPoint.save()
    if(!saved) {
        return result = {
            success: false,
            status: 500,
            description: "error in saving video"
        }
    }

    return result = {
        success: true,
        status: 200,
        description: saved
    }
}

const getPointsForVideo = async (video) => {
    const isVideoExists = await Video.findById(video)
    if(!isVideoExists) {
        return result = {
            success: false,
            status: 404,
            description: "video not found"
        }
    }

    const points = await Point.find({video}, 'rate')
    return result = {
        success: true,
        status: 200,
        description: points
    }
}

const getPoint = async (user, video) => {
    const isVideoExists = await Video.findById(video)
    if(!isVideoExists) {
        return result = {
            success: false,
            status: 404,
            description: "video not found"
        }
    }

    const point = await Point.findOne({video, user}, 'rate')

    return result = {
        success: true,
        status: 200,
        description: point
    }
}


module.exports = {
    sendPoint, getPointsForVideo, getPoint,
}