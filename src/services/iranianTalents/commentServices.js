const Video = require('../../models/iranianTalents/video')
const Comment = require('../../models/iranianTalents/comment')

const sendComment = async (user, video, comment) => {
    const isVideoExists = await Video.findById(video)

    if(!isVideoExists) {
        return result = {
            success: false,
            description: "video not found",
            status: 404
        }
    }

    const newComment = new Comment({
        text: comment,
        video,
        user,
    })

    const saved = await newComment.save()
    if(! saved) {
        return result = {
            success: false,
            description: "error in saving comment",
            status: 500
        }
    }
    const commentsOfVideo = await Comment.find({video})

    let arrayOfIds = [];

    for(let c of commentsOfVideo){
        arrayOfIds.push(c._id)
    }

    const updateVideoComments = await Video.updateOne({_id: video}, {comment: arrayOfIds})

    if(!updateVideoComments) {
        return result = {
            success: false,
            description: "error in updating video",
            status: 500
        }
    }

    return result = {
        success: true,
        description: "done",
        status: 200
    }

}

const deleteComment = async (user, comment) => {
    const isCommentExists = await Comment.findById(comment)
    
    if( !isCommentExists ) {
        return result = {
            success: false,
            status: 404,
            description: 'comment not found'
        }
    }
    // if user can update his comment
    const isUserAllowed = (JSON.stringify(isCommentExists.user) == JSON.stringify(user) ) ? true : false;
    if(!isUserAllowed) {
        return result = {
            success: false,
            status: 403,
            description: 'you have no access'
        }
    }

    const deleted = await Comment.deleteOne({_id: comment})
    if(!deleted){
        return result = {
            success: false,
            status: 500,
            description: 'error in deleting comment'
        }
    } 

    const commentsOfVideo = await Comment.find({video: isCommentExists.video}, '_id');

    let array = []
    for(let c of commentsOfVideo){
        array.push(c._id)
    }

    const updatedVideo = await Video.updateOne({_id: isCommentExists.video}, {comment: array})

    if(!deleted){
        return result = {
            success: false,
            status: 500,
            description: 'error in updating video comments'
        }
    }


    return result = {
        success: true,
        status: 200,
        description: 'done'
    }
}

const updateComment = async (user ,comment, newComment) => {

    const isCommentExists = await Comment.findById(comment)
    if(!isCommentExists) {
        return result = {
            success: false,
            status: 404,
            description: 'comment not found'
        }
    }

    // if user can update his comment
    const isUserAllowed = (JSON.stringify(isCommentExists.user) == JSON.stringify(user) ) ? true : false;
    if(!isUserAllowed) {
        return result = {
            success: false,
            status: 403,
            description: 'you have no access'
        }
    }

    const updatedComment = await Comment.updateOne({_id: comment}, {text: newComment})
    if(!updatedComment) {
        return result = {
            success: false,
            status: 500,
            description: 'error in updating comment'
        }
    }
    return result = {
        success: true,
        status: 200,
        description: updatedComment
    }
}

const getCommentsForVideo = async (video) => {
    const isVideoExists = await Video.findById(video);
    if(!isVideoExists) {
        return result = {
            success:false,
            status: 404,
            description: 'video not found'
        }
    }

    const comments = await Comment.find({video}, 'user text')
        .populate('user', 'name')

        return result = {
            success: true,
            status: 200,
            description: comments
        }

}


module.exports = {
    sendComment, deleteComment, updateComment, getCommentsForVideo
}