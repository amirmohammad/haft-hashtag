
const Category = require('../../models/iranianTalents/category')
const Video = require('../../models/iranianTalents/video')

const sendVideo = async (category, video) => {
    const isCategoryExists = await Category.findById(category)
    if(!isCategoryExists) {
        return result = {
            success: false,
            status: 404,
            description: 'category not exists'
        }
    }

    const newVideo = new Video({
        uri: video,
        category: category
    })

    const saved = await newVideo.save()
    if(!saved) {
        return result = {
            success: false,
            status: 500,
            description: 'error in saving video'
        }
    }

    return result = {
        success: true,
        status: 200,
        description: saved
    }
}

const deleteVideo = async (video) => {
    
    const isVideoExists = await Video.findById(video)
    if(! isVideoExists ){
        return result = {
            success: false,
            description: 'video not found',
            status: 404
        }
    }

    const deleted = await Video.deleteOne({_id: video})

    if(! deleted ){
        return result = {
            success: false,
            description: 'unable to delete video',
            status: 500
        }
    }

    return result = {
        success: true,
        description: deleted,
        status: 200
    }
}

const getVideo = async (video) => {

    const get = await Video.findById(video, 'uri category')

    if(!get){
        return result = {
            success: false,
            status: 404,
            description: 'video not found'
        }
    }

    return result = {
        success: true,
        status: 200,
        description: get
    }
}


module.exports = {
    sendVideo, deleteVideo, getVideo
}