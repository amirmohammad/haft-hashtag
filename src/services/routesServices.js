const Channel = require('../models/tv/channel')
const Ads = require('../models/advertisement');

const getTVChannels = async () => {
    return await Channel.find({active: true}, 'name image id');
}

const getAds = async () => {
    const adsWallpaper = await Ads.find({wallpaper: true}).sort({createdAt: -1});

    return adsWallpaper[0]
}

const getTVWallPaper = async () => {
    const channels = await Channel.find({});

    const random = Math.floor(Math.random() * channels.length);

    const randomWallpaper = channels[random].image;
    return randomWallpaper
}

module.exports = {
    getTVChannels,
    getAds,
    getTVWallPaper,
}