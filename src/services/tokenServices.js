const kaveNegarService = require('./kavenegarServices')
const ActivationCode = require('../models/activationcode')
const moment = require('moment-jalaali');

const User = require('../models/user')

async function sendSMSService(number){
    
    const code = rand()
    console.log(code)
    const sendSMS = await kaveNegarService.sendSMS(number, code)
    const expire = moment().add(1, 'day').format('jYYYY/jMM/jDD HH:mm')
    
    const newActivation = new ActivationCode({
        code,
        expire,
        phoneNumber: number
    })

    const saved = await newActivation.save()
    if(!saved){
        return result = {
            success: false,
            description: 'error in saving'
        }
    }

    return result = {
        success: true,
        description: sendSMS
    }
    
}

function rand(min = 1000, max = 9999) {  
    let randomNum = Math.random() * (max - min) + min;  
    return Math.floor(randomNum);
}

async function verifyTokenSVC(token, phoneNumber){

    const isCodeExists = await ActivationCode.find({phoneNumber}).sort({createdAt:-1})
 
    if(isCodeExists.length == 0) {
        return result = {
            success: false,
            description: 'sms not found in database',
            status: 404,
        }
    }

    if(isCodeExists[0].used){
        return result = {
            success: false,
            description: 'token has been used before',
            status: 406,
        }
    }

    // if(isCodeExists[0].code != token) {
    if(token.localeCompare(isCodeExists[0].code) != 0) {

        return result = {
            success: false,
            description: 'token you have entered is incorrect',
            status: 406,
        }
    }

    const updateToken = await ActivationCode.updateOne({_id: isCodeExists[0]._id}, {used: true});
    
    if(!updateToken){
        return result = {
            success: false,
            description: 'error in updating token',
            status: 500,
        }
    }

    else{
        return result = {
            success: true,
            description: 'token verifed',
            status: 200,
        }
    }

    
}

const isUserExists = async (phonenumber) => {
    return await User.findOne({phonenumber})
}

const deletePhoneNumberifExists = async (phoneNumber) => await ActivationCode.deleteMany({phoneNumber})

const sms = async (phoneNumber, token) => await kaveNegarService.sendSMS(phoneNumber, token)

const saveTokenInDB = async (phoneNumber, code, expire) => {
    const newActivation = new ActivationCode({phoneNumber, code, expire})
    const saved = await newActivation.save();
    return saved ? true : false; 
}

const phoneNumberData = async (phoneNumber) => await ActivationCode.findOne({phoneNumber})

const updateActivation = async (phoneNumber) => await ActivationCode.updateOne({phoneNumber}, {used: true});

module.exports = {
    sendSMSService,
    verifyTokenSVC,
    isUserExists,
    deletePhoneNumberifExists,
    sms,
    saveTokenInDB,
    phoneNumberData,
    updateActivation,
}