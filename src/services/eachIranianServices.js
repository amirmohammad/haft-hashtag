
const People = require('../models/eachIranian/people')
const Plan = require('../models/eachIranian/plans')
const moment = require('moment-jalaali')
const ObjectId = require('mongoose').Types.ObjectId;
const BoughtPlan = require('../models/eachIranian/boughtPlan');
const AppliedJob = require('../models/eachIranian/jobs');

async function uploadPeopleService(userid, fullname, type, birthdate, gender, personalID, description, image ){
    
    const isPeopleExists = await People.findOne({personalID})
    if(isPeopleExists != null) {
        return result = {
            success: false,
            description: 'user has saved once'
        }

    }

    const newPeople = new People({
        user: userid,
        personalID,
        gender,
        fullname,
        type,
        birthdate,
        description,
        image
    })

    const saved = await newPeople.save()
    if(!saved) {
        return result = {
            success: false,
            description: 'error in save people'
        }
    }
    return result = {
        success: true,
        description: saved
    }

}

async function getPeopleService(people){

    const found = await People.findById(people, 'personalID gender fullname type description')

    if(!found){
        return result = {
            success: false,
            description: 'not found'
        }
    }

    return result = {
        success: true,
        description: found
    }

}

async function createPlanSVC(name, days, fee) {

    const isNameExists = await Plan.find({name}) || null;

    if (isNameExists.length != 0) {
        return result = {
            success: false, 
            description: 'plan name exists'
        }
    } 
    else{
        const newPlan = new Plan({name, days, fee})
        const saved = await newPlan.save()
    
        if(!saved){
            return result = {
                success: false, 
                description: 'error in saving'
            }
        }
    
        return result = {
            success: true, 
            description: saved
        }
    }

    
}

async function seePlanSVC(){
    const plans = await Plan.find({}, 'name days fee')
    return plans
}

async function updatePlanSVC(id, name, days, fee){
    
    const isPlanExists = await Plan.findById(id)

    if(!isPlanExists){
        return result = {
            success: false,
            description: 'plan not exists'
        }
    }

    const updated = await Plan.updateOne({_id: id}, {name, days, fee})

    return result = {
        success: true,
        description: updated
    }
}

async function deletePlanSVC(id){
    const isPlanExists = await Plan.findById(id)

    if(!isPlanExists) {
        return result = {
            success: false,
            description: 'plan not exists'
        }
    }

    const deleted = await Plan.deleteOne({_id: id});

    return result = {
        success: true,
        description: deleted
    }
}

async function buyPlanSVC(plan, userid){

    if(! await ObjectId.isValid(plan)) {
        return result = {
            success: false,
            description: 'plan is not objectID'
        }
    }
    const isPlanExists = await Plan.findOne({_id: plan})

    if(!isPlanExists) {
        return result = {
            success: true,
            description: 'plan not exists'
        }
    }
    

    // plan exists and its time to save plan
    const plans = await BoughtPlan.find({user: userid}).sort({createdAt:-1})
    if(plans.length == 0){
        // save plan for now
        // const newBouth
        // const startDate = moment().format('jYYYY/jMM/jDD HH:mm')
        let endPlan = moment().add(isPlanExists.days , 'day').format('jYYYY/jMM/jDD HH:mm')

        let newBought = new BoughtPlan({user: userid, end: endPlan, plan: isPlanExists._id})
        let saved = await newBought.save()
        
        if(!saved){
            return result ={
                success: false,
                description: "saved failed"
            }
        }

        return result ={
            success: true,
            description: saved
        }

    }

    let startPlan = plans[0].end;
    let endPlan = moment(startPlan, 'jYYYY/jMM/jDD HH:mm').add(isPlanExists.days, 'day').format('jYYYY/jMM/jDD HH:mm')  
    let newBought = new BoughtPlan({user: userid, start: startPlan, end: endPlan, plan: isPlanExists._id})
    let saved = await newBought.save()

    if(!saved){
        return result ={
            success: false,
            description: "saved failed"
        }
    }

    return result ={
        success: true,
        description: saved
    }
}

async function boughtPlan(id){
    const plans = await BoughtPlan.find({user: id}, 'start end plan user').sort({createdAt: 1})
        .populate('user', 'name');
    return plans;
}

async function applyForJobSVC(user, name, city, state, plan, jobTitle, description){

    if(! await ObjectId.isValid(plan)) { 
        return result = {
            success: false,
            description: 'plan id is incorrect' 
        }
    }

    // check is applied before 
    const isAppliedBefore = await AppliedJob.find({user}).sort({createdAt: -1});

    if(isAppliedBefore.length != 0) {
        const expireAppliedDate = moment(isAppliedBefore[0].createdAt).add(7, 'day').format('jYYYY/jMM/jDD HH:mm')
        const lastupload= moment(isAppliedBefore[0].createdAt).format('jYYYY/jMM/jDD HH:mm')
        
        const canUserApply = (expireAppliedDate > lastupload) ? false : true;
        if(!canUserApply){
            return result = {
                success: false,
                description: 'you have applied in the last 7 days'
            }
        }
    }
    
    console.log(plan)
    const isActive = await BoughtPlan.findOne({_id: plan});

    if(!isActive) {
        return result = {
            success: false,
            description: 'plan not exists'
        }
    }
     
    // const compare = Date(moment(isActive.end, 'jYYYY/jMM/jDD HH:mm'))  - Date(Date.now())
    const d1 = new Date(moment(isActive.end, 'jYYYY/jMM/jDD HH:mm').format('jYYYY/jMM/jDD HH:mm'))
    const d2 = new Date(moment().format('jYYYY/jMM/jDD HH:mm')) 
    const compare = (d1 > d2) ? true : false;

    if(!compare) {
        return result = {
            success: false,
            description: 'plan expired'
        }
    }

    // const planID = (isPlanExists.length != 0) ? (freePlan[0]._id) : (isPlanExists.id)

    const newApplied = new AppliedJob({user, name, city, state, jobTitle, description, plan})

    const saveApply = await newApplied.save();

    if( !saveApply) {
        return result = {
            success: false,
            description: 'error in saving apply'
        }
    }

    return result = {
            success: true,
            description: saveApply,
        }
}


const boughtPlans = async (id) =>  await BoughtPlan.find({user: id}).populate('plan', 'name')



module.exports = {
    uploadPeopleService,
    getPeopleService,
    createPlanSVC,
    seePlanSVC,
    updatePlanSVC,
    deletePlanSVC,
    buyPlanSVC,
    boughtPlan,
    applyForJobSVC,
    boughtPlans,
}