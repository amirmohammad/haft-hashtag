const Channel = require('../../models/tv/channel')
const Video = require('../../models/tv/channelVideo')

const getVideoSVC = async (video) =>{
    const isVideoExists = await Video.findById(video, 'uri channel')
        .populate('channel', 'name')

    if(!isVideoExists) {
        return result = {
            success: false,
            status: 404,
            description: 'video not found'
        }
    }

    return result = {
        success: true,
        status: 200,
        description: isVideoExists
    }
}

const deleteVideoSVC = async (video) =>{
   
    const deleted = await Video.deleteOne({_id: video})
    return result ={
        success: true,
        status: 200,
        description: deleted
    }
    
}

const uploadVideoSVC = async (channel, video) =>{

    const isChannelExists = await Channel.findOne({name: channel})
    if(!isChannelExists){
        return result = {
            success: false,
            status: 404,
            description: 'channel not found'
        }
    }

    const newVideo = new Video({
        uri: video,
        channel: isChannelExists._id,
    })

    const saved = await newVideo.save();

    if(!saved){
        return result = {
            success: false,
            status: 500,
            description: 'error in saving video'
        }
    }

    return result = {
        success: true,
        status: 200,
        description: saved
    }

}

const getVideosOfChannelSVC = async (channel) =>{
    const isChannelExists = await Channel.findById(channel)
    if(!isChannelExists) {
        return result = {
            success: true,
            status: 404,
            description: 'channel not found'
        }
    }

    const videos = await Video.find({channel}, 'uri');

    return videos;

}

const bannerVideo = async () => await Video.findOne({ banner: true }, 'uri');


module.exports = {
    getVideosOfChannelSVC, getVideoSVC, uploadVideoSVC, deleteVideoSVC,
    bannerVideo
}
