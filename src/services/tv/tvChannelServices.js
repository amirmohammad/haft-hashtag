const Channel = require('../../models/tv/channel')
const Video = require('../../models/tv/channelVideo')

const createChannelSVC = async (channel, free=true, channelType='exclusive', image) =>{
    const isChannelExists = await Channel.findOne({name: channel});
    if(isChannelExists) {
        return result = {
            success: false, 
            status: 400,
            description: 'channel has created before'
        }
    }

    const newChannel = new Channel({name: channel, free, channelType, image})
    const saved = await newChannel.save()

    if(!saved) {
        return result = {
            success: false,
            status: 500,
            description: 'error in saving result'
        }
    }

    return result = {
        success: true,
        status: 200,
        description: saved
    }
}

const getChannelSVC = async (channel) =>{
    return await Video.find({channel}, 'uri channel').sort({createdAt: -1})
        .populate('channel', 'name')
}

const getChannelsSVC = async () =>{
    const channels = await Channel.find({}, 'free channelType image name');
    return channels.filter( channel => channel.name != 'banner')
}

const updateChannelSVC = async (channel, newChannel, image) =>{
    
    const isChannelExists = await Channel.findById(channel);
    if(!isChannelExists) {
        return result = {
            success: false, 
            status: 404,
            description: 'channel not found '
        }
    }

    const updated = await Channel.updateOne({_id: channel}, {name: newChannel, image: image});

    if(!updated) {
        return result = {
            success: false, 
            status: 500,
            description: 'error in updting channel'
        }
    }

    return result = {
        success: true, 
        status: 200,
        description: updated
    } 

}

const deleteChannelSVC = async (channel) =>{
    const isChannelExists = await Channel.findById(channel);
    if(!isChannelExists) {
        return result = {
            success: false, 
            status: 404,
            description: 'channel not found '
        }
    }

    const deleteVideos = await Video.deleteMany({channel})
    
    const deleteChannel = await Channel.deleteOne({_id: channel})

    return result = {
        status: 200,
        success: true,
        description: {deleteVideos, deleteChannel}
    }
}

const exclusivesSVC = async () => {
    return await Channel.find({channelType: 'exclusive'}, 'name id free')
}

const isVideoExists = async (id) => await (await Video.findById(id)) ? true : false;


module.exports = {
    createChannelSVC, getChannelsSVC, getChannelSVC, updateChannelSVC, deleteChannelSVC, exclusivesSVC,
    isVideoExists, 
    
}