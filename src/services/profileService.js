const User = require('../models/user')
const Company = require('../models/jobsDB/company')
const CompanyGallery = require('../models/jobsDB/companyImages')
const CompanyCertificate = require('../models/jobsDB/companyCertificate')


const isUserRoleCompany = async (user) => {
    const userinfo = await User.findById(user);
    if(!userinfo) return false;
    return (await userinfo.role == 'company') ? true : false;
}

const getCompanyFromToken = async (id) =>  await Company.findOne({owner: id});

const companyGallery = async (id) => await CompanyGallery.find({company: id})

const companyCertificate = async (id) => await CompanyCertificate.find({ company: id });

module.exports = {
    isUserRoleCompany,
    getCompanyFromToken,
    companyGallery,
    companyCertificate
}

