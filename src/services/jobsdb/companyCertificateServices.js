const { Result } = require('express-validator')
const { CommandCursor } = require('mongodb')
const company = require('../../models/jobsDB/company')
const Company = require('../../models/jobsDB/company')
const CompanyCertificate = require('../../models/jobsDB/companyCertificate')

async function uploadCertificateService(images, company){

    // check if company exists
    const isCompanyExists = await Company.findById(company)

    if(!isCompanyExists) {
        return result = {
            success: false,
            description: 'company not exists'
        }
    }

    let array = []
    for (let i of images) {
        let saveCertificateImage = await saveOneImage(i, company)
        array.push(saveCertificateImage)
    }

    // update company certificate
    const companyCertificates = await CompanyCertificate.find({company}, 'id')

    let arrayofids = []

    for(let c of companyCertificates){
        arrayofids.push(c.id)
    }

    // return companyCertificates
    const companyCertificateUpdate = await Company.updateOne({_id: company}, {certificate: arrayofids})

    if(!companyCertificateUpdate) {
        return result = {
            success: false,
            description: "update company failed"
        }
    }

    return result = {
        success: true,
        description: companyCertificateUpdate
    } 


}


async function saveOneImage(image, company ) {
    const newCertificate = new CompanyCertificate({
        url: image.path,
        company: company
    })

    const saved = await newCertificate.save()
    if(!saved) return false
    return true;
}

async function updateCertificateService(){
    return
}

async function deleteCertificateService(image){
    const isImageExists = await CompanyCertificate.findById(image)

    if(!isImageExists){
        return result = {
            success: false,
            description: "certificate does not exists"
        }
    }

    const deleted = await CompanyCertificate.deleteOne({ _id: image});

    if(!deleted) {
        return result = {
            success: false,
            description: "failed to delete certificate"
        }
    }

    const certificates  = await CompanyCertificate.find({company: isImageExists.company}, '_id')

    let arrayOfIds = [];
    for(let c of certificates) {
        arrayOfIds.push(c._id)
    }

    const updateCompanyCertificates = await Company.updateOne({_id: isImageExists.company}, {certificate: arrayOfIds})

    if(!updateCompanyCertificates){
        return result = {
            success: false,
            description: "update company certificates failed"
        }
    }

    return result = {
        success: true,
        description: updateCompanyCertificates
    }
}

async function seeCertificateService(company){
    
    const isCompanyExist = await Company.findById(company)

    if(!isCompanyExist) {
        return result = {
            success: false,
            description: 'company not exists'
        }
    }

    const certificates = await CompanyCertificate.find({company})
        .populate('company', 'name')

    return certificates
}

const uploadCert = async (company, certificates) => {
    let failed = [];
    let successful = [];

    for (let certificate of certificates) {
        const newCertificate = new CompanyCertificate({
            url: certificate,
            company,
        })

        const saved = await newCertificate.save()
        if(!saved) failed.push(certificate)
        else successful.push(certificate)
    }
    
    return ({successful, failed})
} 

module.exports = {
    uploadCertificateService,
    updateCertificateService,
    deleteCertificateService,
    seeCertificateService,
    uploadCert,
}