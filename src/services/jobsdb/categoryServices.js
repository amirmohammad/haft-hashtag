
const { updateCategory } = require('../../controllers/apiController/companyController');
const CompanyCategory = require('../../models/jobsDB/category');
const company = require('../../models/jobsDB/company');
const Company = require('../../models/jobsDB/company')

async function createCategoryService(categoryName){
    
    const categoryExists = await CompanyCategory.findOne({ name : categoryName });
    if(categoryExists != null) {
        return result = {
            success: false,
            description: "category exsists"
        }
    }


    const newCompanyCategory = await new CompanyCategory({ name : categoryName })

    const saved = await newCompanyCategory.save()
    if(!saved) {
        return result = {
            success: false,
            description: saved
        }
    }

    else{
        return result = {
            success: true,
            description: saved
        }
    }
}

async function seeAllCategories(){
    const categories = await CompanyCategory.find({});

    return categories;
}

async function updateCategoryService(oldCategory, newCategory) {

    const categoryExists = await CompanyCategory.findOne({ name: oldCategory })

    if(!categoryExists){
        return result = {
            success: false, 
            description: "category not exists"
        }
    }

    const updated = await CompanyCategory.updateOne({ name: oldCategory }, { name: newCategory });

    if(!newCategory) {
        return result = {
            success: false,
            description: "operation failed"
        }
    }

    else{
        return result = {
            success: true, 
            description: updated
        }
    }
}

async function deleteCategoryService(categoryName) {
    
    const category = await CompanyCategory.findOne({name: categoryName})
    if(!category) {
        return result = {
            success: false,
            description: "category not found"
        }
    }

    const companies = await Company.deleteMany({ category: category._id });

    const deleted = await CompanyCategory.findOneAndDelete({name: categoryName})

    if(!deleted) {
        return result = {
            success: false,
            description: "error in deleting category"
        }

    }
    else{
        return result = {
            success: true,
            description: deleted
        }
    } 

}

module.exports = {
    createCategoryService,
    seeAllCategories,
    updateCategoryService,
    deleteCategoryService,
}