const Company = require('../../models/jobsDB/company')
const companyImages = require('../../models/jobsDB/companyImages')
// const companyImages = require('../../models/jobsDB/companyImages')
const CompanyGallery = require('../../models/jobsDB/companyImages')

async function createCompanyGalleryService(images, company){
    
    const companyExists = await Company.findById(company)


    if(!companyExists) {
        return result = {
            success: false,
            description: 'company not found'
        }
    }

    var array = []
    for (let i of images) {
        let saveResult = await saveOneImage(i.path, company)
        array.push(JSON.stringify(saveResult))
    }

    // update company Gallery 
    var arrayofids = []
   const companyimages = await CompanyGallery.find({company}, '_id')

   for (let i of companyimages) {
       arrayofids.push(i._id)
   }

   var companyUpdateGallery = await Company.updateOne({_id: company}, {gallery: arrayofids})


   if(!companyUpdateGallery) {
    return result ={ 
        success: false, 
        description: 'company gallery'
    }
   }

   if(!companyUpdateGallery) {
       return result ={ 
        success: false, 
        description: 'company gallery'
    }
   }

    return result ={ 
        success: true, 
        description: companyUpdateGallery
    }
}

async function saveOneImage(image, company ) {
    const newImage = new CompanyGallery({
        url: image,
        company: company
    })

    const saved = await newImage.save()
    if(!saved) return false
    return true;
}

async function seeCompanyGalleryService(company){
    const images = await CompanyGallery.find({company})

    return images;
}

async function updateCompanyGalleryService(image, company){
    return
}

async function deleteCompanyGalleryService(image){
    const imageExists = await CompanyGallery.findById(image)

    if(!imageExists) {
        return result = {
            success: false,
            description: 'image not exists'

        }
    }

    const deleted = await CompanyGallery.deleteOne({_id: image})
    
    if(!deleted){
        return result = {
            success: false,
            description: 'delete failed'

        }
    }

    // update company images

    const company = await Company.findOne({_id: imageExists.company});

    let newcompanyGallery = company.gallery.filter( id => id != image )

    let updateCompany = await Company.updateOne({_id: imageExists.company}, {gallery: newcompanyGallery})

    if(!updateCompany) {
        return result = {
            success: false,
            description: 'delete failed'

        }
    }

    return result = {
        success: true,
        description: 'image deleted'
    }

}

const saveGallery = async (path, company) => {
    
    let failed = [];
    let successful = [];

    for (let p of path) {
        const newGallery = new CompanyGallery({
            url: p,
            company,
        })

        const saved = await newGallery.save()
        if(!saved) failed.push(p)
        else successful.push(p)
    }
    
    return ({successful, failed})
    // return ({path, company})
}

module.exports = {
    createCompanyGalleryService,
    seeCompanyGalleryService,
    updateCompanyGalleryService,
    deleteCompanyGalleryService,
    saveGallery,

}