const Company = require('../../models/jobsDB/company')
const CompanyCategory = require('../../models/jobsDB/category');
const User = require('../../models/user')

async function createCompanyService(id, name, description, category, state, city, logo='static/companyLogo.png') {
    
    const categoryID = await CompanyCategory.findOne({name: category})

    if(!categoryID) {
        return result = {
            success: false,
            description: 'category not exists'
        };
    }

    const newCompany = new Company({
        name,
        description,
        owner: id,
        category: categoryID._id,
        logo,
        state, 
        city,
    })

    const save = await newCompany.save()

    if(!save) {
        return result = {
            success: false,
            description: 'unable to save'
        };
    }
    else {
        return result = {
            success: true,
            description: save
        }
    }
}

async function getAllCompaniesService(){
    const companies = await Company.find({}).populate('owner', 'name')
        .populate('category', 'name')  
        .populate('gallery','url')
    return companies
}

async function updateCompanyService(id, description, name, category) {
    const companyExists = await Company.findById(id);
    const categoryExists = await CompanyCategory.findOne({name: category});


    if(!companyExists || !categoryExists) {
        return result = {
            success: false,
            description: 'company or category does not exists'
        }
    }

    const updated = await Company.updateOne({_id: id}, {name, description, category: categoryExists._id})
    
    if(!updated){
        return result = {
            success: false,
            description: "error in saving"
        }
    }

    return result = {
        success: true,
        description: updated
    }
}

async function deleteCompanyService(id) {

    const companyExists = await Company.findById(id)
    if(!companyExists) {
        return result = {
            success: false,
            description: 'company does not exists'
        }
    }

    const deleteCompany = await Company.deleteOne({_id: id})

    if(!deleteCompany) {
        return result = {
            success: false,
            description: 'delete failed'
        }
    }

    return result = {
        success: true,
        description: deleteCompany
    }
}

async function getCompany(name){
    const companies = await Company.find({name}).populate('owner', 'name')
        .populate('category', 'name')  

    return companies
}

const isOwnerTypeCompany = async (user) => {
    const userInfo = await User.findById(user);
    return (await userInfo.role == 'company') ? true : false;
}

const iscompanyExists = async (id) => await Company.findById(id);



module.exports = {
    createCompanyService,
    updateCompanyService,
    getAllCompaniesService,
    getCompany,
    deleteCompanyService,
    isOwnerTypeCompany,
    iscompanyExists,
}