const kaveNegar = require('kavenegar_promise')
const Kavenegar = require('kavenegar');
const {kavenegar_token, messages, kavenegar_template} = require('../config')

const api = kaveNegar.KavenegarApi({
    apikey: kavenegar_token
})

// const api = Kavenegar.KavenegarApi({
//     apikey: kavenegar_token
// });


async function sendSMS(phoneNumber, code) {

    const sent = await api.VerifyLookup({
        receptor: phoneNumber,
        token: code,
        template: kavenegar_template
    });

    return sent
}


const sendToken = (phoneNumber, token) => {
    console.log(kavenegar_template)
    return api.VerifyLookup({
        receptor: phoneNumber,
        token: token,
        template: kavenegar_template
    }, function(response, status) {
        console.log(response);
        console.log(status);
    });

}



module.exports = {
    sendSMS,
    sendToken,
}