
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const ActivationCode = require('../models/activationcode')

// model
const User = require('../models/user')
const config = require('../config')
const { syslog } = require('winston/lib/winston/config')


async function saveUser(data) {
    // console.log(data.birthdate)
    
    const isUniqueUser = await isUique(data.phonenumber)

    if(!isUniqueUser){
        return result = {
            success: false, 
            description: 'user has saved before'
        }
    }

    const newUser = new User ({phonenumber: data.phoneNumber});

    const saved = await newUser.save();

    if(!saved) {
        return result = {
            success: false, 
            description: saved
        }
    }

    else{
        const token = await createJWTToken(data.phoneNumber)
        return result = {
            success: true,
            description: saved,
            token: token
        }
    }
}

async function isUique(phoneNumber){
    const user = await User.findOne({phonenumber: phoneNumber });

    if(!user) return true;
    else return false;
}

async function isUserExists(phonenumber) {
    const exitst = await User.findOne({phonenumber})
    if(!exitst) return false;
    else return true;
}

async function passwordMatches(phonenumber, password) {
    const user = await User.findOne({phonenumber})
    
    const comparePassword = await bcrypt.compare(password, user.password)

    if(!comparePassword) {
        return result = {
            success: false,
            description: "password does not match"
        }
    }
    else{
        return result = {
            success: true,
            description: "password matches"
        }
    }
}

async function createJWTToken(phonenumber){

    const user = await User.findOne({phonenumber});

    let token = await jwt.sign({
        data: user._id,
    }, config.secret_key, {expiresIn: '365d'});
    return token

}

async function editUser(id, name, personalCodeID, gender, state, city, birthdate, role){

    const updated = await User.updateOne({ _id: id }, { name, personalCodeID, gender, state, city, birthdate, role })

    return result = {
        success: true,
        description: "user updated"
    }

}

async function uploadImageService(id, image) {
    
    const uploadImage = await User.updateOne({_id: id}, {profilePicture: image })

    if(!uploadImage) {
        return result = {
            success: false,
            description: "upload image failed"
        }
    }

    return result = {
        success: true, 
        description: uploadImage
    }
    
}

async function deleteImageService(id){
    return
}

async function getUserInfoService(id){
    
    return await User.findById(id);
}

const isUserVerifiedNumber = async (phoneNumber) => {
    const isactived = await ActivationCode.findOne({$and: [{phoneNumber}, {used: true}]});
    if(!isactived) return false
    else return true
}

module.exports = {
    saveUser,
    isUserExists,
    passwordMatches,
    createJWTToken,
    editUser,
    uploadImageService,
    deleteImageService,
    getUserInfoService,
    isUserVerifiedNumber,
}