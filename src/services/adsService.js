const Ads = require('../models/advertisement');
const mongoose = require('mongoose')

async function saveAdsService(filepath, link, description){
    const newAds = new Ads({
        bannerImage: filepath,
        link,
        description
    })

    const saveAds = await newAds.save(); 

    if(! saveAds){
        return result = {
            success: false,
            description: saveAds,
        }
        
    }

    else{
        return result = {
            success: true, 
            description: saveAds
        }
    }
}

async function getAdsService() {
    const ads = await Ads.find();
    return ads;
}

const setWallpaper = async (image) => {
    const newAds = new Ads({
        bannerImage: image,
        wallpaper: true
    })
    const saved = await newAds.save(); 
    if(!saved) {
        return result = {
            success: false,
            status: 500,
            description: 'error in saving wallpaper'
        }
    }

    return{
        success: true,
        status: 200,
        description: saved
    }
}

module.exports = {
    saveAdsService,
    getAdsService,
    setWallpaper
}