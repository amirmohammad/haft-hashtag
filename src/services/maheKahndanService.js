const Video = require('../models/maheKhandan/maheKhandanVideo')
const Comment = require('../models/maheKhandan/maheKhandanVideoComments');
const User = require('../models/user')
const Point = require('../models/maheKhandan/point')

async function uploadVideoSVC(video) {
    
    const newVideo = new Video({uri: video})
    const saved = await newVideo.save()

    if(!saved) {
        return result = {
            success: false,
            description: 'error in saving video',
            status: 500
        }
    }
    return result = {
        success: true,
        description: saved,
        status: 200
    }
}

const sendCommentSVC = async (user, video, text) => {
    const isVideoExists = await Video.findById(video)
    if(!isVideoExists){
        return result = {
            success: false,
            status: 404,
            description: 'video not found'
        }
    }

    const newComment = new Comment({user, video, text}) 
    const saved = await newComment.save();

    if(!saved) {
        return result = {
            success: false,
            status: 500,
            description: 'error in saving video'
        }
    }

    // update video
    const comments = await Comment.find({video}, '_id')

    let arrayOfIDs = [];
    for(let c of comments){
        arrayOfIDs.push(c._id)
    }

    const updatedVideos = await Video.updateOne({_id: video}, {comment: arrayOfIDs})


    return result = {
        success: true,
        status: 200,
        description: saved
    }
}

const getVideosSVC = async (video) => {

    const isVideoExists = await Video.findById(video)

    if(!isVideoExists){
        return result = {
            success: false,
            description: 'video not exists',
            status: 404
        }
    }

    
    const vid = await Video.findById(video, 'uri').populate('comment', 'text')

    return result = {
        success: true,
        description: vid,
        status: 200
    }
}

const deleteVideoSVC = async (video) => {
    const isVideoExists = await Video.findById(video)
    if(!isVideoExists){
        return result = {
            success: false,
            description: 'video not exists',
            status: 404
        }
    }

    const deleteComments = await Comment.deleteMany({video})
    const deleted = await Video.deleteOne({_id: video})

    if(!deleted || !deleteComments) {
        return result = {
            success: false,
            description: 'delete failed',
            status: 500
        } 
    }

    return result = {
        success: true,
        description: 'done',
        status: 200
    } 
}

const getAllVideosSVC = async (video) => {
    return await Video.find({}, 'uri thumbnail')
}

const updateCommentSVC = async (comment, newComment) => {

    const isCommentExists = await Comment.findById(comment)
    if(!isCommentExists) {
        return result = {
            success: false,
            description: 'comment not found',
            status: 404
        }
    }

    const updated = await Comment.updateOne({_id: comment}, {text: newComment})


    return result = {
        success: true,
        description: updated,
        status: 200
    }
}

const deleteCommentSVC = async (comment, userid) => {
    const isCommentExists = await Comment.findById(comment)

    if(!isCommentExists) {
        return result = {
            success: false,
            description: 'comment not found',
            status: 404
        }

    }

    const isAdmin = await User.findById(userid, 'admin')
    const canRemoveComment = (isCommentExists.user != userid || !(isAdmin.isAdmin)) ? true : false;

    if(!canRemoveComment) {
        return result = {
            success: false,
            description: 'can not access',
            status: 403
        }
    }

    const deleted = await Comment.deleteOne({_id: comment});

    if(!deleted) {
        return result = {
            success: false,
            description: 'error in deleting',
            status: 500
        }
    }
    return result = {
        success: true,
        description: 'successful',
        status: 200
    }
}

const videoExistsSVC = (video) => Video.findById(video);

const reactionsSVC = (video) => Point.find({video});

const sendReactionSVC = async (user, video, point) => {
    const newPoint = new Point({
        rate: point,
        user,
        video
    })
    return await newPoint.save()
}

const sendReactionBeforeSVC = async (user, video) =>  await Point.findOne({ $and: [{user}, {video}]});

const updateRactionSVC = async (reaction, point) => await Point.updateOne({_id: reaction}, {rate: point})


module.exports = {
    uploadVideoSVC, 
    sendCommentSVC,
    getVideosSVC,
    deleteVideoSVC,
    getAllVideosSVC,
    updateCommentSVC,
    deleteCommentSVC,
    videoExistsSVC,
    reactionsSVC,
    sendReactionSVC,
    sendReactionBeforeSVC,
    updateRactionSVC,
}