const transform = require('../transform')

class companyTransform extends transform {


    createCompanyTransform(item){
        return {
            gallery: item.gallery,
            certificate: item.certificate,
            name: item.name,
        }
    }

    deleteCompanyTransform(item){
        return {
            
        }
    }

    updateCompanyTransform(item){}

    getCompanyTransform(item){}

    getCompaniesTransform(item){
        var array = [];

        for (let i of item) {
            let obj = {
                gallery: i.gallery,
                name: i.name,
                owner: `${i.owner.name}`,
                certificate: i.certificate,
                category: i.category.name,
                description: i.description,
                id: i._id
            }
            array.push(obj)
        }
        return array
    }
}

module.exports = new companyTransform()