const transform = require('../transform')

class companyGalleryTransform extends transform {


    seeCompanyGalleryTransform(item) {
        let array = []

        for (let i of item) {
            array.push({
                url: i.url,
                id: i._id,
            })
        }

        return array;
    }


}

module.exports = new companyGalleryTransform()