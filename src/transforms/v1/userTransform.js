const transform = require('../transform')

class adsTransform extends transform {


    saveUserTransform(item, token) {
    
        return {
            name: item.name,
            email: item.email,
            gender: item.gender,
            active: item.active,
            admin: item.admin,
            state: item.state,
            city: item.city, 
            personalCodeID: item.personalCodeID,
            createdAt: item.createdAt,
            token: token,
        }
    }    

    getUserInfoTransform(item) {
        return {
            name: item.name,
            birthdate: item.birthdate,
            gender: item.gender,
            state: item.state,
            city: item.city,
            admin: item.admin,
            profilePicture: item.profilePicture,
            personalCodeID: item.personalCodeID,
            role: item.role,
        }
    }
}

module.exports = new adsTransform()