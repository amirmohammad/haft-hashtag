const transform = require('../transform')

class companyCertificateTransform extends transform {


    seeCompanyCertificateTransform(item) {
        let array = []

        for (let i of item) {
            array.push({
                url: i.url,
                id: i._id,
                company: i.company,
                certificateLevel: i.certificateLevel,
            })
        }

        return array;
    }


}

module.exports = new companyCertificateTransform()