const transform = require('../transform')

class ideaTransform extends transform {

    saveTransform(item) {
        return {
            firstname: item.firstname,
            lastname: item.lastname,
            type: item.type,
            request: item.request,
            description: item.description,
        }
    }

    getTransform(item){
        let array = [];

        for(let i of item) {
            let obj = {
                user: i.user,
                firstname: i.firstname,
                lastname: i.lastname,
                type: i.type,
                request: i.request,
                description: i.description,
                category: i.category
            }
            array.push(obj)
        }

        return array;
    }

}

module.exports = new ideaTransform()