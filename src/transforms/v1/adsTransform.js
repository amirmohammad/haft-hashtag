const transform = require('../transform')

class adsTransform extends transform {

    saveAds(item) {
        const data = item.description;

        return {
            bannerImage: data.bannerImage,
            link: data.link,
            description: data.description
        }
    }

    getAds(item) {
        
        var result = [];

        for (let i of item) {
            let data = {
                link: i.link,
                description: i.description,
                bannerImage: i.bannerImage
            }
            result.push(data)
        }

        return result
    }
}

module.exports = new adsTransform()