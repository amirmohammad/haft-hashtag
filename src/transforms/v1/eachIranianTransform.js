const transform = require('../transform')

class companyTransform extends transform {

    eachIranianCreatePeople(item){
        return {
            personalID: item.personalID,
            gender: item.gender,
            fullname: item.fullname,
            type: item.type,
            description: item.description,
            id: item._id,
            image: item.image,
        }
    }

    savePlanTransform(item){
        return {
            id: item._id,
            name: item.name,
            days: item.days,
            fee: item.fee,
        }
    }

    boughtPlanTransform(item) {
        return {
            id: item._id,
            user: item.user,
            start: item.start,
            end: item.end,
            plan: item.plan
        }
    } 

    applyForJobTransform(item){
        return{ 
            id: item.id,
            name: item.name,
            state: item.state,
            city: item.city,
            jobTitle: item.jobTitle,
            description: item.description,
            plan: item.plan
        }
    }

}

module.exports = new companyTransform()