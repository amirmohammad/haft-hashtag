const appConfigs = require('./app.config')
const LoggerConfigs = require('./logger.config')
const DBconfigs = require('./mongoose.config')
const settingsConfig = require('./settings.config')
const messagesConfig = require('./messages.config')
const servicesConfig = require('./services.config')
const sessionConfig = require('./session.config')

module.exports = {
    "app" : appConfigs,
    "logs": LoggerConfigs,
    "database": DBconfigs,
    "settings": settingsConfig,
    "secret_key": process.env.SECRET_KEY,
    "kavenegar_token": process.env.KAVENEGAR_KEY,
    "messages": messagesConfig, 
    "service": servicesConfig,
    cookie_secretkey: '!ljdf%$SsadflJNUI;i09347#23$VBVTYfq',
    session: sessionConfig,
    kavenegar_template: 'verify',
}