
module.exports = {
    port: process.env.PORT || 3000,
    apiRoute: '/api',
    webRoute: '/'
}