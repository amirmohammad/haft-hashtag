const autoBind = require('auto-bind')
const { validationResult } = require('express-validator')
const Recaptcha = require('express-recaptcha').RecaptchaV3;
const config = require('../config/index')

class controller {
    constructor(){
        autoBind(this);
        // this.recaptchaConfig();
    }

    recaptchaConfig() {
        this.recaptcha = new Recaptcha(
            config.service.recaptcha.client_key,
            config.service.recaptcha.secret_key , 
            {...config.service.recaptcha.options}
        );
    }

    recaptchaValidation(req , res) {
        return new Promise((resolve , reject) => {
            this.recaptcha.verify(req , (err , data) => {
                if(err) {
                    req.flash('errors' , 'گزینه امنیتی مربوط به شناسایی روبات خاموش است، لطفا از فعال بودن آن اطمینان حاصل نمایید و مجدد امتحان کنید');
                    this.back(req,res)
                } else resolve(true);
            })
        })
    }

    async validationData(req , res) {
        const result = validationResult(req);
        if (! result.isEmpty()) {
            const errors = result.array();
            const messages = [];
           
            errors.forEach(err => messages.push(err.msg));

            this.failed(messages , res , 403);

            return false;
        }

        return true;
    }

    async havename(user, res){
        if(!user.name){
            return res.json({
                status: "error",
                message : "423"
            })
        }
        else return
    }

    async isMongoId(paramId, res) {
        if(! isMongoId(paramId)) return false
        return true
    }

    
    async escapeAndTrim(feild) {
        let item = feild.trim();
        item = escape(item);
        return item
    }

    failed(msg , res , statusCode = 500) {
        return res.json({
            code: '433',
            message : msg,
            status : 'error'
        })
    }

    normalizePath(path) {
        return path.replace(" ", "%20")
    }

    back(req , res) {
        req.flash('formData' , req.body);
        return res.redirect(req.header('Referer') || '/');
    }

    alert(req , data) {
        let title = data.title || '',
            message = data.message || '',
            type = data.type || 'info',
            button = data.button || null,
            timer = data.timer || 2000;

        req.flash('sweetalert' , { title , message , type , button , timer});
    }

    alertAndBack(req, res , data) {
        this.alert(req , data);
        this.back(req , res);
    }

    async validationForm(req) {
        const result = validationResult(req);
        if (! result.isEmpty()) {
            const errors = result.array();
            const messages = [];
           
            errors.forEach(err => messages.push(err.msg));

            req.flash('errors' , messages)

            return false;
        }

        return true;
    }

}

module.exports = controller; 