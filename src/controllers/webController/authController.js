const controller = require('../index');
const passport = require('passport')
const authService = require('../../services/web/authServices');
const user = require('../../models/user');

class authController extends controller {

    showLoginForm(req, res) {
        const title = 'صفحه ورود';
        // return res.render('login' , { title, errors: [], recaptcha: this.recaptcha.render()});
        const errors = req.flash('errors')
        // console.log(res.recaptcha)
        return res.render('auth/login', { errors, title});
    
    }

    showRegistrationForm(req, res) {
        return  res.render('register');
    }

    showRegsitrationForm(req , res) {
        res.render('auth/register' , { errors : req.flash('errors'), title: 'ثبت نام'});
    }

    login(req, res, next) {
        passport.authenticate('local-login' , (err , user) => {
            if(!user) return res.redirect('/login');

            req.logIn(user , err => {
                if(req.body.remember) {
                    user.setRememberToken(res);
                }

                return res.redirect('/');
            })

        })(req, res , next);
    }

    async showregisterationNumber(req, res) {
        return res.render('auth/registerNumber', {phoneNumber: req.params.phoneNumber});
    }

    async registerForNumber(req, res){
        const {phoneNumber, password} = req.body;

        // console.log(phoneNumber, password)
        // check number 
        const isExists = await authService.isNumberExists(phoneNumber, password)
        console.log(isExists)
        if(!isExists) {
            req.flash('errors', 'رمز عبور اشتباه/منقظی است')
            return res.redirect('/auth/login')
        }
        // compare send and db code
        // const isEqual = await authService.isEqual(phoneNumber, password)

        // const

        return res.json({phoneNumber, password})
    }

    async register(req, res, next) {

        if(! await this.validationForm(req) ) return res.redirect('/auth/register')

        if(req.body.password != req.body.repassword) {
            req.flash('errors', 'پسورد با تکرارش یکسان نیست')
            return res.redirect('/auth/register')
        }

        else {
            const phonenumber = req.body.phoneNumber;
            const password = req.body.password;
            this.registerP(req, res, next)
        }

        // return  res.json('register');
    }

    // registerP(req, res, next) {
    //     passport.authenticate('local.register' , { 
    //         successRedirect : '/',
    //         failureRedirect : '/auth/register',
    //         failureFlash : true
    //     })(req, res , next);
    // }

    registerP(req, res, next) {
        passport.authenticate('local-register' , { 
            successRedirect : '/',
            failureRedirect : '/auth/register',
            failureFlash : true
        })(req, res , next);
    }

    async test(req, res, next){
        // return res.json(req.body)
        // passport.authenticate()
        console.log('rest sdljflasjd flsa')
        // return res.json(req.body)
        res.redirect('/')
        
        // passport.authenticate('local-test')
    }
}

module.exports = new authController();