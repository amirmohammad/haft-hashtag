const controller = require('../../index')
const ObjectId = require('mongoose').Types.ObjectId;

// service
const companyService = require('../../../services/web/companyServices');
const companyServices = require('../../../services/web/companyServices');

class CompanyController extends controller {

    async categoryPage(req, res) {
        const title = 'لیست کتگوری ها'
        const errors = req.flash('errors')

        const categories = await companyService.allCategories();

        return res.render('admin/company/company', {title, errors, categories})
    }

    async viewCategory(req, res) {
        
        if(! ObjectId.isValid(req.params.id) ){
            req.flash('errors', 'آی دی صحیح نیست')
            return res.redirect('/admin/company')
        }

        const category = await companyService.category(req.params.id)

        if(!category) {
            req.flash('errors', 'کتگوری وجود ندارد')
            return res.redirect('/admin/company')
        }

        const companies = await companyService.companies(req.params.id);

        const title = category.name;
        const errors = req.flash('errors')
        return res.render('admin/company/view-category', { title, errors, category, companies })
    }

    async createCategoryPage(req, res) {
        
        const title = 'ساخت کتگوری مشاغل';
        const errors = req.flash('errors')

        return res.render('admin/company/create-category', {title, errors})
    }

    async createCategory(req, res) {
        if(! await this.validationForm(req) ) return res.redirect('/admin/company/create-category')

        const isCategoryExists = await companyService.isCategoryExists(req.body.category)

        // category name saved before
        if(isCategoryExists) {
            req.flash('errors', 'نام کتگوری قبلا ثبت شده. نام دیگری انتخاب کنید')
            return res.redirect('/admin/company/create-category')
        }

        // save category
        const saved = await companyServices.saveCategory(req.body.category)
        
        if(!saved) {
            req.flash('errors', 'مشکلی در ذخیره اتفاق افتاده. لطفا بعدا بررسی کنید')
            return res.redirect('/admin/company/create-category')
        }

        return res.redirect('/admin/company')

    }

    async editCateogryPage (req, res) {

        const errors = req.flash('errors')
        const title = 'نغییر نام کتگوری'
        const category = req.params.id;
        return res.render('admin/company/edit-category', {title, errors, category})
    }

    async editCategory(req, res) {
        if(! await this.validationForm(req) ) return res.redirect('/admin/company')

        const isNewCategoryNameExists = await companyServices.isCategoryExists(req.body.category);
        
        if(isNewCategoryNameExists) {
            req.flash('errors', 'نام کتگوری قبلا ثبت شده نام دیگری برای آن انتخاب کنید')
            return res.redirect(`/admin/company/edit-category/${req.params.id}`);
        }

        const updated = await companyServices.updateCategoryName(req.params.id, req.body.category);

        if(!updated.ok) {
            req.flash('errors', 'مشکلی برای ذخیره کتگوری به وجود امده بعدا امتحان کنید')
            return res.redirect(`/admin/company/edit-category/${req.params.id}`);
        }
        
        return res.redirect(`/admin/company`);

    }

    async deleteCategoryPage(req, res) {

        if(! ObjectId.isValid(req.params.id) ){
            req.flash('errors', 'آی دی صحیح نیست')
            return res.redirect('/admin/company')
        }

        const category = await companyService.category(req.params.id)

        if(!category) {
            req.flash('errors', 'کتگوری وجود ندارد')
            return res.redirect('/admin/company')
        }

        const companies = await companyService.companies(req.params.id);

        const title = category.name;
        const errors = req.flash('errors')
        return res.render('admin/company/delete-category', { title, errors, category, companies })

    }

    async deleteCateogry(req, res) {
        if(! ObjectId.isValid(req.params.id) ){
            req.flash('errors', 'آی دی صحیح نیست')
            return res.redirect('/admin/company')
        }

        const category = await companyService.category(req.params.id)

        if(!category) {
            req.flash('errors', 'کتگوری وجود ندارد')
            return res.redirect('/admin/company')
        }

        const deleteCompanies = await companyServices.deleteCompanies(req.params.id)

        const deleteCategory = await companyServices.deleteCategory(req.params.id)

        if(!deleteCategory) {
            req.flash('errors', 'مشکلی در حذف کتگوری پیش امده')
            return res.redirect(`/admin/company/delete-category/${req.params.id}`)
        }

        return res.redirect('/admin/company')
    }

    async viewJobPage(req, res) {
        
        if(! ObjectId.isValid(req.params.id) ){
            req.flash('errors', 'آی دی صحیح نیست')
            return res.redirect('/admin/company')
        }

        const company = await companyServices.company(req.params.id)

        if(!company) {
            req.flash('errors', 'شرکت یافت نشد')
            return res.redirect('/admin/company')
        }


        const category = (await companyServices.findCategory(req.params.id)).category;

        const title = company.name;
        const errors = req.flash('errors');

        return res.render('admin/company/view-company', {title, errors, company, category})
    }

    async deleteCompany(req, res) {
        if(! ObjectId.isValid(req.params.id) ){
            req.flash('errors', 'آی دی صحیح نیست')
            return res.redirect('/admin/company')
        }

        const isCompanyExists = await companyServices.isCompanyExists(req.params.id);
        if(!isCompanyExists) {
            req.flash('errors', 'کمپانی وجود ندارد')
            return res.redirect('/admin/company')
        }

        const deleteCompany = await companyServices.deleteCompany(req.params.id)
        if(!deleteCompany.ok) {
            req.flash('errors', 'مشکلی در حذف کمپانی بوجود امده است')
            return res.redirect('/admin/company') 
        }

        return res.redirect('/admin/company')
    }

    async deleteCompanyPage(req, res) {
        if(! ObjectId.isValid(req.params.id) ){
            req.flash('errors', 'آی دی صحیح نیست')
            return res.redirect('/admin/company')
        }

        const company = await companyServices.company(req.params.id)
        const category = (await companyServices.findCategory(req.params.id)).category;


        if(!company) {
            req.flash('errors', 'شرکت یافت نشد')
            return res.redirect('/admin/company')
        }
        
        const title = 'حذف شرکت';
        const errors = req.flash('errors')

        return res.render('admin/company/delete-company', {title, errors, company, category})
    }
}

module.exports = new CompanyController();