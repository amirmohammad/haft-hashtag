const { restart } = require('nodemon');
const controller = require('../../index')

class adminController extends controller {

    statisticsForm(req, res) {
        return res.json('statistics form')
    }

    videoEdit(req, res) {
        return res.json('edit video')
    }

    showAdminPanel(req, res) {
        const title = 'پنل ادمین'
        return res.render('admin/admin', {title, })
    }

}

module.exports = new adminController();