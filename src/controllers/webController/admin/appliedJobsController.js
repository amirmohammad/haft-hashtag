const controller = require('../../index')

const appliedJobsService = require('../../../services/web/appliedJobsServices')

class appliedJobsController extends controller {

    async appliedJobsPage(req, res) {

        const title = 'لیست درخواست ها'
        const errors = req.flash('errors')

        const job = await appliedJobsService.allAppliedJobs();

        return res.render('admin/appliedJob/applied-jobs', {title, errors, job})
    }

}

module.exports = new appliedJobsController();