const controller = require('../../index')
const ObjectId = require('mongoose').Types.ObjectId;
// const thumbsupply = require('thumbsupply');
// const { ThumbnailGenerator } = require('video-thumbnail-generator');
const mt = require('media-thumbnail')
const ffmpeg = require('fluent-ffmpeg');
const uniqueString = require('unique-string');

// services
const mahekhandanServices = require('../../../services/web/mahekhandanService')

class maheKhandanController extends controller {

    async showMahekhandanManagementPage(req, res) {
        const title = 'ماه خندان'
        const errors = req.flash('errors')

        // const allCategories = await mahekhandanServices.allCategories();
        const allVideos = await mahekhandanServices.allVideos();

        return res.render('admin/maheKhandan/mahekhandan', {errors, title, video: allVideos})
    }

    async createVideoPage (req, res) {
        const title = 'آپلود ویدیوی ماه خندان'
        const errors = req.flash('errors');
        return res.render('admin/maheKhandan/create-video', {title, errors})
    }

    async uploadVideo (req, res) {

        if(!req.file) {
            req.flash('errors', 'فایل موجود نیست')
            return res.redirect('/admin/maheKhandan/create-video')
        }

        const path = req.file.path.replace(" ", "%20");
        const filename = `${uniqueString()}.png`
        const destination = `uploads/thumbnail/` + filename;

        const proc = await new ffmpeg(req.file.path)
        .takeScreenshots({
                count: 1,
                filename: filename,
                timemarks: [ '1' ], // number of seconds
            }, 'uploads/thumbnail', function(err) {
            console.log('screenshots were saved');
        });


        const uploaded = await mahekhandanServices.uploadVideo(path, destination);
        if(!uploaded) {
            req.flash('errors', 'مشکلی دز اپلود پیش امده لطفا بعدا بررسی کنید')
            return res.redirect('/admin/maheKhandan/create-video')
        }

        return res.redirect('/admin/maheKhandan')
    }

    async viewVideoPage(req, res) {

        if(! ObjectId.isValid(req.params.id)) {
            req.flash('errors', 'ای دی معتبر نیست')
            return res.redirect('/admin/maheKhandan')
        }

        const title = 'تماشای ویدیو'
        const errors = req.flash('errors')

        const video = await mahekhandanServices.video(req.params.id)
        const comments = await mahekhandanServices.comments(req.params.id)
        const reactions = await mahekhandanServices.allReactions(req.params.id)


        if(!video) {
            req.flash('errors', 'ویدیو وجود ندارد')
            return res.redirect('/admin/maheKhandan')
        }

        return res.render('admin/maheKhandan/view-video', {title, errors, video, comments, reactions})
    }

    async deleteVideoPage(req, res) {
        if(! ObjectId.isValid(req.params.id)) {
            req.flash('errors', 'ای دی معتبر نیست')
            return res.redirect('/admin/maheKhandan')
        }

        const title = 'حذف ویدیو'
        const errors = req.flash('errors')

        const video = await mahekhandanServices.video(req.params.id)

        if(!video) {
            req.flash('errors', 'ویدیو وجود ندارد')
            return res.redirect('/admin/maheKhandan')
        }

        return res.render('admin/maheKhandan/delete-video', {title, errors, video})
    }

    async removeVideo(req, res) {
        if(! ObjectId.isValid(req.params.id)) {
            req.flash('errors', 'ای دی معتبر نیست')
            return res.redirect('/admin/maheKhandan')
        }

        const removed = await mahekhandanServices.remove(req.params.id);
        if(!removed) {
            req.flash('errors', 'مشکلی در حذف ویدیو پیش امده')
            return res.redirect('/admin/maheKhandan')
        }

        return res.redirect('/admin/maheKhandan')

    }

    async deleteComment(req, res) {
        if(! ObjectId.isValid(req.params.id)) {
            req.flash('errors', 'ای دی معتبر نیست')
            return res.redirect('/admin/maheKhandan')
        }

        const removed = await mahekhandanServices.removeComment(req.params.id);
        if(!removed) {
            req.flash('errors', 'مشکلی در حذف کامنت پیش امده')
            return res.redirect('/admin/maheKhandan')
        }

        return res.redirect('/admin/maheKhandan')
    }


}

module.exports = new maheKhandanController();