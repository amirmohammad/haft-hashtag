const controller = require('../../index')

const adsServices = require('../../../services/web/adsServices')
const ObjectId = require('mongoose').Types.ObjectId;

class adsController extends controller {
    async adsManagement(req, res) {

        const allAds = await adsServices.allAds()

        const title = 'مدیریت تبلیغات'
        const errors = req.flash('errors')
        return res.render('admin/ads/ads-management', {title, errors, ads: allAds})
    }

    createAds(req, res) {

        const errors = req.flash('errors')
        const title = 'ایجاد تبلیغات'
        return res.render('admin/ads/create-ads', {title, errors})
    }

    async adsInfo(req, res) {

        if(! ObjectId.isValid(req.params.id) ){
            req.flash('errors', 'آی دی صحیح نیست')
            return res.redirect('/admin/ads/ads-management')
        }

        const adsInfo = await adsServices.adsInfo(req.params.id)

        const title = 'مشاهده تبلیغ'
        return res.render('admin/ads/ads-info', {title, ads: adsInfo})
    }

    editAds(req, res) {
        return res.render('admin/ads/edit-ads')
    }

    async deleteAds(req, res) {

        if(! ObjectId.isValid(req.params.id) ){
            req.flash('errors', 'آی دی صحیح نیست')
            return res.redirect('/admin/ads/ads-management')
        }

        const adsInfo = await adsServices.adsInfo(req.params.id)

        const title = 'حذف تبلیغ'
        return res.render('admin/ads/delete-ads', {title, ads: adsInfo})
    }

    async uploadAds(req, res) {

        if(!req.file) {
            req.flash('errors', 'فایل موجود نیست')
            return res.redirect('/admin/ads/create-ads')
        }

        const uploaded = await adsServices.uploadAds(req.file.path, req.body.link, req.body.description)
        if(!uploaded) {
            req.flash('errors', 'مشکلی در اپلوپ پیش امده لطفا بعدا تلاش کنید')
            return res.render('/admin/ads/create-ads')
        }
        
        return res.redirect('/admin/ads/ads-management')
    }

    async removeAds(req, res) {
        if(! ObjectId.isValid(req.params.id) ){
            req.flash('errors', 'آی دی صحیح نیست')
            return res.redirect('/admin/ads/ads-management')
        }

        const removed = await adsServices.removeAds(req.params.id)
        if(!removed) {
            req.flash('errors', 'اشکالی در حذف تبلغ صورت گرفته')
            return res.redirect(`/admin/ads/delete-ads/${req.params.id}`)
        }

        return res.redirect('/admin/ads/ads-management')
    }
}

module.exports = new adsController();