const controller = require('../../index')

const planServices = require('../../../services/web/planServices')

class planController extends controller {

    async showPlansPage(req, res) {
        const title = 'پلن ها'
        const errors = req.flash('errors')

        // const plans = 
        const plans = await planServices.allPlans();

        return res.render('admin/plan/plans', { title, errors, plans})
    }

    async createPlanPage(req, res) {
        const title = 'ساخت پلن جدید'
        const errors = req.flash('errors')

        return res.render('admin/plan/create-plan', {title, errors})
    }

    async createPlan(req, res) {
        if(! await this.validationForm(req) ) return res.redirect('/admin/plan/create-plan')
        
        // check if planexists
        const isPlanExists = await planServices.isPlanExists(req.body.name)
        if(isPlanExists) {
            req.flash('errors', 'پلن قبلا ذخیره شده')
            return res.redirect('/admin/plan/create-plan')
        }

        const createPlan = await planServices.createPlan(req.body.name, req.body.fee, req.body.days)
        if(!createPlan) {
            req.flash('erros', 'مشکلی در ذخیره پلن بوجود امده است')
            return res.redirect('/admin/plan/create-plan')
        }

        return res.redirect('/admin/plan')
    }

    async editPlanPage(req, res) {
        if(! await this.validationForm(req) ) return res.redirect('/admin/plan')

        const plan = await planServices.plan(req.params.id);
        const title = 'تغییر پلن'
        const errors = req.flash('errors');
        
        return res.render('admin/plan/edit-plan', { title, errors, plan})
    }

    async editPlan(req, res) {
        if(! await this.validationForm(req) ) return res.redirect('/admin/plan')

        const {id, name, fee, days} = req.body;
        
        const isPlanExists = await planServices.findPlanByID(id)
        if(!isPlanExists) {
            req.flash('errors', 'پلن وجود ندارد ')
            return res.redirect('/admin/plan')
        }

        const newPlan = await planServices.updatePlan(id, name, fee, days);
        if(!newPlan.ok) {
            req.flash('errors', 'مشکلی در اپدیت پلن به وجود امده است')
            return res.redirect('/admin/plan')
        }

        return res.redirect('/admin/plan')

    }

    async deletePlanPage(req, res) {
        if(! await this.validationForm(req) ) return res.redirect('/admin/plan')

        const title = 'حذف پلن'
        const errors = req.flash('errors')

        const plan = await planServices.plan(req.params.id);
        return res.render('admin/plan/delete-plan', { title, errors, plan})

    }

    async deletePlan(req, res) {
        if(! await this.validationForm(req) ) return res.redirect('/admin/plan')

        const deleted = await planServices.deletePlan(req.body.id)
        if(!deleted.ok) {
            req.flash('errors', 'مشکلی در حذف پلن پیش امده است')
            return res.redirect('/admin/plan')
        }

        return res.redirect('/admin/plan')
    }
}

module.exports = new planController();