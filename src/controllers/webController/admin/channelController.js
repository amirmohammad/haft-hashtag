
const controller = require('../../index')

const channelServices = require('../../../services/web/channelService');
const channel = require('../../../models/tv/channel');
const { channelName, removeBanner } = require('../../../services/web/channelService');
const ObjectId = require('mongoose').Types.ObjectId;

class channelController extends controller {

    async channelManagement(req, res) {
        // return res.json('channle management')
        // get all channels
        const allChannels = await channelServices.allChannels();
        const title = 'مدیریت شبکه ها'
        const errors = req.flash('errors')
        return res.render('admin/channel/channel-management', {title, channels: allChannels, errors})
    }

    createChannel(req, res) {
        const errors = req.flash('errors')
        const title = 'ساخت کانال جدید'
        return res.render('admin/channel/create-channel', {title, errors})
    }

    async editChannel(req, res) {
        if(! ObjectId.isValid(req.params.id) ){
            req.flash('errors', 'آی دی صحیح نیست')
            return res.redirect('/admin/channel-management')
        }

        const channelInfo = await channelServices.info(req.params.id)
        const title = 'تغییر کانال';
        const errors = req.flash('errors');
        return res.render('admin/channel/edit-channel', {title, channel: channelInfo, errors})
    }

    async viewChannel(req, res) {

        if(! ObjectId.isValid(req.params.id) ){
            req.flash('errors', 'آی دی صحیح نیست')
            return res.redirect('/admin/channel-management')
        }

        const channelInfo = await channelServices.info(req.params.id);

        const allVideos = await channelServices.allVideos(req.params.id);

        if(!channelInfo) {
            req.flash('errors', 'کانال یافت نشد')
            return res.redirect('/admin/channel-management')
        }

        // console.log(channelInfo)
        const title = channelInfo.name;
        return res.render('admin/channel/channel', {title, channel: channelInfo, videos: allVideos})
    }

    async deleteChannel(req, res) {

        if(! ObjectId.isValid(req.params.id) ){
            req.flash('errors', 'آی دی صحیح نیست')
            return res.redirect('/admin/channel-management')
        }

        const channelInfo = await channelServices.info(req.params.id);

        const allVideos = await channelServices.allVideos(req.params.id);

        if(!channelInfo) {
            req.flash('errors', 'کانال یافت نشد')
            return res.redirect('/admin/channel-management')
        }

        const errors = req.flash('errors')
        const title = 'حذف کانال'

        return res.render('admin/channel/delete-channel', {title, errors, channel: channelInfo, videos: allVideos})
    }

    async uploadVideo(req, res) {

        if(! ObjectId.isValid(req.params.id) ){
            req.flash('errors', 'آی دی صحیح نیست')
            return res.redirect('/admin/channel-management')
        }

        const channelName = await channelServices.channelName(req.params.id)
        const title = 'آپلود ویدیو';
        const errors = req.flash('errors')
        const channel = req.params.id
        return res.render('admin/channel/upload-video', {title, channelName, errors, channel})
    }

    async viewVideo (req, res) {
        // console.log('id', req.params.id)
        // i
        if(! ObjectId.isValid(req.params.id) ){
            req.flash('errors', 'آی دی صحیح نیست')
            return res.redirect(`admin/channel-management`)
        }

        const videoUrl = await channelServices.getVideo(req.params.id);
        const title = 'تماشای ویدیو'
        const channel = (await channelServices.findChannelFromVideo(req.params.id)).channel
        return res.render('admin/channel/view-video', {title, videoUrl, channel})
    }

    async deleteVideo(req, res) {
        // console.log('id', req.params.id)
        // i
        if(! ObjectId.isValid(req.params.id) ){
            req.flash('errors', 'آی دی صحیح نیست')
            return res.redirect(`admin/channel-management`)
        }

        const videoUrl = await channelServices.getVideo(req.params.id);
        const title = 'حذف ویدیو'
        const channel = (await channelServices.findChannelFromVideo(req.params.id)).channel
        return res.render('admin/channel/delete-video', {title, videoUrl, channel})
    }

    async updateChannel(req, res) { 
        if(!req.file){
            req.flash('errors', 'فایل را آپلود کنید')
            return res.redirect(`/admin/edit-channel/${req.body.id}`)
        }

        if(req.body.channelName.trim() == ''){
            req.flash('errors', 'درج نام کانال الزامی است')
            return res.redirect(`/admin/edit-channel/${req.body.id}`)
        }

        const isChannelNameUnique = await channelServices.isChannelNameUnique(req.body.channelName.trim(), req.body.id);
        if(!isChannelNameUnique) {
            req.flash('errors', 'نام کانال تکراری است')
            return res.redirect(`/admin/edit-channel/${req.body.id}`)
        }

        // update channel
        const { id, channelName, free, type } = req.body;

        const update = await channelServices.updateChannel(id, free, type, req.file.path, channelName)

        // return res.json(update)
        return res.redirect(`/admin/channel-management`)

    }

    async insertChannel(req, res) {

        if(!req.file){
            req.flash('errors', 'فایل را آپلود کنید')
            return res.redirect(`/admin/create-channel`)
        }

        if(req.body.channelName.trim() == ''){
            req.flash('errors', 'درج نام کانال الزامی است')
            return res.redirect(`/admin/create-channel`)
        }

        const isChannelUnique = await channelServices.isChannelUnique(req.body.channelName.trim());
        if(!isChannelUnique) {
            req.flash('errors', 'نام کانال تکراری است')
            return res.redirect(`/admin/create-channel`)
        }   

        // create channel
        const { channelName, free, type } = req.body
        const create = await channelServices.createChannel(channelName, free, type, req.file.path)
        if(!create) {
            req.flash('errors', 'مشکلی در ایجاد فایل به وجود امده است')
            return res.redirect(`/admin/create-channel`)
        }

        return res.redirect(`/admin/channel-management`)
    }

    async removeChannel(req, res){
        if(! ObjectId.isValid(req.params.id) ){
            req.flash('errors', 'آی دی صحیح نیست')
            return res.redirect('/admin/channel-management')
        }

        // check if channel exists
        const channel = await channelServices.isChannelExists(req.params.id)
        if(!channel) {
            req.flash('errors', 'کانال وجود ندارد')
            return res.redirect('/admin/channel-management')
        }

        // delete all channelVideos
        const deleteAllVideos = await channelServices.deleteChannelVideos(req.params.id);

        // delete channel
        const deleted = await channelServices.deleteChannel(req.params.id)

        return res.redirect('/admin/channel-management')
    }

    async uploadChannelVideo(req, res) {
        if(!req.file) {
            req.flash('errors', 'فایل ویدیو را وارد کنید')
            return res.redirect(`/admin/upload-video/${req.body.id}`)
        }

        // is channelExists
        const isChannelExists = await channelServices.isChannelExists(req.body.id);
        if(!isChannelExists) {
            req.flash('errors', 'کانال وجود ندارد')
            return res.redirect(`/admin/upload-video/${req.body.id}`)
        }

        // upload
        const uploaded = await channelServices.uploadVideo(req.body.id, req.file.path)

        // console.log(req.body)
        // console.log(req.file)
        if(!uploaded) {
            req.flash('errors', 'مشکلی در اپلود پیش امده. بعدا تلاش کنید')
            return res.redirect(`/admin/upload-video/${req.body.id}`)
        }

        return res.redirect(`/admin/view-channel/${req.body.id}`)
    }

    async removeVideo(req, res) {
        // const id = req.parmas.id

        if(! ObjectId.isValid(req.params.id) ){
            req.flash('errors', 'آی دی صحیح نیست')
            return res.redirect('/admin/channel-management')
        }

        // check if video exists 
        const isVideoExists = await channelServices.isVideoExists(req.params.id)
        if(!isVideoExists) {
            req.flash('errors', 'ویدیو وجود ندارد')
            return res.redirect('/admin/channel-management')
        }

        // delete video
        const deleted = await channelServices.deleteVideo(req.params.id)
        if(!deleted) {
            req.flash('errors', 'مشکلی در حذف فایل رخ داده')
            return res.redirect('/admin/channel-management')
        }

        return res.redirect('/admin/channel-management')

    }

    async setFirstPage (req, res) {
        return res.json('set first page')
    }

    async setBannerVideo(req, res) {
        console.log(req.params.id)
        if(! await this.validationForm(req) ) return res.redirect(`/admin/channel-management`)
        
        const id = req.params.id;
        const isVideoExists = await channelServices.isVideoExists(req.params.id)
        if(!isVideoExists) {
            req.flash('errors', 'ویدیو وجود ندارد')
            return res.redirect('/admin/channel-management')
        }

        // remove other banner
        const removeOtherBanner = await channelServices.removeBanner();
        if(!removeOtherBanner.ok) {
            req.flash('errors', 'مشکلی در ذخیره تغییران بوجود امده')
            return res.redirect(`/admin/view-video/${req.params.id}`)
        }

        const setBanner = await channelServices.setBanner(req.params.id);
        if(! setBanner.ok) {
            req.flash('errors', 'مشکلی در ذخیره تغییران بوجود امده')
            return res.redirect(`/admin/view-video/${req.params.id}`) 
        }
        
        
        const channel = await channelServices.findChannelFromVideo(req.params.id);
        return res.redirect(`/admin/view-channel/${channel.channel}`)
    }

}

module.exports = new channelController();