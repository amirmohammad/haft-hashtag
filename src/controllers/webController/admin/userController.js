
const userServices = require('../../../services/web/usersServices')
const ObjectId = require('mongoose').Types.ObjectId;

const controller = require('../../index')

class userController extends controller {

    async usersPage(req, res) {

        const title = 'مدیریت کاربران'
        const errors = req.flash('errors')

        const users = await userServices.last100Users()

        return res.render('admin/users/users', {title, errors, users})
    }

    async viewUserPage (req, res) {

        if(! await this.validationForm(req) ) return res.redirect('/admin/user');

        // now id is an mongoose object 
        // check if userFounds
        const user = await userServices.user(req.params.id)
        const title = user.name;
        const errors = req.flash('errors');

        return res.render('admin/users/view-user', {title, errors, user })
    }

    async delteUserPage (req, res) {
        if(! await this.validationForm(req) ) return res.redirect('/admin/user');

        const title = 'حذف کاربر'
        const errors = req.flash('errors')
        const user = await userServices.user(req.params.id);

        return res.render('admin/users/delete-user', {title, errors, user});
    }

    async removeUser (req, res) {
        if(! await this.validationForm(req) ) return res.redirect('/admin/user');

        const isUserExists = await userServices.isUserExists(req.params.id)
        if(!isUserExists) {
            req.flash('errors', 'کاربر وجود ندارد')
            return res.redirect('/admin/user');
        }

        const deleted = await userServices.deleteUser(req.params.id);

        if(!deleted.ok) {
            req.flash('errors', 'مشکلی در حذف کاربر به وجود امده');
            return res.redirect('/admin/user')
        }

        return res.redirect('/admin/user')
    }

}

module.exports = new userController();