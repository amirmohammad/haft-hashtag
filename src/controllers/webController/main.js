
const controller = require('../index')

class mainController extends controller {

    mianRoute(req, res) {
        // console.log(req.user.admin) 
        return  res.render('main', { layout: false} );
    }

    uploadForm(req, res) {
        return res.render('upload');
    }

}

module.exports = new mainController()