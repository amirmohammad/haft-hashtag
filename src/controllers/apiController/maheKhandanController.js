const controller = require('../index')

const { uploadVideoSVC, sendCommentSVC, getVideosSVC, deleteVideoSVC, getAllVideosSVC, updateCommentSVC, deleteCommentSVC, videoExistsSVC, reactionsSVC, sendReactionSVC, sendReactionBeforeSVC, updateRactionSVC } = require('../../services/maheKahndanService')

const maheKhandanTransform = require('../../transforms/v1/maheKhandanTransform');


class maheKhandanController extends controller {
    
    async category(req, res, next) {
        return  res.json('mahe khandan category');
    }

    async charity(req, res, next) {
        return  res.json('mahe khandan charity');
    }

    async uploadVideo(req, res ){
        
        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(404).json({
                methode: 'updateAds',
                code : '404',
                status: 'error',
                message : 'you should upload image',
                data:{}
            })
        }

        const video = req.files.video; 
        // const filename = image.name.replaceAll(" ", "%20")
        const uploadPath = `./uploads/videos/mahekhandan` + new Date().toISOString() +  '-' + video.name;

        // upload file
        const upload = await video.mv(uploadPath, function(err) {
            if (err) {
                return res.status(500).json({
                    methode: 'uploadVideo',
                    code : '500',
                    status: 'error',
                    message : 'unable to upload video',
                    data:{}
                })
            }
        });

        const path = uploadPath.replace("./", "");
        
        const uploaded = await uploadVideoSVC(path)

        if(!uploaded.success) return  res.status(500).json(uploaded);

        const result = await maheKhandanTransform.saveTransform(uploaded.description)

        return  res.status(200).json(result);
    }

    async getVideo(req, res ){
        if(! await this.validationData(req, res)) return;

        const video = await getVideosSVC(req.body.video)

        

        return  res.json(video);
    }

    async getAllVideos(req, res ){

        const allVideos = await getAllVideosSVC();
        return  res.json(allVideos);
    }

    async deleteVideo(req, res ){
        if(! await this.validationData(req, res)) return;

        const deleted = await deleteVideoSVC(req.body.video)

        return  res.json(deleted);
    }

    async sendComment(req, res) {
        if(! await this.validationData(req, res)) return;
        const {video, text} = req.body; 
        const commentSent = await sendCommentSVC(req.user._id, video, text)

        return  res.json(commentSent);
    }

    async getComment(req, res) {
        return  res.json('get comment');
    }

    async updateComment(req, res) {
        if(! await this.validationData(req, res)) return;

        const {comment, newComment} = req.body
        const updated = await updateCommentSVC(comment, newComment) 

        return  res.json(updated);
    }

    async deleteComment(req, res)  {
        if(! await this.validationData(req, res)) return;

        const deleted = await deleteCommentSVC(req.body.comment, req.user._id)
        return  res.json(deleted);
    }

    async allReactions(req, res) {
        if(! await this.validationData(req, res)) return;

        const videoExists = await videoExistsSVC(req.body.video);
        if(!videoExists) {
            return res.json({
                status: '404',
                message: 'video not found',
                success: false
            });
        }

        const reactions = await reactionsSVC(req.body.video)

        const zeros = await reactions.filter( reaction => reaction.rate == '0')
        const ones = await reactions.filter( reaction => reaction.rate == '1')
        const twos = await reactions.filter( reaction => reaction.rate == '2')
        const threes = await reactions.filter( reaction => reaction.rate == '3')

        const rates = {
            zeros: zeros.length,
            ones: ones.length,
            twos: twos.length,
            threes: threes.length,
        }


        return res.status(200).json({
            status: '200',
            message: rates,
            success: true
        }) 
    }

    async sendReaction(req, res) {
        if(! await this.validationData(req, res)) return;

        const videoExists = await videoExistsSVC(req.body.video);
        if(!videoExists) {
            return res.json({
                status: '404',
                message: 'video not found',
                success: false
            });
        }

        const sentReactionBefore = await sendReactionBeforeSVC(req.user.id, req.body.video);

        if(!sentReactionBefore) {
            const sendReaction = await sendReactionSVC(req.user.id, req.body.video, req.body.point);
            if(!sendReaction) {
                return res.status(500).json({
                    status: '500',
                    message: 'unable to save',
                    success: false
                })
            }

            return res.status(200).json({
                status: '200',
                message: sendReaction,
                success: true,
            })
        }

        // update reaction
        const updated = await updateRactionSVC(sentReactionBefore._id, req.body.point)

        return res.status(200).json({
            status: '200',
            message: updated,
            success: true,
        });

    }
}

module.exports = new maheKhandanController();