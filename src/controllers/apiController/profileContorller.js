const controller = require('../index')

const profileServices = require('../../services/profileService')

class profileController extends controller {
    
    async myData(req, res, next) {
        return  res.json('my data');
    }

    async seeCompanyInfo(req, res, next) {
        return  res.json('see company info');
    }

    async changeCompanyInfo(req, res, next) {
        return  res.json('changeCompanyInfo');
    }

    async createCompany(req, res, next) {
        return  res.json('create company');
    }

    async uploadGallery(req, res, next) {

        if(!req.file){
            return res.status(404).json({
                methode: 'updateArticle',
                code : '404',
                status: 'error',
                message : 'you should upload image',
                data:{}
            })
        }
        return  res.json('uploadGallery');
    }
    
    async deleteGallery(req, res, next) {
        return  res.json('deleteGallery');
    }

    async seeGallery(req, res, next) {
        if(! await this.validationData(req, res)) return;

        // check if user type compnay
        const isUserRoleCompany = await profileServices.isUserRoleCompany(req.user.id)
        
        if(!isUserRoleCompany){
            return res.status(403).json({
                status: '403',
                message: 'شما کمپانی نیستید',
                success: 'false',
            })
        }

        const company = await profileServices.getCompanyFromToken(req.user.id);
        if(!company) {
            return res.status(404).json({
                status: '404',
                success: false,
                message: 'company not found'
            })
        }

        const gallery = await profileServices.companyGallery(company._id);

        return res.status(200).json({
            status: 200,
            success: true,
            message: gallery
        })
    }

    async seeCertificate(req, res, next) {
        if(! await this.validationData(req, res)) return;

        // check if user type compnay
        const isUserRoleCompany = await profileServices.isUserRoleCompany(req.user.id)
        
        if(!isUserRoleCompany){
            return res.status(403).json({
                status: '403',
                message: 'شما کمپانی نیستید',
                success: 'false',
            })
        }
        const company = await profileServices.getCompanyFromToken(req.user.id);
        if(!company) {
            return res.status(404).json({
                status: '404',
                success: false,
                message: 'company not found'
            })
        }

        const certificate = await profileServices.companyCertificate(company._id);

        return res.status(200).json({
            status: 200,
            success: true,
            message: certificate
        })
    }

    async uploadCertificate(req, res, next) {
        return  res.json('uploadCertificate');
    }

    async deleteCertificate(req, res, next) {
        return  res.json(' deleteCertificate');
    }
}

module.exports = new profileController();