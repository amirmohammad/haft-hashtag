
const controller = require('../index')
const multer = require('multer')

// services
const {saveAdsService, getAdsService, setWallpaper} = require('../../services/adsService');

// transforms
const {saveAds, getAds} = require('../../transforms/v1/adsTransform')

class adsController extends controller {

    async getAds(req, res, next) {

        const ads = await getAdsService();

        const result = await getAds(ads)

        return  res.json(result);
    }

    async uploadAds(req, res, next) {

        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(404).json({
                methode: 'updateAds',
                code : '404',
                status: 'error',
                message : 'you should upload image',
                data:{}
            })
        }

        const image = req.files.image; 
        const uploadPath = `./uploads/images/ads/` + new Date().toISOString() +  '-' + image.name;

        // upload file
        const upload = await image.mv(uploadPath, function(err) {
            if (err) {
                return res.status(500).json({
                    methode: 'updateAds',
                    code : '500',
                    status: 'error',
                    message : 'unable to upload image',
                    data:{}
                })
            }
        });

        const { description, link } = req.body;

        // const filePath = req.file.path.replace(" ", "%20")
        const saved = await saveAdsService(uploadPath, link, description); 
        // transform to show result
        const result = await saveAds(saved)

        return  res.json(result);
    }

    async wallpaper(req, res) {
        if(!req.file){
            return res.status(404).json({
                methode: 'ads wallpaper',
                code : '404',
                status: 'error',
                message : 'you should upload image',
                data:{}
            })
        }


        // const path = this.normalizePath(req.file.path)
        // return  res.json(path);
        const filePath = req.file.path.replace(" ", "%20")
        const saveWallpaper = await setWallpaper(filePath)
        return  res.json(saveWallpaper);
    }
}



module.exports = new adsController(); 