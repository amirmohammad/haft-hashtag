
const controller = require('../index')

const {createCategoryService, updateCategoryService, seeCategoryService, deleteCategoryService} = require('../../services/bankIdea/categoryServices')
const {saveIdeaService, getIdeaService} = require('../../services/bankIdea/ideaServices')

// transform
const ideaTransform = require('../../transforms/v1/ideaTransform')

class bankIdeaController extends controller {

    async createCategory(req, res){

        if(! await this.validationData(req, res)) return; 

        const {category} = req.body; 

        const created = await createCategoryService(category)

        if(! created.success) return res.json(created);

        return  res.json(created.success);
    }

    async deleteCategory(req, res){
        if(! await this.validationData(req, res)) return; 

        const {category} = req.body;

        const deleted = await deleteCategoryService(category)

        if(!deleted.success) return  res.json(delted);

        return  res.json(deleted.success);
    }

    async updateCategory(req, res){
        if(! await this.validationData(req, res)) return; 

        const {newname, oldname} = req.body;

        const updated = await updateCategoryService(oldname, newname);

        if(!updated.success) return res.json(updated);

        return  res.json(updated.success);
    }

    async seeCategory(req, res){
        const result = await seeCategoryService()

        return  res.json(result);
    }

    async uploadIdea(req, res) {

        if(! await this.validationData(req, res)) return; 

        const { description, request, type, category } = req.body;
        const firstname = req.body.firstname || null;
        const lastname = req.body.lastname || null ;

        const saveIdea = await saveIdeaService(req.user._id, firstname, lastname, type, category, request, description)

        if(!saveIdea.success) return  res.json(saveIdea);

        const result = await ideaTransform.saveTransform(saveIdea.description)

        return  res.json(result);
    }

    async getIdeas(req, res) {

        if(! await this.validationData(req, res)) return; 

        const {category} = req.body;

        const seeIdeas = await getIdeaService(category)

        if(!seeIdeas.success) return  res.json(seeIdeas);

        const result = await ideaTransform.getTransform(seeIdeas.description)

        return  res.json(result);
    }
}

module.exports = new bankIdeaController(); 