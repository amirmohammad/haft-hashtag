const controller = require('../index')

const { createCategoryService, seeAllCategories, updateCategoryService, deleteCategoryService } = require('../../services/jobsdb/categoryServices')

// transform
const { saveCategoryTransform, seeAllCategoriesTransform} = require('../../transforms/v1/jobsDBTransform');
const { update } = require('../../models/user');

class jobsDBController extends controller {
    
    async seeCategory(req, res, next) {
        
        const categories = await seeAllCategories();

        const result = await seeAllCategoriesTransform(categories)

        return  res.json(result);
    }

    async list(req, res, next) {
        return  res.json('list');
    }

    async description(req, res, next) {
        return  res.json('description');
    }

    async imageGallery(req, res, next) {
        return  res.json('image gallery');
    }

    async certificates(req, res, next) {
        return  res.json('certificate');
    }

    async createCategory(req, res, next) {
        if(! await this.validationData(req, res)) return;

        const { categoryName } = req.body;

        const saveCategory = await createCategoryService(categoryName)

        if(! saveCategory.success ){
            return  res.json(saveCategory);
        }

        const result = await saveCategoryTransform(saveCategory.description)

        return  res.json(result);
    }

    async updateCategory(req, res, next ) {
        if(! await this.validationData(req, res)) return;
        const {oldCategoryName, newCategoryName} = req.body;

        const updateCategory = await updateCategoryService(oldCategoryName, newCategoryName);

        if(!updateCategory.success){
            return  res.json(updateCategory);
        }

        else{
            return  res.json({
                success: true
            });
        }
        
    }

    async deleteCategory(req, res, next) {
        if(! await this.validationData(req, res)) return;

        const {categoryName} = req.body;

        const deleteResult = await deleteCategoryService(categoryName)

        if(!deleteResult.success) {
            return  res.json(deleteResult);
        }

        

        return  res.json({
            success: true,
            deleted: categoryName
        });
    }
}
module.exports = new jobsDBController();