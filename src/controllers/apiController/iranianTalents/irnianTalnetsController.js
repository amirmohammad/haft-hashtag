const controller = require('../../index')

class iranianTalentController extends controller {
    
    async getCategories(req, res){
        return  res.json('get categories');
    }

    async createCategory(req, res){
        return  res.json('createCategory');
    }

    async deleteCategory(req, res){
        return  res.json('deleteCategory');
    }

    async updateCategory(req, res){
        return  res.json('updateCategory');
    }

    async getCategoryVideos(req, res){
        return  res.json('getCategoryVideos');
    }

    
    async getVideo(req, res){
        return  res.json('getVideos');
    }

    async sendVideo(req, res){
        return  res.json('sendVideos');
    }

    async deleteVideo(req, res){
        return  res.json('deleteVideos');
    }


    async sendComment(req, res){
        return  res.json('sendComment');
    }

    async deleteComment(req, res){
        return  res.json('deleteComment');
    }

    async updateComment(req, res){
        return  res.json('updateComment');
    }



    async sendPoint(req, res){
        return  res.json('sendPoint');
    }

    async getPoint(req, res){
        return  res.json('getPoint');
    }

    async getPointsForVideo(req, res){
        return  res.json('getPointsForVideo');
    }
}

module.exports = new iranianTalentController();