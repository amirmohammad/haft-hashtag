const controller = require('../../index')

const {createCategory, getCategoies, updateCategoy, deleteCategoy, getCategoyVideos, } = require('../../../services/iranianTalents/categoryServices');

class iranianTalentCategoryController extends controller {
    
    async getCategories(req, res){
        const categories = await getCategoies();

        return  res.json(categories);
    }

    async createCategory(req, res){

        if(! await this.validationData(req, res)) return;

        const created = await createCategory(req.body.category)

        return  res.json(created);
    }

    async deleteCategory(req, res){
        if(! await this.validationData(req, res)) return;

        const deleted = await deleteCategoy(req.body.category)

        return  res.json(deleted);
    }

    async updateCategory(req, res){
        if(! await this.validationData(req, res)) return;

        const {oldCategory, newCategory} = req.body; 

        const updated = await updateCategoy(oldCategory, newCategory);

        return  res.json(updated);
    }

    async getCategoryVideos(req, res){
        if(! await this.validationData(req, res)) return;

        const videos = await getCategoyVideos(req.body.category)

        return  res.json(videos);
    }

}

module.exports = new iranianTalentCategoryController();