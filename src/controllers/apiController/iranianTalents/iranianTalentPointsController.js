const controller = require('../../index')

const {sendPoint, getPointsForVideo, getPoint, } = require('../../../services/iranianTalents/pointServices')

class iranianTalentController extends controller {
    async sendPoint(req, res){
        if(! await this.validationData(req, res)) return;
        const {point, video} = req.body;

        const sent = await sendPoint(req.user._id, video, point)

        return  res.json(sent);
    }

    async getPoint(req, res){
        if(! await this.validationData(req, res)) return;

        const point = await getPoint(req.user._id, req.body.video)

        return  res.json(point);
    }

    async getPointsForVideo(req, res){
        if(! await this.validationData(req, res)) return;

        const points = await getPointsForVideo(req.body.video)

        return  res.json(points);
    }
}

module.exports = new iranianTalentController();