const controller = require('../../index')

// services
const {sendVideo, deleteVideo, getVideo} = require('../../../services/iranianTalents/videoServices');
const { uploadVideo } = require('../../../middlewares/uploadMiddlewares/video/IranianTalentsUpload');

class iranianTalentController extends controller {
    
    async getVideo(req, res){
        if(! await this.validationData(req, res)) return;

        const loaded = await getVideo(req.body.video)

        return  res.json(loaded);
    }

    async sendVideo(req, res){
        if(! await this.validationData(req, res)) return;
        
        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(404).json({
                methode: 'updloadProfileImage',
                code : '404',
                status: 'error',
                message : 'you should upload image',
                data:{}
            })
        }

        let video = req.files.video;
        // video.name = video.name.replaceAll(" ", "");
        video.name = video.name.replace(/\s/g, '');
        const uploadPath = `./uploads/videos/iraniantalents/` + new Date().toISOString() +  '-' + video.name;

        const upload = await video.mv(uploadPath, function(err) {
            if (err) {
                return res.status(500).json({
                    methode: 'sendVideo',
                    code : '500',
                    status: 'error',
                    message : 'unable to upload video',
                    data:{}
                })
            }
        });
        const filePath = uploadPath.replace("./", "")
        const saved = await sendVideo(req.query.category, filePath)

        return  res.json(saved);
    }

    async deleteVideo(req, res){
        if(! await this.validationData(req, res)) return;

        const deleted = await deleteVideo(req.body.video)
        
        return  res.json(deleted);
    }
}

module.exports = new iranianTalentController();