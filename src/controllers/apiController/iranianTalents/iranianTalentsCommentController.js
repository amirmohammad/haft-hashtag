const controller = require('../../index')

const {sendComment, deleteComment, updateComment, getCommentsForVideo} = require('../../../services/iranianTalents/commentServices')

class iranianTalentCommentController extends controller {

    async sendComment(req, res){

        if(! await this.validationData(req, res)) return;

        const {comment, video} = req.body;
        const sent = await sendComment(req.user._id, video, comment)

        return  res.json(sent);
    }

    async deleteComment(req, res){
        if(! await this.validationData(req, res)) return;

        const deleted = await deleteComment(req.user._id ,req.body.comment)

        return  res.json(deleted);
    }

    async updateComment(req, res){
        if(! await this.validationData(req, res)) return;

        const {comment, newComment} = req.body;

        const updated = await updateComment(req.user._id, comment, newComment)

        return  res.json(updated);
    }

    async getComments(req, res) {
        if(! await this.validationData(req, res)) return;

        const comments = await getCommentsForVideo(req.body.video)

        return  res.json(comments);
    }
}

module.exports = new iranianTalentCommentController();