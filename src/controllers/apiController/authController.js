
const controller = require('../index')
const jwt = require('jsonwebtoken')

// services
const { saveUser, isUserExists, passwordMatches, createJWTToken, editUser, uploadImageService, deleteImageService, getUserInfoService, isUserVerifiedNumber } = require('../../services/userServices')

// transofrms
const { saveUserTransform } = require('../../transforms/v1/userTransform');
const { check } = require('express-validator');
const userTransform = require('../../transforms/v1/userTransform');

class authController extends controller {

    //fixed : login only by number
    async login(req, res) {

        if(! await this.validationData(req, res)) return;

        let phoneNumber = req.body.phoneNumber.trim().toLowerCase();
        // let password = req.body.password.trim().toLowerCase();

        const isUser = await isUserExists(phoneNumber)

        if(!isUser) {
            return  res.json('user does not exists');
        }

        //check if users number verified before
        const isVerified = await isUserVerifiedNumber(phoneNumber)
        if(!isVerified) {
            return  res.json({
                success: false,
                status: 403,
                description: 'user is not actived'
            })
        }

        // const cehckpassword = await passwordMatches(phoneNumber, password);

        // if(!cehckpassword.success){
        //     return  res.json('password does not match');
        // }

        const token = await createJWTToken(phoneNumber)

        return res.status(200).json({
            status : "OK",
            message : "login successful",
            code : '200',
            methode : "login",
            data:{
                token,
            }
        })

    }

    async signup(req, res) {
        // return res.json(req.body)

        if(! await this.validationData(req, res)) return;

        const saved = await saveUser(req.body)

        if (saved.success){
            const result = await saveUserTransform(saved.description, saved.token)
        }
        else{
            const result = saved
        }


        return  res.json(result);
    }

    async edit(req, res) {
        if(! await this.validationData(req, res)) return;

        const {gender, personalCodeID, name} = req.body;

        const state = req.body.state || null;
        const city = req.body.city || null;
        const birthdate = req.body.birthdate || null;
        const role = req.body.role || 'normalUser';

        const edited = await editUser(req.user._id, name, personalCodeID, gender, state, city, birthdate, role)

        return res.json(edited)
    }

    async updloadProfileImage(req, res) { 


        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(404).json({
                methode: 'updloadProfileImage',
                code : '404',
                status: 'error',
                message : 'you should upload image',
                data:{}
            })
        }

        let image = req.files.image;
        // image.name = image.name.replaceAll(" ", "");
        image.name = image.name.replace(/\s/g, '')
        const uploadPath = `./uploads/images/profile/` + new Date().toISOString() +  '-' + image.name;

        const upload = await image.mv(uploadPath, function(err) {
            if (err) {
                return res.status(500).json({
                    methode: 'updloadProfileImage',
                    code : '500',
                    status: 'error',
                    message : 'unable to upload image',
                    data:{}
                })
            }
        });

        const filePath = uploadPath.replace("./", "")
        const uploaded = await uploadImageService(req.user._id, filePath);

        return  res.json(uploaded); 
    }

    async getUserInfo(req, res) {

        const userinfo = await getUserInfoService(req.user._id)

        const result = await userTransform.getUserInfoTransform(userinfo);

        return  res.json(result);
    }

    async authStatus(req, res, status) {
        if(status == 'login'){
            await this.login(req, res)
        }
        else{
            await this.signup(req, res)
        }
    }
}

module.exports = new authController(); 