
const controller = require('../index')

// services
const { uploadCertificateService, deleteCertificateService, updateCertificateService, uploadCert, seeCertificateService} = require('../../services/jobsdb/companyCertificateServices')

// transform
const {seeCompanyCertificateTransform} = require('../../transforms/v1/companyCertificateTransform');
const { iscompanyExists } = require('../../services/jobsdb/companyServices');

class authController extends controller {

    async uploadCertificate(req, res){

        if(! await this.validationData(req, res)) return; 

        if(!req.body.company) {
            return res.status(404).json({
                methode: 'uploadCertificate',
                code : '404',
                status: 'error',
                message : 'you should upload company name',
                data:{}
            })
        }
        const {company} = req.body;

        const compnayExists = await iscompanyExists(company);
        if(!compnayExists) {
            return res.status(404).json({
                methode: 'uploadCertificate',
                code : '404',
                status: 'error',
                message : 'company not exists',
                data:{}
            })
        }

        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(404).json({
                methode: 'uploadCertificate',
                code : '404',
                status: 'error',
                message : 'you should upload image certicates',
                data:{}
            })
        }

        // let image = req.files.image;
        
        let file = req.files.image;
        for ( let f of file ) {
            // f.name = f.name.replaceAll(" ", "");
            f.name = f.name.replace(/\s/g, '')
        }
        
        let uploadPath = [];

        for(let i in file){

            let path = './uploads/images/company/certificate/'+ new Date().toISOString() +  '-' + file[i].name;
            uploadPath.push(path)
            file[i].mv(path, function (err){
                if(err){
                    return res.status(500).json({
                        methode: 'uploadCertificate',
                        code : '500',
                        status: 'error',
                        message : 'unable to save file',
                        data:{}
                    })
                }

            })

        }

        // return res.json(uploadPath)
        for (let p in uploadPath) { 
            uploadPath[p] = uploadPath[p].replace("./", "");
        }

        // const uploaded  = await uploadCertificateService(files, company)
        const uploaded = await uploadCert(company, uploadPath)

        // return  res.json(uploaded.success);
        return res.status(200).json({
            methode: 'uploadCertificate',
            code : '200',
            status: 'success',
            message : uploaded,
            data:{}
        })
    }
    
    async updateCertificate(req, res){
        if(! await this.validationData(req, res)) return; 
        return  res.json('updte certificate');
    }    
    

    async deleteCertificate(req, res){
        if(! await this.validationData(req, res)) return; 
        
        const { image } = req.body;

        const deleted = await deleteCertificateService(image);

        if(!deleted.success) return  res.json(deleted);

        return  res.json(deleted.success);
    }    

    async seeCertificate(req, res){
        if(! await this.validationData(req, res)) return; 

        const {company} = req.body; 

        const seeCertificateResult = await seeCertificateService(company)

        const result = await seeCompanyCertificateTransform(seeCertificateResult)

        return  res.json(result);
    }    

    
}

module.exports = new authController(); 