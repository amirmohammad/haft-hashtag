const config = require('../../config')
const controller = require('../index');

// svc
const {getTVWallPaper, getAds, getTVChannels} = require('../../services/routesServices');
const { Result } = require('express-validator');

class routeController extends controller {
    
    async splashScreen(req, res, next) {

        const options = await {
            page: "SplashScreen",
            required_update: config.settings.required_update,
            has_non_required_update: config.settings.has_non_required_update,
        }

        return  res.json(options);
    }

    mainMenu(req, res, next) {
        return  res.json('main menu');
    }

    profileRoute(req, res, next) {
        return  res.json('profile route');
    }

    advertisementRoute(req, res, next){
        return  res.json('advertisment route');
    }

    settingsRoute(req, res, next) {
        return  res.json('settings route');
    }

    guide(req, res, next) {
        return  res.json('guides here ');
    }

    async homepage(req, res) {

        const activeChannels = await getTVChannels();
        const adsWallPaper = await getAds();
        // const adPath = await this.normalizePath(adsWallPaper.bannerImage)
        const tvWallpaper = await getTVWallPaper();

        return  res.json({
            success: true,
            status: 200,
            activeChannels,
            adsWallPaper,
            tvWallpaper
        });
    }

}

module.exports = new routeController(); 