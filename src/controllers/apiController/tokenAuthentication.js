 
const phonenumberValidator = require('../../validators/phonenumber-validator');
const controller = require('../index');
const moment = require('moment-jalaali');


// services
const {sendSMSService, verifyTokenSVC, isUserExists, deletePhoneNumberifExists, sms, saveTokenInDB, phoneNumberData, updateActivation } = require('../../services/tokenServices')

// controllers
const authController = require('./authController');

class routeController extends controller {
    
    // async sendSMSToken0(req, res, next){

    //     if(! await this.validationData(req, res)) return; 
        
    //     const { phoneNumber } = req.body;

    //     const sendSMS = await sendSMSService(phoneNumber)

    //     if(!sendSMS.success) return res.status(500).json(sendSMS);

    //     // use kavenegar api later ...
    //     return  res.status(200).json(sendSMS);
    // }

    async sendSMSToken(req, res) {
        if(! await this.validationData(req, res)) return; 

        const { phoneNumber } = req.body;
        const smsToken = this.rand()

        // const check for phoneNumber in database that has sent sms befor,
        // if yes delete the database to save new code in that
        const ifNumberExistsDelete = await deletePhoneNumberifExists(phoneNumber)

        const sendNewSMS = await sms(phoneNumber, smsToken)

        if(sendNewSMS.status != '200') {
            console.log(phoneNumber, 'failed to send sms token')
            return res.status(500).json({
                success: false,
                status: '500',
                message: 'error in sending sms, check panel'
            })
        }

        const expire = moment().add(1, 'day').format('jYYYY/jMM/jDD HH:mm')
        const saveInfo = await saveTokenInDB(phoneNumber, smsToken, expire)
        if(!saveInfo) {
            return res.status(500).json({
                success: false,
                description: 'error in saving in database',
                status: '500'
            })
        }

        
        return res.status(200).json({
            success: true,
            description: 'message sent',
            status: 200
        });
    }

    async verifySMSToken0(req, res, next){
        if(! await this.validationData(req, res)) return;

        const { phoneNumber, token } = req.body;

        const verified = await verifyTokenSVC(token, phoneNumber)

        if(!verified.success) return res.json(verified)

        //login or signup
        const userExists = await isUserExists(phoneNumber)
        const authStatus = (userExists) ? 'login' : 'signup';
        return await authController.authStatus(req, res, authStatus)

        // check activation code database and confirm login
    }

    async verifySMSToken(req, res, next) {
        if(! await this.validationData(req, res)) return;

        const { phoneNumber, token } = req.body;

        // get phonenumber and password from database
        const numberData = await phoneNumberData(phoneNumber);
        if(!numberData) {
            return res.status(404).json({
                success: false,
                status: 404,
                message: 'phone number not found'
            })
        }

        // if code used before return error
        if(numberData.used) {
            return res.status(403).json({
                success: false,
                status: '403',
                message: 'token is used before'
            })
        }

        // check if token is correct 
        if(numberData.code !== token) {
            return res.status(403).json({
                success: false,
                status: '403',
                message: 'token is incorrect'
            })
        }

        // token is correct now. 
        // update token as used
        const updateActivationToUsed = await updateActivation(phoneNumber);
        if(!updateActivationToUsed.ok ) {
            return res.status(500).json({
                success: false,
                status: '500',
                message: 'activation failed'
            })
        }

        //login or signup
        const userExists = await isUserExists(phoneNumber)
        const authStatus = (userExists) ? 'login' : 'signup';
        return await authController.authStatus(req, res, authStatus)

    }

    rand(min = 1000, max = 9999) {  
        let randomNum = Math.random() * (max - min) + min;  
        return Math.floor(randomNum);
    }


}

module.exports = new routeController(); 