
const controller = require('../index')

const {createCompanyService, getAllCompaniesService, getCompany, deleteCompanyService, updateCompanyService, isOwnerTypeCompany, iscompanyExists} = require('../../services/jobsdb/companyServices')
const {createCompanyGalleryService, seeCompanyGalleryService, updateCompanyGalleryService, deleteCompanyGalleryService, saveGallery} = require('../../services/jobsdb/companyGalleryServices')

const { createCompanyTransform, getCompaniesTransform } = require('../../transforms/v1/companyTransform');
const {seeCompanyGalleryTransform} = require('../../transforms/v1/companyGalleryTransform');


class companyController extends controller {

    async createCompany(req, res){

        // if(! await this.validationData(req, res)) return;


        const name = req.body.name.trim() || null;
        const description = req.body.description.trim() || null;
        const category = req.body.category.trim() || null;
        const state = req.body.state.trim() || null;
        const city = req.body.city.trim() || null; 

        if( !name || !description || !category) {
            return res.status(404).json({
                code: '433',
                message: [
                    "همه فیلدها وارد شود"
                ],
                status: "error"
            })
        }

        // check if user type is not company return 
        const isOwnerCompany = await isOwnerTypeCompany(req.user.id)
        if(!isOwnerCompany) {
            return res.status(403).json({
                code: '403',
                message: 'یوزر باید از نوع شرکت تعریف شود',
                status: 'error'
            })
        }


        let createCP;
        if(!req.files || Object.keys(req.files).length === 0) {
            createCP = await createCompanyService(req.user.id, name, description, category, state, city)
        } else {
            let logo = req.files.logo;
            // logo.name = logo.name.replaceAll(" ", "");
            logo.name = logo.name.replace(/\s/g, '')
            const uploadPath = `./uploads/images/company/logo/` + new Date().toISOString() +  '-' + logo.name;
            const upload = await logo.mv(uploadPath, function(err) {
                if (err) {
                    return res.status(500).json({
                        methode: 'createCompany',
                        code : '500',
                        status: 'error',
                        message : 'unable to upload logo',
                        data:{}
                    })
                }
            });
            const filePath = uploadPath.replace("./", "")
            createCP = await createCompanyService(req.user.id, name, description, category, state, city, filePath)
        }

        

        if(!createCP.success) {
            console.log('adf')
            return  res.json(createCP);
        }

        const result = await createCompanyTransform(createCP.description)
        return  res.json(result);
    }

    async deleteCompany(req, res){

        if(! await this.validationData(req, res)) return;

        const {id} = req.body;

        const deleted = await deleteCompanyService(id);

        if(!deleted.success) return  res.json(deleted);

        return  res.json(deleted.success);
    }

    async updateCompany(req, res){

        if(! await this.validationData(req, res)) return;

        const {id, description, name, category} = req.body;

        const updated = await updateCompanyService(id, description, name, category)

        if(!updated.success) return updated

        return  res.json(updated.success);
    }

    async seeCompanies(req, res){

        const companies = await getAllCompaniesService();

        const result = await getCompaniesTransform(companies)

        return  res.json(companies);
    }

    async seeCompany(req, res) {

        const {name} = req.body;

        const companies = await getCompany(name);

        console.log(companies)

        const result = await getCompaniesTransform(companies)

        return  res.json(result);
    }

    async seeCompanyGallery(req, res) {
        if(! await this.validationData(req, res)) return; 

        const { company } = req.body;

        const listOfGalleries = await seeCompanyGalleryService(company)

        const result = await seeCompanyGalleryTransform(listOfGalleries)

        return  res.json(result);
    }

    async updateCompanyGallery(req, res) {
        return  res.json('update company gallery');
    }

    async deleteCompanyGallery(req, res) {
        if(! await this.validationData(req, res)) return;

        const {image} = req.body;

        const deleted = await deleteCompanyGalleryService(image)
        return  res.json(deleted.success);
    }

    async createCompanyGallery(req, res) {

        if(!req.body.companyid) {
            return  res.json('insert companyid');
        }
        const {companyid} = req.body;

        const company = await iscompanyExists(companyid)
        if(!company) {
            return res.status(404).json({
                methode: 'createCompanyGallery',
                code : '404',
                status: 'error',
                message : 'company not found',
            })
        }

        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(404).json({
                methode: 'createCompanyGallery',
                code : '404',
                status: 'error',
                message : 'you should upload image gallery',
                data:{}
            })
        }

        // let image = req.files.image;
        
        let file = req.files.image;
        for ( let f of file ) {
            // f.name = f.name.replaceAll(" ", "");
            f.name = f.name.replace(/\s/g, '')
        }
        
        let uploadPath = [];

        for(let i in file){

            let path = './uploads/images/company/gallery/'+ new Date().toISOString() +  '-' + file[i].name;
            uploadPath.push(path)
            file[i].mv(path, function (err){
                if(err){
                    return res.status(500).json({
                        methode: 'createCompanyGallery',
                        code : '500',
                        status: 'error',
                        message : 'unable to save file',
                        data:{}
                    })
                }

            })

        }



        // return res.json(uploadPath)
        for (let p in uploadPath) { 
            uploadPath[p] = uploadPath[p].replace("./", "");
        }
        // const savedGallery = await createCompanyGalleryService(uploadPath, companyid)
        const savedGallery = await saveGallery(uploadPath, companyid)


        return  res.status(200).json({
            status: 200,
            success: true,
            message: savedGallery
        });
    }
}

module.exports = new companyController(); 