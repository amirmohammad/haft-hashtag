const controller = require('../../index');
const ObjectId = require('mongoose').Types.ObjectId;

const {createChannelSVC, getChannelsSVC, getChannelSVC, updateChannelSVC, deleteChannelSVC, exclusivesSVC} = require('../../../services/tv/tvChannelServices');
const { updateChannel, deleteChannel } = require('../../../validators/tvValidator');

class tvController extends controller { 

    async getChannels(req, res, next) {
        const channels = await getChannelsSVC();
        return  res.json(channels);
    }

    async createChannel(req, res, next) {

        if(! req.body.channel) {
            return  res.json({
                success: false,
                status: 404,
                description: 'enter channel name'
            })
        }

        // if(! await this.validationData(req, res)) return;

        if(!req.file){
            return res.status(404).json({
                methode: 'updateArticle',
                code : '404',
                status: 'error',
                message : 'you should upload image',
                data:{}
            })
        }

        const {channel, free, channelType} = req.body;
        const created = await createChannelSVC(channel, free, channelType, req.file.path)
        return  res.json(created);
    }

    async getChannel(req, res, next) {
        if(! await this.validationData(req, res)) return;

        const channel = await getChannelSVC(req.body.channel)

        return  res.json(channel);
    }

    async updateChannel(req, res, next) {
        // if(! await this.validationData(req, res)) return;

        if(!req.body.channel || !req.body.newChannel) {
            return  res.json({
                success: false,
                status: 404,
                description: 'enter channel new and old name'
            })
        }

        if(!ObjectId(req.body.channel) ){
            return  res.json({
                success: false,
                status: 400,
                description: 'old channel id is incorrect'
            })
        }

        if(!req.file){
            return res.status(404).json({
                methode: 'updateArticle',
                code : '404',
                status: 'error',
                message : 'you should upload image',
                data:{}
            })
        }

        const { channel, newChannel } = req.body;

        const updated = await updateChannelSVC(channel, newChannel, req.file.path)

        return  res.json(updated);
    }

    async deleteChannel(req, res, next) {
        if(! await this.validationData(req, res)) return;

        const deleted = await deleteChannelSVC(req.body.channel)

        return res.status(200).json(deleted);
    }

    async exclusives(req, res) {
        const channels = await exclusivesSVC();
        return res.json(channels);
    }
}

module.exports = new tvController(); 