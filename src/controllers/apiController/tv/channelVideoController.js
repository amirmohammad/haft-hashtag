const controller = require('../../index');

// const {createChannelSVC, getChannelsSVC, getChannelSVC, updateChannelSVC, deleteChannelSVC} = require('../../../services/tv/tvChannelServices');
const { getVideosOfChannelSVC, getVideoSVC, uploadVideoSVC, deleteVideoSVC, bannerVideo } = require('../../../services/tv/videoChannelService')

class tvController extends controller { 

    async uploadVideo(req, res) {
        if(!req.file){
            return res.status(404).json({
                methode: 'upload video channel',
                code : '404',
                status: 'error',
                message : 'you should upload video',
                data:{}
            })
        }

        const uploaded = await uploadVideoSVC(req.body.channel, req.file.path)

        return  res.json(uploaded);
    }

    async getVideo(req, res) {
        if(! await this.validationData(req, res)) return;

        const got = await getVideoSVC(req.body.video)

        return  res.json(got);
    }

    async deleteVideo(req, res) {
        if(! await this.validationData(req, res)) return;

        const deleted = await deleteVideoSVC(req.body.video)

        return  res.json(deleted);
    }

    async allVideos(req, res) {
        if(! await this.validationData(req, res)) return;

        const allVids = await getVideosOfChannelSVC(req.body.channel);
        return  res.json(allVids);
    }

    async bannerVideo(req, res) {
        if(! await this.validationData(req, res)) return;
        
        const video = await bannerVideo();
        return res.json(video)
    }
}

module.exports = new tvController(); 