
const controller = require('../index')
const moment = require('moment-jalaali')


// service
const { uploadPeopleService, getPeopleService, createPlanSVC, seePlanSVC, updatePlanSVC, deletePlanSVC, buyPlanSVC, boughtPlan, applyForJobSVC, boughtPlans } = require('../../services/eachIranianServices')
//  transform
const eachIRTransform = require('../../transforms/v1/eachIranianTransform');
const { create } = require('../../models/user');

class eachIranianController extends controller {

    async jobs(req, res, next) {
        if(! await this.validationData(req, res)) return;
        
        const {name, city, state, plan, jobTitle, description} = req.body;
        // return res.json(plan)

        const applied = await applyForJobSVC(req.user._id, name, city, state, plan, jobTitle, description)

        if(!applied.success) return  res.json(applied);

        const result = await eachIRTransform.applyForJobTransform(applied.description)

        return  res.json(result);

    }

    async people(req, res) {

        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(404).json({
                methode: 'updloadProfileImage',
                code : '404',
                status: 'error',
                message : 'you should upload image',
                data:{}
            })
        }

        let image = req.files.image;
        // image.name = image.name.replaceAll(" ", "");
        image.name = image.name.replace(/\s/g, '');
        const uploadPath = `./uploads/images/profile/` + new Date().toISOString() +  '-' + image.name;

        const upload = await image.mv(uploadPath, function(err) {
            if (err) {
                return res.status(500).json({
                    methode: 'updloadProfileImage',
                    code : '500',
                    status: 'error',
                    message : 'unable to upload image',
                    data:{}
                })
            }
        });

        const fullname = req.body.fullname || null;
        const type = req.body.type || null;
        const birthdate = req.body.birthdate || null;
        const description = req.body.description || null;
        const gender = req.body.gender || null;
        const personalID = req.body.personalID || null;
        const filePath = uploadPath.replace("./", "")

        const saved = await uploadPeopleService(req.user._id, fullname, type, birthdate, gender, personalID, description, filePath )

        if(!saved.success) return  res.json(saved);

        const result = eachIRTransform.eachIranianCreatePeople(saved.description)
        // console.log(req.body)
        return  res.json(result);
    }

    async getPople(req, res) {
        if(! await this.validationData(req, res)) return; 

        const {people} = req.body;

        const got = await getPeopleService(people)

        return  res.json(got);
    }

    async createPlans(req, res){
        if(! await this.validationData(req, res)) return; 

        const {days, name, fee} = req.body;

        const created = await createPlanSVC(name, days, fee)

        if(!create.success) return  res.json(created);

        const result = await eachIRTransform.savePlanTransform(created.description)

        return  res.json(result);
    }

    async updatePlans(req, res){
        if(! await this.validationData(req, res)) return; 

        const { id, name, days, fee } = req.body;

        const updated = await updatePlanSVC(id, name, days, fee);

        return  res.json(updated);
    }

    async deletePlans(req, res){

        if(! await this.validationData(req, res)) return; 

        const deleted = await deletePlanSVC(req.body.id)

        return  res.json(deleted);
    }

    async seePlans(req, res){
        
        const plans = await seePlanSVC();

        return  res.json(plans);
    }

    async buyPlan(req, res){
        if(! await this.validationData(req, res)) return; 

        const bought = await buyPlanSVC(req.body.plan, req.user._id)
        if(!bought.success) return bought
        const result = await eachIRTransform.boughtPlanTransform(bought.description)

        return  res.json(result);  
    }

    async boughtPlan(req, res) {

        const boughts = await boughtPlan(req.user._id)

        return res.json(boughts);
    }

    async acitvePlan(req, res) {
        
        const userBoughtPlans = await boughtPlans(req.user.id);

        let activeplan = {
            free: true,
        };
        
        let now = new Date();
        for (let plan of userBoughtPlans) {

            let start = new Date(moment(plan.start, 'jYYYY/jMM/jDD HH:mm'))  
            let end = new Date(moment(plan.end, 'jYYYY/jMM/jDD HH:mm'))
            
            if( (start.getTime() < now.getTime()) && ( now.getTime() < end.getTime()) ) {
                activeplan = plan;
            }

        }

        return res.json(activeplan)
    }

}

module.exports = new eachIranianController(); 