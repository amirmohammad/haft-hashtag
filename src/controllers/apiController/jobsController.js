const controller = require('../index')

class jobController extends controller {
    
    async apply(req, res, next) {
        return  res.json('apply for job');
    }

    async allJobs(req, res, next) {
        return  res.json('all jobs');
    }
}

module.exports = new jobController();