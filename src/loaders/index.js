// import expressLoader from './express';
// import dependencyInjectorLoader from './dependencyInjector';
// import mongooseLoader from './mongoose';
// import jobsLoader from './jobs';
// import Logger from './logger';
// //We have to import at least all the events once so they can be triggered
// import './events';
const mongooseLoader = require('./mongoose')
const Logger = require('./logger');
const { mongo } = require('mongoose');
const winston = require('winston')
const expressLoader = require('./express')

module.exports = async ({ expressApp }) => {
  
    const mongooseConnection = await mongooseLoader();
    console.info('db loaded and connected')

    await expressLoader({ app: expressApp });
    console.info('✌ Express loaded');


};