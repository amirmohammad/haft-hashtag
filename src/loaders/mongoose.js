const mongoose = require('mongoose')
const config = require('../config')

module.exports = async () => {
    // console.log(config.database.databaseURL)
    const connection = await mongoose.connect(config.database.databaseURL, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true });
    return connection.connection.db;
}