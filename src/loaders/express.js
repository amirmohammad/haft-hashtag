const express = require('express');
const bodyParser = require('body-parser')
const cors = require('cors')
const routes = require('../routes')
const config =  require('../config')
const apiRoute = require('../routes/api')
const webRoute = require('../routes/web')
const Handlebars = require('handlebars')
// const handlebars = require('express-handlebars')
const expressHandlebars = require('express-handlebars')
const {allowInsecurePrototypeAccess} = require('@handlebars/allow-prototype-access')
// const insecureHandlebars = allowInsecurePrototypeAccess(Handlebars)
const flash = require('connect-flash');
const path = require('path')
const rememberLogin = require('../middlewares/rememberLogin');
const Helpers = require('../helpers')
const cookieParser = require('cookie-parser');
const session = require('express-session');
const passport = require('passport');
const fileUpload = require('express-fileupload');


// module.exports = ({ app }: { app: express.Application }) => {
module.exports = ({ app }) => {
  /**
   * Health Check endpoints
   * @TODO Explain why they are here
   */
  app.get('/status', (req, res) => {
    res.status(200).end();
  });
  app.head('/status', (req, res) => {
    res.status(200).end();
  });

  
  // set view engine
  app.use(require('method-override')());

  app.set('view engine', 'handlebars');

  app.engine('handlebars', expressHandlebars({
    handlebars: allowInsecurePrototypeAccess(Handlebars),
    layoutsDir: path.join(__dirname, '../resource/views/layouts'),
    defaultLayout: 'index',
  }))

  app.enable('trust proxy');
  app.use(cors());
  
  app.use(express.static(path.join(__dirname, '../public')));
  app.set('views',path.join(__dirname, '../resource/views'));
  app.use('/uploads',express.static('uploads'));
  app.use('/static', express.static('static'))

  
  app.use(cookieParser(config.cookie_secretkey));
  app.use(session({...config.session}));
  app.use(flash());

  require('../passport/passport-local');




  // Some sauce that always add since 2014
  // "Lets you use HTTP verbs such as PUT or DELETE in places where the client doesn't support it."
  // Maybe not needed anymore ?

  // Middleware that transforms the raw string of req.body into json
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: true}));



 // passport config
  
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(rememberLogin.handle);

  app.use((req , res , next) => {
    app.locals = new Helpers(req, res).getObjects();
    next();
  });


    
  // Load API routes
  app.use('/331421.txt', (req, res) => {
    const addr = path.join(__dirname, '../../331421.txt')
    res.sendFile(addr,  function (err) { 
        if (err) { 
            // next(err); 
        } else { 
            console.log('Sent:', 'fileName'); 
        } 
    }); 
  })
  app.use(config.app.webRoute, webRoute);
  // express file upload 
  // upload file middleware should not be moved because it not supported for web routes, but for api routes
  app.use(fileUpload());
  app.use(config.app.apiRoute, apiRoute);


  /// catch 404 and forward to error handler
  app.use((req, res, next) => {
    const err = new Error('Not Found');
    err['status'] = 404;
    next(err);
  });

  /// error handlers
  app.use((err, req, res, next) => {
    /**
     * Handle 401 thrown by express-jwt library
     */
    if (err.name === 'UnauthorizedError') {
      return res
        .status(err.status)
        .send({ message: err.message })
        .end();
    }
    return next(err);
  });
  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
      errors: {
        message: err.message,
      },
    });
  });
};