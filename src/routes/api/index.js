const express = require('express');
const router = express.Router();

const {uploadImage} = require('../../middlewares/uploadMiddlewares/image/test')
const apiAuth = require('../../middlewares/apiAuth')

const apiV1 = require('./v1');

router.get('/', (req, res, next) => res.json('api main route, set version'));

//
router.use('/v1', apiV1);

router.post("/test", uploadImage.single("image"), (req, res) => {
    if(!req.file) {
        return res.json('file not found')
    }
    console.log(" file disk uploaded");
    res.send("file disk upload success");
});

router.post('/test2', apiAuth, function(req, res) {
    let sampleFile;
    let uploadPath;
  
    if (!req.files || Object.keys(req.files).length === 0) {
      res.status(400).send('No files were uploaded.');
      return;
    }
  
    sampleFile = req.files.sampleFile;
  
    uploadPath = "./uploads/images/test/" + sampleFile.name;
  
    sampleFile.mv(uploadPath, function(err) {
      if (err) {
        return res.status(500).send(err);
      }
       
      res.json({uploadPath, body: req.body});
    });
  });

module.exports = router;
