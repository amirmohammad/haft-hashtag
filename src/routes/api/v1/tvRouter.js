const express = require('express');
const router = express.Router();

// controller
const tvController = require('../../../controllers/apiController/tv/tvController')
const channelController = require('../../../controllers/apiController/tv/channelVideoController')

// validators
const tvValidator = require('../../../validators/tvValidator')

// middlewares
const apiAuth = require('../../../middlewares/apiAuth')
const adminAuth = require('../../../middlewares/adminAuth')
const {uploadVideo} = require('../../../middlewares/uploadMiddlewares/video/tvChannelUploadVideo')
const {uploadImage} = require('../../../middlewares/uploadMiddlewares/image/channelImageUpload')


router.get('/', (req, res, next) =>  res.json('tv routes'));

router.get('/channels', apiAuth, tvController.getChannels);
router.post('/channel', apiAuth, tvValidator.getChannel(), tvController.getChannel);
router.post('/channel', apiAuth, adminAuth.handle, uploadImage.single('image'), tvController.createChannel)
router.put('/channel', apiAuth, adminAuth.handle, uploadImage.single('image'), tvController.updateChannel)
router.delete('/channel', apiAuth, adminAuth.handle, tvValidator.deleteChannel(), tvController.deleteChannel)
router.get('/exclusiveChannels', apiAuth, tvController.exclusives)

router.post('/channelVideo', apiAuth, tvValidator.getVideo(), channelController.getVideo )
router.post('/channelVideo', apiAuth, adminAuth.handle, uploadVideo.single('video'), channelController.uploadVideo )
router.delete('/channelVideo', apiAuth, adminAuth.handle, tvValidator.deleteVideo(), channelController.deleteVideo )
router.get('/channelVideos', apiAuth, tvValidator.allVideos(), channelController.allVideos )

router.get('/banner', apiAuth, channelController.bannerVideo)




module.exports = router; 