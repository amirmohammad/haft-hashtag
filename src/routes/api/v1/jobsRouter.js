const express = require('express');
const router = express.Router();

const jobController = require('../../../controllers/apiController/jobsController')

router.get('/', (req, res, next) =>  res.json('jobs main route'))

router.get('/apply', jobController.apply)
router.get('/allJobs', jobController.allJobs)

module.exports = router; 