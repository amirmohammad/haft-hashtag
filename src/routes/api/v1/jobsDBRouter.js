const express = require('express');
const router = express.Router();

const jobsDBController = require('../../../controllers/apiController/jobsDBController');
const apiAuth = require('../../../middlewares/apiAuth');
const adminAuth = require('../../../middlewares/adminAuth');
const companyController = require('../../../controllers/apiController/companyController')
const companyCertificateController = require('../../../controllers/apiController/companyCertificateController')

// validators
const companyCategory = require('../../../validators/companyCategoryValidators');
const companyValidator = require('../../../validators/companyValidator');
const companyGalleryValidator = require('../../../validators/companyGalleryValidator')
const companyCertificateValidator = require('../../../validators/companyCertificateValidator');

// const { uploadImage } = require('../../../services/uploadImage')
const { uploadCompanyImage } = require('../../../middlewares/uploadMiddlewares/image/companyGalleryUpload')
const { uploadCompanyCertificate } = require('../../../middlewares/uploadMiddlewares/image/companyCertificateUpload')
const { uploadCompanyLogo } = require('../../../middlewares/uploadMiddlewares/image/companyLogo')





router.get('/', (req, res) =>  res.json('jobsdb router'))

router.get('/list', jobsDBController.list)
router.get('/description', jobsDBController.description)
router.get('/imageGallery', jobsDBController.imageGallery)
router.get('/certificates', jobsDBController.certificates)

router.get('/category', jobsDBController.seeCategory)
router.post('/category', apiAuth, adminAuth.handle, companyCategory.createCategoryValidator(), jobsDBController.createCategory)
router.put('/category', apiAuth, adminAuth.handle, companyCategory.updateCategoryName(), jobsDBController.updateCategory)
router.delete('/category', apiAuth, adminAuth.handle, companyCategory.deleteCategoryValidator(),jobsDBController.deleteCategory);

router.get('/company', companyValidator.seeCompanyValidator() ,companyController.seeCompany)
// router.post('/company', apiAuth, uploadCompanyLogo.single('logo'), companyController.createCompany) 
router.post('/company', apiAuth, companyController.createCompany) 
router.put('/company', apiAuth, adminAuth.handle, companyValidator.updateCompanyValidator(), companyController.updateCompany)
router.delete('/company', apiAuth, adminAuth.handle, companyValidator.deleteCompanyValidator(), companyController.deleteCompany)
router.get('/companies', companyController.seeCompanies)

router.post('/seeCompanyGallery', companyGalleryValidator.seeCompanyGalleryValidator() , companyController.seeCompanyGallery)
// router.post('/companyGallery', apiAuth, adminAuth.handle, uploadCompanyImage.array('image', 10),companyController.createCompanyGallery)
router.post('/companyGallery', apiAuth, companyController.createCompanyGallery)
router.put('/companyGallery', apiAuth, adminAuth.handle, companyController.updateCompanyGallery)
router.delete('/companyGallery', apiAuth, adminAuth.handle, companyGalleryValidator.deleteCompanyGalleryValidator(), companyController.deleteCompanyGallery)

router.get('/companyCertificate', companyCertificateValidator.seeCertificateValidator(), companyCertificateController.seeCertificate)
// router.post('/companyCertificate', apiAuth, uploadCompanyCertificate.array('image', 10), companyCertificateController.uploadCertificate)
router.post('/companyCertificate', apiAuth, companyCertificateController.uploadCertificate)

router.put('/companyCertificate', companyCertificateController.updateCertificate)
router.delete('/companyCertificate', apiAuth, adminAuth.handle, companyCertificateValidator.deleteCertificateValidator() , companyCertificateController.deleteCertificate)



module.exports = router; 