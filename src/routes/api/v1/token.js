const express = require('express');
const router = express.Router();

// controllers
const smsTokenController = require('../../../controllers/apiController/tokenAuthentication');

// validators
const nubmerValidator = require('../../../validators/phonenumber-validator')

router.post('/sendSMSToken', nubmerValidator.checkFormat(), smsTokenController.sendSMSToken)
router.post('/verifySMSToken', nubmerValidator.verifyToken(), smsTokenController.verifySMSToken);

module.exports = router;