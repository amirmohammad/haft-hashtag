
const express = require('express');
const router = express.Router();

const eachIranianController = require('../../../controllers/apiController/eachIranianController');

// const {uploadImage} = require('../../../services/uploadImage')
const {uploadImage} = require('../../../middlewares/uploadMiddlewares/image/eachIranianUploadImage')

// middlewares
const apiAuth = require('../../../middlewares/apiAuth')
const adminAuth = require('../../../middlewares/adminAuth')

// validator
const eachIranianValidator = require('../../../validators/eachIranianValidator')

router.get('/', (req, res) => res.json('each iranian router'))
router.post('/jobs', apiAuth, eachIranianValidator.applyForJobValidator(), eachIranianController.jobs);

// router.post('/people', apiAuth, uploadImage.single('image'), eachIranianController.people)
router.post('/people', apiAuth, eachIranianController.people)

router.get('/people', apiAuth, eachIranianValidator.getpeopleValidator(), eachIranianController.getPople)

// plans
router.post('/plan', apiAuth, adminAuth.handle, eachIranianValidator.createPlanValidator(), eachIranianController.createPlans)
router.get('/plan', apiAuth, eachIranianController.seePlans)
router.put('/plan', apiAuth, adminAuth.handle, eachIranianValidator.updatePlanValidator(), eachIranianController.updatePlans)
router.delete('/plan', apiAuth, adminAuth.handle, eachIranianValidator.deletePlanValidator(), eachIranianController.deletePlans)

router.post('/buyPlan', apiAuth, eachIranianValidator.buyPlanValidator(), eachIranianController.buyPlan)
router.get('/boughtPlans', apiAuth, eachIranianController.boughtPlan)

router.get('/active-plan', apiAuth, eachIranianController.acitvePlan)



module.exports = router;