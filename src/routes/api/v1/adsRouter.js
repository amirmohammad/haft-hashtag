const express = require('express');
const router = express.Router()

// contorller
const adsController = require('../../../controllers/apiController/adsController')

// middlewares
const apiAuth = require('../../../middlewares/apiAuth')
const adminAuth = require('../../../middlewares/adminAuth')


router.get('/', (req, res) =>  res.json('ads main route'))

router.get('/getAds', apiAuth, adsController.getAds)

// router.post('/uploadAds', apiAuth, adminAuth.handle, uploadImage.single('image'), adsController.uploadAds);
router.post('/uploadAds', apiAuth, adminAuth.handle, adsController.uploadAds);

// router.post('/adsWallpaper', apiAuth, adminAuth.handle, uploadImage.single('image'), adsController.wallpaper)

module.exports = router