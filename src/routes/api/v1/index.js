
const express = require('express');
const router = express.Router();

// controllers
const routeController = require ('../../../controllers/apiController/routesController');


// routers
const tokenRouters = require('./token');
const tvRouter = require('./tvRouter');
const jobsRouter = require('./jobsRouter');
const adsRouter = require('./adsRouter');
const eachIranianRouter = require('./eachIranianRouter')
const iranianTalentsRouter = require('./iranianTalentsRouter');
const maheKhandanRouter = require('./maheKhandanRouter');
const jobsDBRouter = require('./jobsDBRouter');
const profileRouter = require('./profileRouter')
const authRouter = require('./authRouter');
const bankIdeaRouter = require('./bankIdeaRouter');

router.get('/', (req, res) => res.json('api v1 router'));

router.use('/token', tokenRouters)
router.use('/tv', tvRouter)
router.use('/jobs', jobsRouter)
router.use('/ads', adsRouter);
router.use('/eachIranian', eachIranianRouter)
router.use('/iranianTalents', iranianTalentsRouter)
router.use('/maheKhandan', maheKhandanRouter)
router.use('/jobsdb', jobsDBRouter)
router.use('/profile', profileRouter)
router.use('/auth', authRouter)
router.use('/bankIdea', bankIdeaRouter)

// pages
router.get('/home', routeController.homepage)
router.get('/splashScreen', routeController.splashScreen);
router.get('/guides', routeController.guide)





module.exports = router;