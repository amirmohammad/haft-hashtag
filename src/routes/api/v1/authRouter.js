const express = require('express');
const router = express.Router(); 

// controllers
const authController = require('../../.../../../controllers/apiController/authController')

// services
// const { uploadImage } = require('../../../services/uploadImage')
const { uploadImage } = require('../../../middlewares/uploadMiddlewares/image/profileImageUpload')

const apiAuth = require('../../../middlewares/apiAuth')

// validators
const authValidator = require('../../../validators/authValidator')

router.get('/', (req, res) => res.json('auth main route'))
router.post('/login', authValidator.login(), authController.login)
router.post('/signup', authValidator.register(), authController.signup)
router.put('/edit', apiAuth, authValidator.edit(), authController.edit)

// router.post('/profile', apiAuth, uploadImage.single('image'), authController.updloadProfileImage)
router.post('/profile', apiAuth, authController.updloadProfileImage)

router.get('/getuserinfo', apiAuth, authController.getUserInfo)


module.exports = router;