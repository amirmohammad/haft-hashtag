const express = require('express');
const router = express.Router();

const maheKhandanController = require('../../../controllers/apiController/maheKhandanController')

const {uploadVideo} = require('../../../middlewares/uploadMiddlewares/video/maheKhandanUpload');

// validators
const maheKhandanValidator = require('../../../validators/maheKhandanValidator')

// middlewares
const apiAuth = require('../../../middlewares/apiAuth')
const adminAuth = require('../../../middlewares/adminAuth')

router.get('/', (req, res, next) =>  res.json('mahe khandan main route'))

router.get('/category', maheKhandanController.category)
router.get('/charity', maheKhandanController.charity)

router.post('/get-video', apiAuth, maheKhandanValidator.getVideo(), maheKhandanController.getVideo)
// router.post('/video', apiAuth, uploadVideo.single('video'), maheKhandanController.uploadVideo)
router.post('/video', apiAuth, maheKhandanController.uploadVideo)
router.delete('/video', apiAuth, adminAuth.handle, maheKhandanValidator.getVideo(), maheKhandanController.deleteVideo)
router.get('/videos', apiAuth, maheKhandanController.getAllVideos)

router.post('/comment', apiAuth, maheKhandanValidator.sendCommentValidator(), maheKhandanController.sendComment)
router.put('/comment', apiAuth, maheKhandanValidator.updateCommentValidator(), maheKhandanController.updateComment)
router.delete('/comment', apiAuth, maheKhandanValidator.deleteCommentValidator(), maheKhandanController.deleteComment)
// router.get('/comment', apiAuth, maheKhandanValidator.getCommentValidator(), maheKhandanController.getComment)

// mahekhandan video reactions
router.post('/reactions', apiAuth, maheKhandanValidator.allReactionsValidator(), maheKhandanController.allReactions)
router.post('/reaction', apiAuth, maheKhandanValidator.sendReactionValidator(), maheKhandanController.sendReaction)

module.exports = router; 