
const express = require('express');
const router = express.Router();

// controller
const bankIdeaController = require('../../../controllers/apiController/bankIdeaController');

// validators
const bankIdeaCategoryValidator = require('../../../validators/bankIdeaCategoryValidator')
const ideaValidator = require('../../../validators/ideaValidator')

// middlewares
const apiAuth = require('../../../middlewares/apiAuth')
const adminAuth = require('../../../middlewares/adminAuth')

router.get('/', (req, res) => res.json('bank idea router'))

router.get('/category', bankIdeaController.seeCategory)
router.post('/category', apiAuth, adminAuth.handle, bankIdeaCategoryValidator.createCategoryValidator(), bankIdeaController.createCategory)
router.put('/category', apiAuth, adminAuth.handle, bankIdeaCategoryValidator.updateCategoryName(), bankIdeaController.updateCategory)
router.delete('/category', apiAuth, adminAuth.handle, bankIdeaCategoryValidator.deleteCategoryValidator(), bankIdeaController.deleteCategory)

router.post('/idea', apiAuth, ideaValidator.sendIdeaValidator(), bankIdeaController.uploadIdea)
router.get('/idea', apiAuth, adminAuth.handle, ideaValidator.getIdeaValidator(), bankIdeaController.getIdeas)


module.exports = router;