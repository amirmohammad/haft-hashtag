const express = require('express');
const { profile } = require('winston');
const router = express.Router();

const profileController = require('../../../controllers/apiController/profileContorller');
const companyController = require('../../../controllers/apiController/companyController');

// middlewares
const apiAuth = require('../../../middlewares/apiAuth')
const adminAuth = require('../../../middlewares/adminAuth')


// services
// const {uploadImage} = require('../../../services/uploadImage') 
const {uploadImage} = require('../../../middlewares/uploadMiddlewares/image/profileGalleryUpload') 


router.get('/', (req, res) =>  res.json('profile router'))
router.get('/myData', profileController.myData)

router.get('/seeCompanyInfo', profileController.seeCompanyInfo)
router.put('/changeCompanyInfo', profileController.changeCompanyInfo)
router.post('/createCompany', apiAuth, adminAuth.handle, profileController.createCompany)

router.get('/seeGallery', apiAuth, profileController.seeGallery)
// router.post('/uploadGallery', apiAuth, uploadImage.single('image'), profileController.uploadGallery)
router.post('/uploadGallery', apiAuth, profileController.uploadGallery)
router.delete('/deleteGallery', apiAuth, profileController.deleteGallery)

router.get('/seeCertificate', apiAuth, profileController.seeCertificate)
router.post('/uploadCertificate', apiAuth, profileController.uploadCertificate)
router.delete('/deleteCertificate', apiAuth, profileController.deleteCertificate)




module.exports = router; 