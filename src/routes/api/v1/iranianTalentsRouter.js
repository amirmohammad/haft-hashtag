const express = require('express');
const router = express.Router();

const iranianTalentsCategoryController = require('../../../controllers/apiController/iranianTalents/iranianTalentsCategoryController');
const iranianTalentVideoController = require('../../../controllers/apiController/iranianTalents/iranianTalentsVideoController')
const iranianTalentCommentController = require('../../../controllers/apiController/iranianTalents/iranianTalentsCommentController')
const iranianTalentPointController = require('../../../controllers/apiController/iranianTalents/iranianTalentPointsController')


// middlewares
const apiAuth = require('../../../middlewares/apiAuth')
const adminAuth = require('../../../middlewares/adminAuth')
const { uploadVideo } = require('../../../middlewares/uploadMiddlewares/video/IranianTalentsUpload');

// validators
const iranianTalentsValidator = require('../../../validators/iranianTalentsValidator')

router.get('/', (req, res) =>  res.json('iranian talents route'))

router.get('/categories', apiAuth, iranianTalentsCategoryController.getCategories)
router.post('/category', apiAuth, adminAuth.handle, iranianTalentsValidator.createCategoryValidator(), iranianTalentsCategoryController.createCategory)
router.put('/category', apiAuth, adminAuth.handle, iranianTalentsValidator.updateCategoryValidator(), iranianTalentsCategoryController.updateCategory)
router.delete('/category', apiAuth, adminAuth.handle, iranianTalentsValidator.deleteCategoryValidator(), iranianTalentsCategoryController.deleteCategory)
router.get('/category', apiAuth, iranianTalentsValidator.getVideoCategoryValidator(), iranianTalentsCategoryController.getCategoryVideos) //to do later

router.get('/video', apiAuth,iranianTalentsValidator.getVideoValidator(), iranianTalentVideoController.getVideo)
// router.post('/video', apiAuth, iranianTalentsValidator.uploadVideoValidator(), uploadVideo.single('video'), iranianTalentVideoController.sendVideo)
router.post('/video', apiAuth, iranianTalentsValidator.uploadVideoValidator(), iranianTalentVideoController.sendVideo)
router.delete('/video', apiAuth, adminAuth.handle, iranianTalentsValidator.deleteVideoValidator(), iranianTalentVideoController.deleteVideo)

router.post('/comment', apiAuth, iranianTalentsValidator.sendCommentValidator(), iranianTalentCommentController.sendComment)
router.put('/comment', apiAuth, iranianTalentsValidator.updateCommentValidator(), iranianTalentCommentController.updateComment)
router.delete('/comment', apiAuth, iranianTalentsValidator.deleteCommentValidator(), iranianTalentCommentController.deleteComment)
router.get('/comments', apiAuth, iranianTalentsValidator.getCommentValidator(), iranianTalentCommentController.getComments)


router.post('/point', apiAuth, iranianTalentsValidator.sendPointValidator(), iranianTalentPointController.sendPoint)
router.get('/point', apiAuth, iranianTalentsValidator.getPointValidator(), iranianTalentPointController.getPoint)
router.get('/pointsForVideo', apiAuth, adminAuth.handle, iranianTalentsValidator.getPointsForVideoValidator(), iranianTalentPointController.getPointsForVideo)





module.exports = router; 