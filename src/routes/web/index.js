const express = require('express');
const router = express.Router();
const indexController = require('../../controllers/webController/main')
const redirectIfAuthenticated = require('../../middlewares/redirectIfAuthenticated')
const redirectIfNotAdmin = require('../../middlewares/redirectIfNotAdmin')

router.get('/', indexController.mianRoute);

// router.get('/upload', indexController.uploadForm)

router.use('/auth', require('./v1/auth.routes'));

router.use('/admin', redirectIfNotAdmin.handle, require('./v1/admin.routes'));


module.exports = router; 