const express = require('express');
const router = express.Router();
const passport = require('passport')
const redirectIfAuthenticated = require('../../../middlewares/redirectIfAuthenticated')
const config = require('../../../config')

// validator
const authValidator = require('../../../validators/authValidator')

const Recaptcha = require('express-recaptcha').RecaptchaV3;
// const recaptchaV3 = new Recaptcha(config.service.recaptcha.client_key, config.service.recaptcha.secret_key, {callback:'cb'})
const recaptcha = new Recaptcha(config.service.recaptcha.client_key, config.service.recaptcha.secret_key, {callback: 'cb'})

// controller
const authController = require('../../../controllers/webController/authController')

router.get('/login', 
    redirectIfAuthenticated.handle,
    recaptcha.middleware.renderWith({'hl': 'fa'}),
    authController.showLoginForm
)

router.get('/registerNumber/:phoneNumber', 
    redirectIfAuthenticated.handle,
    authController.showregisterationNumber
)
router.get('/register', 
    redirectIfAuthenticated.handle,
    authController.showRegsitrationForm
)

// router.post('/login', 
//     redirectIfAuthenticated.handle,
//     passport.authenticate('local-login', { failureRedirect: '/auth/login' }),
//     authController.login
// );

router.post('/login', redirectIfAuthenticated.handle, authController.login)

router.post('/registerNumber', redirectIfAuthenticated.handle, authController.registerForNumber)
router.post('/register', redirectIfAuthenticated.handle, authValidator.registerForm(), authController.register)

router.get('/logout' , (req ,res) => {
    // console.log('logout');
    req.logout();
    res.clearCookie('remember_token');
    res.redirect('/');
});

module.exports = router;