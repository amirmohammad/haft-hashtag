const express = require('express')
const router = express.Router();

const uploadController = require('../../../controllers/webController/admin/uploadController')
const adminController = require('../../../controllers/webController/admin/adminController');
const channelController = require('../../../controllers/webController/admin/channelController')
const adsController = require('../../../controllers/webController/admin/adsController')

const channelValidator = require('../../../validators/web/channelValdators')

// middleware
const { uploadImage } = require('../../../middlewares/uploadMiddlewares/image/channelImageUpload');
const { uploadVideo } = require('../../../middlewares/uploadMiddlewares/video/tvChannelUploadVideo')
const adsMiddleware = require('../../../middlewares/uploadMiddlewares/image/adsUploadImage')

const channel = require('../../../models/tv/channel');

router.get('/', adminController.showAdminPanel);

router.get('/upload', uploadController.showUploadForm)
router.get('/statistics', adminController.statisticsForm)
router.get('/videoEdit', adminController.videoEdit)

// channels
router.get('/channel-management', channelController.channelManagement)
router.get('/create-channel/', channelController.createChannel)
router.get('/edit-channel/:id', channelController.editChannel)
router.get('/view-channel/:id', channelValidator.viewChannel(), channelController.viewChannel)
router.get('/delete-channel/:id', channelController.deleteChannel)
router.get('/upload-video/:id', channelController.uploadVideo)
router.get('/view-video/:id', channelController.viewVideo)
router.get('/delete-video/:id', channelController.deleteVideo)
router.post('/update-channel', uploadImage.single('channelImage'), channelController.updateChannel)
// router.post('/update-channel', channelController.updateChannel)
router.post('/create-channel', uploadImage.single('channelImage'), channelController.insertChannel)
router.get('/remove-channel/:id', channelController.removeChannel)
router.post('/upload-video', uploadVideo.single('channelVideo'), channelController.uploadChannelVideo)
router.get('/remove-video/:id', channelController.removeVideo)
router.get('/set-banner/:id', channelValidator.setBanner(), channelController.setBannerVideo)

router.use('/ads', require('./ads.routes'))
router.use('/maheKhandan', require('./maheKhandan'))
router.use('/company', require('./company.routes'))
router.use('/user', require('./user.routes'))
router.use('/plan', require('./plan.routes'))
router.use('/appliedJobs', require('./appliedJobs.routes'))

// ads


// router.post('/upload-ads', adsMiddleware.uploadImage(''), adsController.uploadAds)



module.exports = router; 