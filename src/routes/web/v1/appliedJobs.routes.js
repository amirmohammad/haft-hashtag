
const express = require('express')
const router = express.Router();

const appliedJobsController = require('../../../controllers/webController/admin/appliedJobsController')

router.get('/', appliedJobsController.appliedJobsPage)

module.exports = router; 