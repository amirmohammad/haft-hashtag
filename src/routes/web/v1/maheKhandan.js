const express = require('express')
const router = express.Router();

const maheKhandanController = require('../../../controllers/webController/admin/maheKhandanController')

// middleware
const {uploadVideo} = require('../../../middlewares/uploadMiddlewares/video/maheKhandanUpload');
const { route } = require('./auth.routes');


router.get('/', maheKhandanController.showMahekhandanManagementPage)
router.get('/create-video', maheKhandanController.createVideoPage)
router.get('/view-video/:id', maheKhandanController.viewVideoPage)
router.get('/delete-video/:id', maheKhandanController.deleteVideoPage)
router.get('/remove-video/:id', maheKhandanController.removeVideo)
router.get('/delete-comment/:id', maheKhandanController.deleteComment)

router.post('/upload-video', uploadVideo.single('video'), maheKhandanController.uploadVideo)


module.exports = router;