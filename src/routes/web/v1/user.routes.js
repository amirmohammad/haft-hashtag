const express = require('express')
const router = express.Router();

const userController = require('../../../controllers/webController/admin/userController')
const userValidator = require('../../../validators/web/usersValidator')

router.get('/', userController.usersPage)
router.get('/view-user/:id', userValidator.viewUsersPage(), userController.viewUserPage)
router.get('/delete-user/:id', userValidator.viewUsersPage(), userController.delteUserPage)

router.get('/remove-user/:id', userValidator.viewUsersPage(), userController.removeUser)

module.exports = router;