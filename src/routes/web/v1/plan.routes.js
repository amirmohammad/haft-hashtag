const express = require('express')
const router = express.Router();

const planController = require('../../../controllers/webController/admin/plansController')

// validator
const planValidator = require('../../../validators/web/planValidator')

router.get('/', planController.showPlansPage)
router.get('/create-plan', planController.createPlanPage)
router.get('/edit-plan/:id', planValidator.viewPlan(), planController.editPlanPage)
router.get('/delete-plan/:id', planValidator.viewPlan(), planController.deletePlanPage)

router.post('/create-plan', planValidator.createPlan(), planController.createPlan)
router.post('/edit-plan', planValidator.createPlan(), planController.editPlan)
router.post('/delete-plan', planValidator.deletePlan(), planController.deletePlan)

module.exports = router;