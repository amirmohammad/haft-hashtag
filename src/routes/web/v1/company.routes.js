const express = require('express')
const router = express.Router()

// controller
const companyController = require('../../../controllers/webController/admin/companyController')

// validator
const companyValidator = require('../../../validators/web/companyValidator')

router.get('/', companyController.categoryPage)
router.get('/view-category/:id', companyController.viewCategory)
router.get('/create-category', companyController.createCategoryPage)
router.get('/edit-category/:id', companyController.editCateogryPage)
router.get('/delete-category/:id', companyController.deleteCategoryPage)
router.get('/view-company/:id', companyController.viewJobPage)
router.get('/delete-company/:id', companyController.deleteCompanyPage)


router.post('/create-category', companyValidator.createCategory(), companyController.createCategory)
router.post('/edit-category/:id', companyValidator.createCategory(), companyController.editCategory)
router.get('/remove-category/:id', companyController.deleteCateogry)
router.get('/remove-company/:id', companyController.deleteCompany)
module.exports = router;