const express = require('express')
const router = express.Router()

const adsController = require('../../../controllers/webController/admin/adsController')
const { uploadImage } = require('../../../middlewares/uploadMiddlewares/image/adsUploadImage')

router.get('/', (req,res) => res.json('ads'))

router.get('/ads-management', adsController.adsManagement)
router.get('/create-ads', adsController.createAds)
router.get('/view-ads/:id', adsController.adsInfo)
router.get('/edit-ads/:id', adsController.editAds)
router.get('/delete-ads/:id', adsController.deleteAds)
router.get('/remove-ads/:id', adsController.removeAds)

router.post('/upload-ads', uploadImage.single('adsBanner'), adsController.uploadAds)
module.exports = router;