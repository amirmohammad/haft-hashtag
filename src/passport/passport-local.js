// console.table([1,2,3,4])
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/admin/adminUser')

console.warn('local passport file')

passport.serializeUser(function(user, cb) {
    cb(null, user.id);
  });
  
  passport.deserializeUser(function(id, cb) {
    User.findById(id, function (err, user) {
      if (err) { return cb(err); }
      cb(null, user);
    });
  });
  

// passport.use(new LocalStrategy(async (username, password, done) => {
//     const user = await User.findOne({username});
//     return done(null, user)
// }))

passport.use(
  'local-register',
  new LocalStrategy({
    passReqToCallback: true
    },function(req, username, password, done) {
      // console.log(phonenumber)
      User.findOne({username}, function(err, user) {
          if (err) { return done(err); }
          if (user) {
              const errors = req.flash('errors', 'کاربر قبلا ثبت نام کرده است')
              return done(null, false, { errors });
           }

          const newUser = new User ({username, password})

          newUser.save( (err) => {
            if(err) {
              const errors = req.flash('errors', 'مشکلی در ساخت کاربر به وجود امده لطفا بعدا تلاش کنید')
              return done(err, false, {errors})
            }

            done(null, newUser)
          })
      })

  })
)


passport.use('local-login', new LocalStrategy({
    passReqToCallback: true
  },(req, username, password, done) => {
    User.findOne({username}, (err, user) => {
      if(err) return done(err);

      if(! user || ! user.comparePassword(password)) {
        const errors = req.flash('errors' , 'اطلاعات وارد شده مطابقت ندارد')
        return done(null , false , { errors });
      }

      done(null, user)
    })
  })
)