const validator = require('./validator'); 
const { check, body, sanitizeBody, buildCheckFunction } = require('express-validator');

class companyCategory extends validator{

    createCompanyValidator(){
        return[
            check('name')
                .trim()
                .not().isEmpty()
                .withMessage('نام وارد شود'),
            check('description')
                .trim()
                .not().isEmpty()
                .withMessage('description وارد شود'),

            check('category')
                .trim()
                .not().isEmpty()
                .withMessage('category وارد شود'),
        ]
    }

    seeCompanyValidator(){
        return [
            check('name')
                .trim()
                .not().isEmpty()
                .withMessage('نام  وارد شود'),
        ]
    }

    deleteCompanyValidator(){
        return[
            check('id')
                .trim()
                .not().isEmpty()
                .withMessage('id وارد شود'),
        ]
    }

    updateCompanyValidator(){
        return[
            check('name')
                .trim()
                .not().isEmpty()
                .withMessage('نام وارد شود'),
            check('description')
                .trim()
                .not().isEmpty()
                .withMessage('description وارد شود'),

            check('category')
                .trim()
                .not().isEmpty()
                .withMessage('category وارد شود'),
            check('id')
                .trim()
                .not().isEmpty()
                .withMessage('id وارد شود'),
        ]
    }

    
}

module.exports = new companyCategory()
