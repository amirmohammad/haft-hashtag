const validator = require('./validator'); 
const { check, body, sanitizeBody, buildCheckFunction } = require('express-validator');

class companyImage extends validator{


    seeCompanyGalleryValidator(){
        return [
            check('company')
                .trim()
                .not().isEmpty()
                .withMessage('company  وارد شود'),
        ]
    }

    deleteCompanyGalleryValidator() {
        return [
            check('image')
                .trim()
                .not().isEmpty()
                .withMessage('image  وارد شود'),
        ]
    }

    
}

module.exports = new companyImage()
