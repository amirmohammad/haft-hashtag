const { check, body, sanitizeBody, buildCheckFunction } = require('express-validator');

const validator = require('./validator');

class company extends validator{


    seeCertificateValidator(){
        return [
            check('company')
                .trim()
                .not().isEmpty()
                .withMessage('company  وارد شود'),
        ]
    }


    deleteCertificateValidator(){
        return [
            check('image')
                .trim()
                .not().isEmpty()
                .withMessage('image  وارد شود'),
        ]
    }
    

    
}

module.exports = new company()
