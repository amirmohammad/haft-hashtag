const validator = require('./validator'); 
const { check, body, sanitizeBody, buildCheckFunction } = require('express-validator');

class authValidator extends validator{

    login(){
        return[
            check('phoneNumber')
                .trim()
                .not().isEmpty()
                .withMessage('شماره خالی است')
                .isLength({max: 11, min: 11})
                .withMessage('طول شماره باید 11 رقم باشد')
                .isNumeric()
                .withMessage('فقط عدد وارد کنید'),
        ]
    }

    register(){
        return[
            // check('email')
            //     .trim()
            //     .not().isEmpty()
            //     .withMessage('ایمیل خالی است')
            //     .normalizeEmail()
            //     .isEmail()
            //     .withMessage('فرمت ایمیل صحیح نیست'),
            // sanitizeBody('notifyOnReply').toBoolean(),

            // check('firstname')
            //     .trim()
            //     .not().isEmpty()
            //     .withMessage('نام را وارد کنید')
            //     .not().isNumeric()
            //     .withMessage('فرمت نام صحیح نیست'),
            // sanitizeBody('notifyOnReply').toBoolean(),

            // check('lastname')
            //     .trim()
            //     .not().isEmpty()
            //     .withMessage('نام خانوادگی را وارد کنید')
            //     .not().isNumeric()
            //     .withMessage('فرمت نام خانوادگی صحیح نیست'),
            // sanitizeBody('notifyOnReply').toBoolean(),

            // check('password')
            //     .trim()
            //     .not().isEmpty()
            //     .withMessage('پسورد را وارد کنید'),
            // sanitizeBody('notifyOnReply').toBoolean(),

            check('phoneNumber')
                .trim()
                .isLength({min: 11, max:11})
                .withMessage('طول شماره صحیح نیست')
                .isNumeric()
                .withMessage('فرمت شماره صحیح نیست'),

                // sanitizeBody('notifyOnReply').toBoolean()

            // check('personalCodeID')
            //     .trim()
            //     .not().isEmpty()
            //     .withMessage('کد ملی خالی است')
            //     .isLength({min: 10, max:10})
            //     .withMessage('طول کد ملی باید ۱۰ کاراکتر باشد')
        ]
    }

    edit() {
        return [
            check('name')
                .trim()
                .not().isEmpty()
                .withMessage('نام را وارد کنید')
                .not().isNumeric()
                .withMessage('فرمت نام صحیح نیست'),
            check('personalCodeID')
                .trim()
                .not().isEmpty()
                .withMessage('کد ملی خالی است')
                .isLength({min: 10, max:10})
                .withMessage('طول کد ملی باید ۱۰ کاراکتر باشد'),
            check('gender')
                .trim()
                .not().isEmpty()
                .withMessage('insert gender')

        ]
    }

    registerForm(){
        return[
            check('username')
                .trim()
                .not().isEmpty()
                .withMessage('نام کاربری خالی است')
                .isLength({min: 4})
                .withMessage('نام کاربری حداقل باید 4 رقم باشد'),

            check('password')
                .trim()
                .not().isEmpty()
                .withMessage('پسورد خالی است')
                .isLength({min : 8})
                .withMessage('پسورد حداقل باید 8 رقم باشد'),

            check('repassword')
                .trim()
                .not().isEmpty()
                .withMessage('تکرار پسورد خالی است')
                .isLength({min : 8})
                .withMessage('تکرار پسورد حداقل باید 8 رقم باشد')
            
        ]
    }
}

module.exports = new authValidator()