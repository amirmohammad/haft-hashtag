const validator = require('./validator'); 
const { check, body, sanitizeBody, buildCheckFunction, query } = require('express-validator');

class Idea extends validator{


    createCategoryValidator(){
        return [
            check('category')
                .trim()
                .not().isEmpty()
                .withMessage('category وارد شود'),
        ]
    }

    updateCategoryValidator(){
        return [
            check('oldCategory')
                .trim()
                .not().isEmpty()
                .withMessage('oldCategory وارد شود'),

            check('newCategory')
                .trim()
                .not().isEmpty()
                .withMessage('newCategory وارد شود'),
        ]
    }

    deleteCategoryValidator(){
        return [
            check('category')
                .trim()
                .not().isEmpty()
                .withMessage('category وارد شود'),
        ]
    }

    uploadVideoValidator(){
        return [
            query('category')
                .trim()
                .not().isEmpty()
                .withMessage('category وارد شود')
                .isMongoId()
                .withMessage('id is invalid'),
        ]
    }

    getVideoCategoryValidator(){
        return [
            body('category')
                .trim()
                .not().isEmpty()
                .withMessage('category وارد شود')
                .isMongoId()
                .withMessage('id is invalid'),
        ]
    }
    
    getVideoValidator(){
        return [
            body('video')
                .trim()
                .not().isEmpty()
                .withMessage('video وارد شود')
                .isMongoId()
                .withMessage('id is invalid'),
        ]
    }

    deleteVideoValidator(){
        return [
            body('video')
                .trim()
                .not().isEmpty()
                .withMessage('video وارد شود')
                .isMongoId()
                .withMessage('id is invalid'),
        ]
    }

    sendCommentValidator(){
        return [
            body('video')
                .trim()
                .not().isEmpty()
                .withMessage('video وارد شود')
                .isMongoId()
                .withMessage('id is invalid'),

            body('comment')
                .trim()
                .not().isEmpty()
                .withMessage('comment وارد شود'),
        ]
    }

    updateCommentValidator(){
        return [
            body('comment')
                .trim()
                .not().isEmpty()
                .withMessage('comment وارد شود')
                .isMongoId()
                .withMessage('comment is invalid'),

            body('newComment')
                .trim()
                .not().isEmpty()
                .withMessage('newComment وارد شود'),
        ]
    }

    deleteCommentValidator(){
        return [
            body('comment')
                .trim()
                .not().isEmpty()
                .withMessage('comment وارد شود')
                .isMongoId()
                .withMessage('comment is invalid'),
        ]
    }

    getCommentValidator(){
        return [
            body('video')
                .trim()
                .not().isEmpty()
                .withMessage('video وارد شود')
                .isMongoId()
                .withMessage('video is invalid'),
        ]
    }

    sendPointValidator(){
        return [
            body('video')
                .trim()
                .not().isEmpty()
                .withMessage('video وارد شود')
                .isMongoId()
                .withMessage('video is invalid'),

            body('point')
                .trim()
                .not().isEmpty()
                .withMessage('point وارد شود'),
        ]
    }

    getPointValidator(){
        return [
            body('video')
                .trim()
                .not().isEmpty()
                .withMessage('video وارد شود')
                .isMongoId()
                .withMessage('video is invalid'),
        ]
    }

    getPointsForVideoValidator(){
        return [
            body('video')
                .trim()
                .not().isEmpty()
                .withMessage('video وارد شود')
                .isMongoId()
                .withMessage('video is invalid'),
        ]
    }

}

module.exports = new Idea()
