const validator = require('./validator'); 
const { check, body, sanitizeBody, buildCheckFunction, param } = require('express-validator');

class phoneNumberValidator extends validator{

    upload(){
        return[
            body('link')
                .trim()
                .not().isEmpty()
                .withMessage('لینک را وارد کنید')
                .isURL()
                .withMessage('فرمت لینک صحیح نیست'),
        ]
    }
}

module.exports = new phoneNumberValidator()
