const validator = require('./validator'); 
const { check, body, sanitizeBody, buildCheckFunction } = require('express-validator');

class Idea extends validator{

    sendIdeaValidator(){
        return [
            check('category')
                .trim()
                .not().isEmpty()
                .withMessage('category وارد شود'),

            check('type')
                .trim()
                .not().isEmpty()
                .withMessage('type وارد شود'),

            check('request')
                .trim()
                .not().isEmpty()
                .withMessage('request وارد شود'),

            check('description')
                .trim()
                .not().isEmpty()
                .withMessage('description وارد شود'),
        ]
    }

    getIdeaValidator(){
        return [
            check('category')
                .trim()
                .not().isEmpty()
                .withMessage('category وارد شود'),
        ]
    }
    
}

module.exports = new Idea()
