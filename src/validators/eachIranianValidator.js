const validator = require('./validator'); 
const { check, body, sanitizeBody, buildCheckFunction } = require('express-validator');

class companyCategory extends validator{

    peopleValidator(){
        return[
            check('type')
                .trim()
                .not().isEmpty()
                .withMessage('type وارد شود'),
            body('fullname')
                .trim()
                .not().isEmpty()
                .withMessage('fullname وارد شود'),

            body('personalID')
                .trim()
                .not().isEmpty()
                .withMessage('personalID وارد شود'),

            body('gender')
                .trim()
                .not().isEmpty()
                .withMessage('gender وارد شود'),

            body('birthdate')
                .trim()
                .not().isEmpty()
                .withMessage('birthdate وارد شود'),

            body('description')
                .trim()
                .not().isEmpty()
                .withMessage('description وارد شود'),
        ]
    }

    getpeopleValidator(){
        return [
            check('people')
                .trim()
                .not().isEmpty()
                .withMessage('people وارد شود'),
        ]
    }

    createPlanValidator(){
        return[
            check('name')
                .trim()
                .not().isEmpty()
                .withMessage('name وارد شود'),
            check('fee')
                .trim()
                .not().isEmpty()
                .withMessage('fee وارد شود'),
            check('days')
                .trim()
                .not().isEmpty()
                .withMessage('days وارد شود'),
        ]
    }

    updatePlanValidator(){
        return[
            check('id')
                .trim()
                .not().isEmpty()
                .withMessage('id وارد شود'),
            check('name')
                .trim()
                .not().isEmpty()
                .withMessage('name وارد شود'),
            check('fee')
                .trim()
                .not().isEmpty()
                .withMessage('fee وارد شود'),
            check('days')
                .trim()
                .not().isEmpty()
                .withMessage('days وارد شود'),
        ]
    }

    deletePlanValidator(){
        return[
            check('id')
                .trim()
                .not().isEmpty()
                .withMessage('id وارد شود'),
        ]
    }


    buyPlanValidator(){
        return[
            check('plan')
                .trim()
                .not().isEmpty()
                .withMessage('plan وارد شود'),
        ]
    }

    applyForJobValidator(){
        return[
            check('plan')
                .trim()
                .not().isEmpty()
                .withMessage('plan وارد شود'),

            check('state')
                .trim()
                .not().isEmpty()
                .withMessage('state وارد شود'),

            check('city')
                .trim()
                .not().isEmpty()
                .withMessage('city وارد شود'),

            check('jobTitle')
                .trim()
                .not().isEmpty()
                .withMessage('jobTitle وارد شود'),

            check('description')
                .trim()
                .not().isEmpty()
                .withMessage('description وارد شود'),

            check('name')
                .trim()
                .not().isEmpty()
                .withMessage('name وارد شود'),
        ]
    }
    
}

module.exports = new companyCategory()
