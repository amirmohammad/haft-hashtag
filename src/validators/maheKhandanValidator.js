const validator = require('./validator'); 
const { check, body, sanitizeBody, buildCheckFunction } = require('express-validator');

class Idea extends validator{

    sendCommentValidator(){
        return [
            check('video')
                .trim()
                .not().isEmpty()
                .withMessage('video وارد شود')
                .isMongoId()
                .withMessage('id اچتباه است'),

            check('text')
                .trim()
                .not().isEmpty()
                .withMessage('request وارد شود'),
        ]
    }



    getVideo(){
        return [
            check('video')
                .trim()
                .not().isEmpty()
                .withMessage('video وارد شود')
                .isMongoId()
                .withMessage('id is incorrect'),
        ]
    }

    updateCommentValidator(){
        return [
            check('comment')
                .trim()
                .not().isEmpty()
                .withMessage('commentid وارد شود')
                .isMongoId()
                .withMessage('id is incorrect'),
            check('newComment')
                .trim()
                .not().isEmpty()
                .withMessage('newcomment وارد شود'),
        ]
    }

    deleteCommentValidator(){
        return [
            check('comment')
                .trim()
                .not().isEmpty()
                .withMessage('commentid وارد شود')
                .isMongoId()
                .withMessage('id is incorrect'),
        ]
    }

    allReactionsValidator(){
        return [
            check('video')
                .trim()
                .not().isEmpty()
                .withMessage('video وارد شود')
                .isMongoId()
                .withMessage('id is incorrect'),
        ]
    }

    sendReactionValidator(){
        return [
            check('video')
                .trim()
                .not().isEmpty()
                .withMessage('video وارد شود')
                .isMongoId()
                .withMessage('id is incorrect'),

            check('point')
                .trim()
                .not().isEmpty()
                .withMessage('point وارد شود')
                .isNumeric()
                .withMessage('point should be numeric')
                .isInt({min: 1, max: 3})
                .withMessage('point range is incorrect'),
        ]
    }
    
    
    
}

module.exports = new Idea()
