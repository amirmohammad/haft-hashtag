const validator = require('./validator'); 
const { check, body, sanitizeBody, buildCheckFunction } = require('express-validator');

class companyCategory extends validator{

    createCategoryValidator(){
        return[
            check('category')
                .trim()
                .not().isEmpty()
                .withMessage('نام کتگوری وارد شود'),
        ]
    }

    updateCategoryName(){
        return [
            check('oldname')
                .trim()
                .not().isEmpty()
                .withMessage('نام قدیم کتکوری وارد شود'),

            check('newname')
                .trim()
                .not().isEmpty()
                .withMessage('نام جدید کتکوری وارد شود'),
        ]
    }

    deleteCategoryValidator(){
        return[
            check('category')
                .trim()
                .not().isEmpty()
                .withMessage('نام کتگوری وارد شود'),
        ]
    }

    
}

module.exports = new companyCategory()
