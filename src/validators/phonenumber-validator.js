const validator = require('./validator'); 
const { check, body, sanitizeBody, buildCheckFunction } = require('express-validator');

class phoneNumberValidator extends validator{

    checkFormat(){
        return[
            check('phoneNumber')
                .trim()
                .isLength({min:11, max:11})
                .withMessage('فرمت شماره صحیح نیست'),
        ]
    }

    verifyToken() {
        return [
            check('phoneNumber')
                .trim()
                .isLength({min:11, max:11})
                .withMessage('فرمت شماره صحیح نیست'),
            
            check('token')
                .trim()
                .not().isEmpty()
                .withMessage('توکن را وارد کنید')
        ]
    }

    
}

module.exports = new phoneNumberValidator()
