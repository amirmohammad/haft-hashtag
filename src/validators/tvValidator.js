const validator = require('./validator'); 
const { check, body, sanitizeBody, buildCheckFunction } = require('express-validator');

class phoneNumberValidator extends validator{

    createChannel(){
        return[
            check('channel')
                .trim()
                .not().isEmpty()
                .withMessage('channel را وارد کنید'),
        ]
    }

    updateChannel(){
        return[
            check('channel')
                .trim()
                .not().isEmpty()
                .withMessage('channel را وارد کنید')
                .isMongoId()
                .withMessage('id format is incorrect'),

            check('newChannel')
                .trim()
                .not().isEmpty()
                .withMessage('new channel را وارد کنید'),
        ]
    }

    getVideo(){
        return[
            check('video')
                .trim()
                .not().isEmpty()
                .withMessage('video را وارد کنید')
                .isMongoId()
                .withMessage('id format is incorrect'),
        ]
    }

    allVideos(){
        return[
            check('channel')
                .trim()
                .not().isEmpty()
                .withMessage('channel را وارد کنید')
                .isMongoId()
                .withMessage('id format is incorrect'),
        ]
    }

    deleteVideo(){
        return[
            check('video')
                .trim()
                .not().isEmpty()
                .withMessage('video را وارد کنید')
                .isMongoId()
                .withMessage('id format is incorrect'),
        ]
    }

    deleteChannel(){
        return[
            check('channel')
                .trim()
                .not().isEmpty()
                .withMessage('channel را وارد کنید')
                .isMongoId()
                .withMessage('id format is incorrect'),
        ]
    }

    getChannel(){
        return[
            check('channel')
                .trim()
                .not().isEmpty()
                .withMessage('channel را وارد کنید')
                .isMongoId()
                .withMessage('id format is incorrect'),
        ]
    }

    
}

module.exports = new phoneNumberValidator()
