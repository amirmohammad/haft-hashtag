const validator = require('./validator'); 
const { check, body, sanitizeBody, buildCheckFunction } = require('express-validator');

class companyCategory extends validator{

    createCategoryValidator(){
        return[
            check('categoryName')
                .trim()
                .not().isEmpty()
                .withMessage('نام کتگوری وارد شود'),
        ]
    }

    updateCategoryName(){
        return [
            check('oldCategoryName')
                .trim()
                .not().isEmpty()
                .withMessage('نام قدیم کتکوری وارد شود'),

            check('newCategoryName')
                .trim()
                .not().isEmpty()
                .withMessage('نام جدید کتکوری وارد شود'),
        ]
    }

    deleteCategoryValidator(){
        return[
            check('categoryName')
                .trim()
                .not().isEmpty()
                .withMessage('نام کتگوری وارد شود'),
        ]
    }

    
}

module.exports = new companyCategory()
