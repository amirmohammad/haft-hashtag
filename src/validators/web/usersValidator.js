
const validator = require('./../validator'); 
const { check, body, sanitizeBody, buildCheckFunction, param, query } = require('express-validator');

class userValidator extends validator{

    viewUsersPage(){
        return[
            param('id')
                .not().isEmpty()
                .withMessage('یوزر نباید خالی باشد')
                .isMongoId()
                .withMessage('ایدی را اصلاح کنید'),
        ]
    }

    
}

module.exports = new userValidator()
