const validator = require('./../validator'); 
const { check, body, sanitizeBody, buildCheckFunction, param, query } = require('express-validator');

class channel extends validator{

    viewChannel(){
        return[
            query('id')
                .trim()
                .not().isEmpty()
                .withMessage('آی دی نباید خالی باشد')
                .isMongoId()
                .withMessage('آی دی اشتباه است'),
        ]
    }

    updateChannel(){
        return [
            body('id')
                .trim()
                .not().isEmpty()
                .withMessage('آی دی نباید خالی باشد')
                .isMongoId()
                .withMessage('آی دی اشتباه است'),

            body('channelName')
                .trim()
                .not().isEmpty()
                .withMessage('آی دی نباید خالی باشد'),

        ]
    }

    setBanner() {
        return [
            param('id')
                .trim()
                .isMongoId()
                .withMessage('آیدی نا معتبر است')
        ]
    }

    
}

module.exports = new channel()
