const validator = require('./../validator'); 
const { check, body, sanitizeBody, buildCheckFunction, param, query } = require('express-validator');

class Plan extends validator{

    createPlan(){
        return[
            body('name')
                .not().isEmpty()
                .withMessage('پلن نباید خالی باشد'),

            body('days')
                .not().isEmpty()
                .withMessage('مدت زمان نباید خالی باشد')
                .isNumeric()
                .withMessage('مدت زمان یک عدد بر حسب روز است'),

            body('fee')
                .not().isEmpty()
                .withMessage('فی نباید خالی باشد')
                .isNumeric()
                .withMessage('فی باید عددی بر حسب ریال باشد'),
        ]
    }

    viewPlan() {
        return [
            param('id')
                .isMongoId()
                .withMessage('id نادرست ')
        ]
    }

    deletePlan() {
        return [
            check('id')
                .isMongoId()
                .withMessage('id نادرست ')
        ]
    }

    
}

module.exports = new Plan()
