module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      fontFamily: {}
    },
  },
  variants: {},
  plugins: [
    require('tailwindcss'),
    require(`autoprefixer`),
    require('tailwindcss-rtl')
  ],
}
