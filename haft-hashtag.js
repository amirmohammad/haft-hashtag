
require('dotenv').config();
require('app-module-path').addPath(__dirname)

const config = require('./src/config');

const express = require('express');

async function startServer() {
    const app = express();

    await require('./src/loaders')({ expressApp: app});

    app.listen(config.app.port, () => {
        console.log(`Server started on ${config.app.port}`);
    });
}

startServer()